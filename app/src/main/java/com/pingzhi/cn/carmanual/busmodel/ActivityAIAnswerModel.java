package com.pingzhi.cn.carmanual.busmodel;

import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

/**
 * Created by ${fanjie} on 2017/7/14.
 */

public class ActivityAIAnswerModel {

    public ShowPagerModel mShowPagerModel;


    public ActivityAIAnswerModel(ShowPagerModel showPagerModel) {
        mShowPagerModel = showPagerModel;
    }

    public ActivityAIAnswerModel() {
    }
}

package com.pingzhi.cn.carmanual.secondpage.fragment.chexian;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.busmodel.CurrentPagerModel;
import com.pingzhi.cn.carmanual.busmodel.LiPeiDataModel;
import com.pingzhi.cn.carmanual.secondpage.fragment.BaseFragment;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.adapter.LiPeiPageradapter;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.LiPeiModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/9.
 */

public class LiPieFragment extends BaseFragment {

    private RecyclerView mRecyclerView;
    private List<LiPeiModel> datas = new ArrayList<>();
    private ViewPager mViewPager;
    private LiPeiPageradapter liPeiPageradapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        mRecyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_lipei, container, false);
//        for (int i = 0; i < 20; i++){
//            LiPeiModel liPeiModel = new LiPeiModel();
//            if (i % 2 == 0){
//                liPeiModel.question = "单方事故出险后未及时报案有什么影响?";
//                liPeiModel.content = "单方事故出险后未及时报案有什么影响?单方事故出险后未及时报案有什么影响?单方事故出险后未及时报案有什么影响?单方事故出险后未及时报案有什么影响?单方事故出险后未及时报案有什么影响?";
//            }else {
//                liPeiModel.question = "同责如何理赔?发票怎么开";
//                liPeiModel.content = "同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开同责如何理赔?发票怎么开";
//            }
//            datas.add(liPeiModel);
//        }
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
//        mRecyclerView.setLayoutManager(linearLayoutManager);
//        LiPeiAdapter liPeiAdapter = new LiPeiAdapter(datas);
//        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
//
//            @Override
//            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//                outRect.set(0, ScreenUtils.dip2px(getContext(), 5), 0, ScreenUtils.dip2px(getContext(), 5));
//            }
//        });
//        mRecyclerView.setAdapter(liPeiAdapter);
        mViewPager = (ViewPager) inflater.inflate(R.layout.fragment_lipei, container, false);
        liPeiPageradapter = new LiPeiPageradapter(datas);
        mViewPager.setAdapter(liPeiPageradapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                startVoice(position);
                EventBus.getDefault().post(new CurrentPagerModel(position, liPeiPageradapter.mLists.size()));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        EventBus.getDefault().register(this);
        return mViewPager;
    }

    private void startVoice(int position){
        List<LiPeiModel> datas = liPeiPageradapter.mLists.get(position);
        String str = "";
        for (int i = 0; i < datas.size(); i++){
            str += datas.get(i).question + "?";
            str += datas.get(i).content;
        }
        mShowtipPresent.startVoiceWithText(str);
    }

    @Subscribe(sticky = true)
    public void handleLiPeiData(LiPeiDataModel liPeiDataModel){

        datas.clear();
        datas.addAll(liPeiDataModel.datas);
        liPeiPageradapter.notifyDataSetChanged();
        mViewPager.setCurrentItem(0);
        startVoice(0);
        EventBus.getDefault().removeStickyEvent(liPeiDataModel);
        EventBus.getDefault().post(new CurrentPagerModel(0, liPeiPageradapter.mLists.size()));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
}

package com.pingzhi.cn.carmanual.secondpage.fragment.chexian.adapter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.TiaoKuanAdapter;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.TiaoKuanModel;
import com.pingzhi.cn.carmanual.utils.ScreenUtils;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/10.
 */

public class TiaoKuanPagerAdapter extends PagerAdapter {

    private List<TiaoKuanModel> datas;

    public TiaoKuanPagerAdapter(List<TiaoKuanModel> datas) {
        this.datas = datas;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    private LayoutInflater mLayoutInflater;

    public LayoutInflater getLayoutInflater(Context context) {
        if (mLayoutInflater == null){
            mLayoutInflater = LayoutInflater.from(context);
        }
        return mLayoutInflater;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        RecyclerView view = (RecyclerView) getLayoutInflater(container.getContext()).inflate(R.layout.pager_tiao_kuan_item, container, false);
        view.setTag(datas.get(position));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(container.getContext());
        view.setLayoutManager(linearLayoutManager);
        TiaoKuanAdapter tiaoKuanAdapter = new TiaoKuanAdapter(datas.get(position));
        view.setAdapter(tiaoKuanAdapter);
        view.addItemDecoration(new RecyclerView.ItemDecoration() {
            /**
             * Draw any appropriate decorations into the Canvas supplied to the RecyclerView.
             * Any content drawn by this method will be drawn before the item views are drawn,
             * and will thus appear underneath the views.
             *
             * @param c      Canvas to draw into
             * @param parent RecyclerView this ItemDecoration is drawing into
             * @param state  The current state of RecyclerView
             */
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                super.onDraw(c, parent, state);
                drawHorizontalLine(c, parent, state);
            }

            /**
             * Retrieve any offsets for the given item. Each field of <code>outRect</code> specifies
             * the number of pixels that the item view should be inset by, similar to padding or margin.
             * The default implementation sets the bounds of outRect to 0 and returns.
             * <p>
             * <p>
             * If this ItemDecoration does not affect the positioning of item views, it should set
             * all four fields of <code>outRect</code> (left, top, right, bottom) to zero
             * before returning.
             * <p>
             * <p>
             * If you need to access Adapter for additional data, you can call
             * {@link RecyclerView#getChildAdapterPosition(View)} to get the adapter position of the
             * View.
             *
             * @param outRect Rect to receive the output.
             * @param view    The child view to decorate
             * @param parent  RecyclerView this ItemDecoration is decorating
             * @param state   The current state of RecyclerView.
             */
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.set(0, 0, 0, ScreenUtils.dip2px(view.getContext(), 1));
            }

            //画横线, 这里的parent其实是显示在屏幕显示的这部分
            public void drawHorizontalLine(Canvas c, RecyclerView parent, RecyclerView.State state){
                int left = parent.getPaddingLeft();
                int right = parent.getWidth() - parent.getPaddingRight() - ScreenUtils.dip2px(parent.getContext(), 5);
                final int childCount = parent.getChildCount();
                Paint paint = new Paint();
                paint.setColor(Color.BLACK);
                paint.setAntiAlias(true);
                for (int i = 0; i < childCount; i++){
                    final View child = parent.getChildAt(i);

                    //获得child的布局信息
                    final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)child.getLayoutParams();
                    final int top = child.getBottom();
                    final int bottom = top + ScreenUtils.dip2px(parent.getContext(), 1);
                    c.drawLine(left, top, right, bottom, paint);
                }
            }
        });
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        if (view.getParent() == container){
            container.removeView(view);
        }
    }

    @Override public int getItemPosition(Object object) {
        View view = (View) object;
        if (!datas.contains(view.getTag())){
            return POSITION_NONE;
        }
        return super.getItemPosition(object);
    }
}

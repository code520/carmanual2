package com.pingzhi.cn.carmanual.voicemanager.voicesynthesizer;

import android.support.annotation.NonNull;

/**
 * Created by ${fanjie} on 2017/6/23.
 */

public interface VoiceSynthesizerAble {

    void startVoice(@NonNull String text);

    void startHelloVoice(@NonNull String text);

    void startHelloVoice(@NonNull String text, int type);

    void stopVoice();

    void destory();

    boolean isSpeaking();

}

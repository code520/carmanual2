package com.pingzhi.cn.carmanual.model;

/**
 * Created by ${fanjie} on 2017/7/2.
 */

public class GetNewVersionModel {


    /**
     * code : 200
     * msg : success
     * content : {"id":1,"gainChannel":"zhushou91","highestVersion":"2.0","downLoadLink":"http://www.baidu.com","note":"增加驾驶模式"}
     */

    private int code;
    private String msg;
    private ContentBean content;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ContentBean getContent() {
        return content;
    }

    public void setContent(ContentBean content) {
        this.content = content;
    }

    public static class ContentBean {
        /**
         * id : 1
         * gainChannel : zhushou91
         * highestVersion : 2.0
         * downLoadLink : http://www.baidu.com
         * note : 增加驾驶模式
         */

        private int id;
        private String gainChannel;
        private String highestVersion;
        private String downLoadLink;
        private String note;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getGainChannel() {
            return gainChannel;
        }

        public void setGainChannel(String gainChannel) {
            this.gainChannel = gainChannel;
        }

        public String getHighestVersion() {
            return highestVersion;
        }

        public void setHighestVersion(String highestVersion) {
            this.highestVersion = highestVersion;
        }

        public String getDownLoadLink() {
            return downLoadLink;
        }

        public void setDownLoadLink(String downLoadLink) {
            this.downLoadLink = downLoadLink;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }
    }
}

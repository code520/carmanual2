package com.pingzhi.cn.carmanual.ownview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.pingzhi.cn.carmanual.busmodel.VoiceFinishModel;
import com.pingzhi.cn.carmanual.busmodel.VoiceNotifyModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static android.animation.ObjectAnimator.ofInt;

/**
 * Created by ${fanjie} on 2017/6/28.
 */

public class MyGroup extends LinearLayout {

    private static final String TAG = "=MyGroup=";

    public MyGroup(Context context) {
        this(context, null);
    }

    private List<MyItem> mViews;
    private Random mRandom;
    private Random mPointRandom;
    private Random mScaleRandom;
    private static final int totalCount = 65;
    private static final int startHeight = 30;
    public MyGroup(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER);
        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 200));
        mRandom = new Random();
        mPointRandom = new Random();
        mScaleRandom = new Random();
        mViews = new LinkedList<>();
        for (int i = 0; i < totalCount; i++){
            View view = new View(context);
            view.setBackgroundColor(Color.RED);
            LayoutParams params = new LayoutParams(3, startHeight);
            params.leftMargin = 5;
            params.rightMargin = 5;
            addView(view, params);
            MyItem item = new MyItem();
            item.mView = view;
            item.mHeight = 30;
            mViews.add(item);
        }

        operatorPoint();
        operatorCount();
    }

    private boolean isStart;

    public boolean isStart() {
        return isStart;
    }

    public void startAutoPlay(){
        removeCallbacks(mRunnable);
        isStart = postDelayed(mRunnable, 300);
    }

    public void stopAutoPlay(){
        isStart = false;
        removeCallbacks(mRunnable);
    }

    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            operatorHeight();
            startAnim();
        }
    };

    private int getHighPoint(){
        return 28 + mPointRandom.nextInt(8);
    }
    private int leftPoint;
    private int highPoint;
    private int rightPoint;

    private void operatorPoint(){
        highPoint = getHighPoint();
        leftPoint = highPoint - (20 + mPointRandom.nextInt(3));
        rightPoint = highPoint + (20 + mPointRandom.nextInt(3));
    }

    private int leftAdd;
    private int hightAdd;
    private int rightAdd;
    private static final int BASESCALE = 5;
    private static final int MAXADD = 50;
    private static final int SUB = 20;
    private void operatorHeight(){
        hightAdd = Math.abs(mScaleRandom.nextInt(MAXADD)) + 50;
        leftAdd = hightAdd - mScaleRandom.nextInt(SUB);
        rightAdd = hightAdd - mScaleRandom.nextInt(SUB);
        Log.e(TAG, "operatorHeight: hightAdd " + hightAdd, null);
        Log.e(TAG, "operatorHeight: leftAdd " + leftAdd, null);
        Log.e(TAG, "operatorHeight: rightAdd " + rightAdd, null);
    }

    public void setVolum(int volum){
        hightAdd = Math.abs(mScaleRandom.nextInt(MAXADD)) + volum + 20;
        leftAdd = hightAdd - mScaleRandom.nextInt(SUB);
        rightAdd = hightAdd - mScaleRandom.nextInt(SUB);
        startAnim();
    }

    private int leftCount;
    private int hightCount;
    private int rightCount;
    private static final int distance = 5;
    private void operatorCount(){
        leftCount = 5 + mRandom.nextInt(4);
        hightCount = 5 + mRandom.nextInt(5);
        rightCount = 5 + mRandom.nextInt(4);
    }

    private static final int SUB_DISTANCE = 10;
    private int getRandomSub(){
        return mScaleRandom.nextInt(SUB_DISTANCE);
    }


    private void setScale(int startPoint, int startAdd, int count){
        int every = startAdd / count;
        for (int i = 0; i < count; i++){
            if (startPoint - i > 0){
                MyItem item1 = mViews.get(startPoint - i);
                item1.addHeight = (int) ((count - i) * 1.0 / (count + 1) * startAdd) + Math.abs(mRandom.nextInt(MAXADD / count));//startAdd - i * every;//
                item1.needAnima = true;
                Log.e(TAG, "setScale: " + item1.addHeight, null);
            }

            if (startPoint + i < totalCount){
                MyItem item2 = mViews.get(startPoint + i);
                item2.addHeight = (int) ((count - i) * 1.0 / (count + 1) * startAdd) + Math.abs(mRandom.nextInt(MAXADD / count));//(int) (startAdd - i * distance);
                item2.needAnima = true;
            }
        }
    }

    private void reset(){
        for (int i = 0; i < mViews.size(); i++){
            MyItem item = mViews.get(i);
            if (!item.needAnima){
                item.addHeight = startHeight - item.mHeight;
            }
        }
    }

    private void resetNeedAnima(){
        for (int i = 0; i < mViews.size(); i++){
            MyItem item = mViews.get(i);
            item.needAnima = false;
        }
    }

    private void startAnim(){
        Log.e(TAG, "startAnim: " + mViews.get(28).mHeight, null);
//        operatorHeight();
//        if (hightAdd < 70){
            operatorPoint();
            operatorCount();
//        }
        setScale(leftPoint, leftAdd, leftCount);
        setScale(highPoint, hightAdd, hightCount);
        setScale(rightPoint, rightAdd, rightCount);
        reset();
        for (int i = 0; i < totalCount; i++) {
            MyItem item = mViews.get(i);
            item.mView.setBackgroundColor(getColor());
            ObjectAnimator animator = ofInt(item, "AddHeight", 0, item.addHeight);
            animator.setDuration(500);
            animator.start();
            resetNeedAnima();
            if (i == highPoint) {
                animator.addListener(new AnimatorListenerAdapter() {
                    /**
                     * {@inheritDoc}
                     *
                     * @param animation
                     */
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                            for (int i = 0; i < totalCount; i++){
                                MyItem item1 = mViews.get(i);
                                Animator animator = ObjectAnimator.ofInt(item1, "Height", item1.mHeight, startHeight);
                                animator.setDuration(500);
                                animator.start();
                            }
                    }
                });
            }
        }
        if (isStart){
            postDelayed(mRunnable, 500);
        }
    }

    public void addItem(MyItem item, LayoutParams params){
        this.addView(item.mView, params);
    }

    private int getColor(){
        return Color.rgb(100 + mRandom.nextInt(50), 100 + mRandom.nextInt(50), 100 + mRandom.nextInt(50));
    }

    public void startRecord(){
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    public void stopRecord(){
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleVoiceModel(VoiceNotifyModel voiceNotifyModel){
        if (isStart){
            stopAutoPlay();
        }
        setVolum(voiceNotifyModel.volum * 2);
    }

    @Subscribe
    public void handleVoiceFinishModel(VoiceFinishModel voiceFinishModel){
        stopAutoPlay();
    }


}

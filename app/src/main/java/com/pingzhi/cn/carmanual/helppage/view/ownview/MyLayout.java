package com.pingzhi.cn.carmanual.helppage.view.ownview;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.chechi.aiandroid.R;


/**
 * Created by ${fanjie} on 2017/6/26.
 */

public class MyLayout extends ConstraintLayout {

    private int icon;
    private String title;
    private String content;
    public MyLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.cardlayout, this, true);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MyLayout);
        icon = typedArray.getResourceId(R.styleable.MyLayout_layout_icon, R.mipmap.baoyang);
        title = typedArray.getString(R.styleable.MyLayout_title);
        content = typedArray.getString(R.styleable.MyLayout_content);
        typedArray.recycle();
    }

    private ImageView mImageView;
    private TextView mTitleView;
    private TextView mContent;
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mImageView = (ImageView) findViewById(R.id.id_mylayout_icon);
        mTitleView = (TextView) findViewById(R.id.id_mylayout_title);
//        mTitleView.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        mContent = (TextView) findViewById(R.id.id_mylayout_content);
        mContent.setLineSpacing(10, 1);
        initData();
    }

    private void initData(){
        mImageView.setImageResource(icon);
        mTitleView.setText(title);
        mContent.setText(content);
    }

    public void setTitle(String title){
        mTitleView.setText(title);
    }

    public void setContent(String content){
        mContent.setText(content);
    }
}

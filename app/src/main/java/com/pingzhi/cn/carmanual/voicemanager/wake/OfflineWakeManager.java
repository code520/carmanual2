package com.pingzhi.cn.carmanual.voicemanager.wake;

import android.content.Context;

/**
 * Created by ${fanjie} on 2017/6/23.
 */

public class OfflineWakeManager implements WakeupAble {
    private Context mContext;

    public OfflineWakeManager(Context context) {
        mContext = context;
    }

    @Override
    public void registerListener(WakeUpCallback wakeUpCallback) {

    }

    @Override
    public void unRegisterListener() {

    }

    @Override
    public void wakeUpStart() {

    }

    @Override
    public void wakeUpStop() {

    }

}

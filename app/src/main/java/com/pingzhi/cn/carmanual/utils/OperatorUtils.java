package com.pingzhi.cn.carmanual.utils;

import android.content.Context;
import android.content.Intent;

/**
 * Created by ${fanjie} on 2017/6/21.
 */

public class OperatorUtils {


    public static void stepToHome(Context context){
        Intent intent= new Intent(Intent.ACTION_MAIN);

//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //如果是服务里调用，必须加入new task标识

        intent.addCategory(Intent.CATEGORY_HOME);

        context.startActivity(intent);
    }

}

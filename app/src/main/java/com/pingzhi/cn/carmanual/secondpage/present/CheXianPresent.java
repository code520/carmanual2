package com.pingzhi.cn.carmanual.secondpage.present;

import android.support.v4.app.Fragment;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.busmodel.AIGonShiModel;
import com.pingzhi.cn.carmanual.busmodel.LiPeiDataModel;
import com.pingzhi.cn.carmanual.busmodel.TiaoKuanDataModel;
import com.pingzhi.cn.carmanual.db.DbHelper;
import com.pingzhi.cn.carmanual.db.model.Insurance;
import com.pingzhi.cn.carmanual.db.model.InsuranceCompany;
import com.pingzhi.cn.carmanual.db.model.InsuranceCompanyDao;
import com.pingzhi.cn.carmanual.db.model.Qa;
import com.pingzhi.cn.carmanual.db.model.QaDao;
import com.pingzhi.cn.carmanual.present.ShowtipPresent;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.CheXianFragment;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.GonShiModel;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.LiPeiModel;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.TiaoKuanModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ${fanjie} on 2017/6/21.
 */

public class CheXianPresent implements BasePresent {
    @Override
    public String[] getTypeTitles() {
        return new String[]{
                "公司", "理赔", "", "", "", "", "", "", "", "", "条款", "投保"
        };
    }

    @Override
    public int[] getTypeIcons() {
        return new int[]{
                R.mipmap.xinxi_, R.mipmap.lipei_, R.mipmap.trans, R.mipmap.trans,
                R.mipmap.trans, R.mipmap.trans, R.mipmap.trans, R.mipmap.trans,
                R.mipmap.trans, R.mipmap.trans, R.mipmap.tiaokuan_, R.mipmap.xubao_
        };
    }

    @Override
    public int[] getSelectIcons() {
        return new int[]{
                R.mipmap.xinxi, R.mipmap.lipei_, R.mipmap.trans, R.mipmap.trans,
                R.mipmap.trans, R.mipmap.trans, R.mipmap.trans, R.mipmap.trans,
                R.mipmap.trans, R.mipmap.trans, R.mipmap.tiaokuan, R.mipmap.xubao
        };
    }

    @Override
    public String getType() {
        return "车 险";
    }

    private Map<String, Integer> types;

    public Map<String, Integer> getTypes() {
        if (types == null){
            types = new HashMap<>(4);
            types.put("理赔", CheXianFragment.LI_PEI);
            types.put("公司", CheXianFragment.GON_SHI);
            types.put("投保", CheXianFragment.TOU_BAO);
            types.put("条款", CheXianFragment.TIAO_KUAN);
        }
        return types;
    }

    public Map<String, String> replaceMap;

    public Map<String, String> getReplaceMap() {
        if (replaceMap == null){
            replaceMap = new HashMap<>();
            replaceMap.put("理赔", "保险理赔");
            replaceMap.put("投保", "购买续保");
            replaceMap.put("公司", "公司");
            replaceMap.put("条款", "条款");
        }
        return replaceMap;
    }

    @Override
    public void query(String world) {
        if (getTypes().containsKey(world)){
            int type = getTypes().get(world);
            if (!cheXianFragment.isAdded()){
                cheXianFragment.setType(type);
            }else {
                cheXianFragment.setFragment(type);
            }

            switch (type){
                case CheXianFragment.LI_PEI:{
                    List<Qa> qaList = DbHelper.getInstance().getDaoSession().getQaDao().queryBuilder().where(QaDao.Properties.Category.eq(getReplaceMap().get(world))).list();
                    int count = qaList.size();
                    List<LiPeiModel> datas = new ArrayList<>(count);
                    for (int i = 0; i< count; i++){
                        Qa qa = qaList.get(i);
                        LiPeiModel liPeiModel = new LiPeiModel();
                        liPeiModel.content = qa.answer;
                        liPeiModel.question = qa.question;
                        datas.add(liPeiModel);
                    }
                    LiPeiDataModel liPeiDataModel = new LiPeiDataModel(datas);
                    EventBus.getDefault().postSticky(liPeiDataModel);
                }
                break;

                case CheXianFragment.GON_SHI:{
                    List<GonShiModel> datas = new ArrayList<>();
                    List<InsuranceCompany> insuranceCompanyList = DbHelper.getInstance().getDaoSession().getInsuranceCompanyDao().queryBuilder().list();
                    for (InsuranceCompany insuranceCompany : insuranceCompanyList){
                        datas.add(insuranceCompany.getGonShiModel());
                    }
                    AIGonShiModel aiGonShiModel = new AIGonShiModel();
                    aiGonShiModel.datas = datas;
                    EventBus.getDefault().postSticky(aiGonShiModel);
                }
                break;
                case CheXianFragment.TOU_BAO:{
                    List<Qa> qaList = DbHelper.getInstance().getDaoSession().getQaDao().queryBuilder().where(QaDao.Properties.Category.eq(getReplaceMap().get(world))).list();
                    int count = qaList.size();
                    List<LiPeiModel> datas = new ArrayList<>(count);
                    for (int i = 0; i< count; i++){
                        Qa qa = qaList.get(i);
                        LiPeiModel liPeiModel = new LiPeiModel();
                        liPeiModel.content = qa.answer;
                        liPeiModel.question = qa.question;
                        datas.add(liPeiModel);
                    }
                    LiPeiDataModel liPeiDataModel = new LiPeiDataModel(datas);
                    EventBus.getDefault().postSticky(liPeiDataModel);
                }
                break;

                case CheXianFragment.TIAO_KUAN:{
                    List<Insurance> insuranceList = DbHelper.getInstance().getDaoSession().getInsuranceDao().queryBuilder().list();
                    List<TiaoKuanModel> datas = new ArrayList<>();
                    for (Insurance insurance : insuranceList){
                        datas.add(insurance.getTiaokuanModel());
                    }
                    EventBus.getDefault().postSticky(new TiaoKuanDataModel(datas));
                }
                break;

                default:
                    break;

            }

            List<InsuranceCompany> insuranceCompanyList = DbHelper.getInstance().getDaoSession().getInsuranceCompanyDao().queryBuilder().where(InsuranceCompanyDao.Properties.Group.eq(world)).list();
            List<Qa> qaList = DbHelper.getInstance().getDaoSession().getQaDao().queryBuilder().where(QaDao.Properties.Category.eq(getReplaceMap().get(world))).list();

        }
    }

    public CheXianFragment cheXianFragment;

    public CheXianFragment getCheXianFragment(ShowtipPresent showtipPresent) {
        if (cheXianFragment == null){
            cheXianFragment = new CheXianFragment();
            cheXianFragment.mShowtipPresent = showtipPresent;
        }
        return cheXianFragment;
    }

    @Override
    public <T> Fragment getFragment(T datas) {
        cheXianFragment = getCheXianFragment((ShowtipPresent) datas);
//        cheXianFragment.setType(CheXianFragment.TIAO_KUAN);
        return cheXianFragment;
    }

    @Override
    public boolean canScroll() {
        return false;
    }

    @Override
    public boolean canScroll(int position) {
        if (position == 0 || position == 1 || position == 10 || position == 11){
            return true;
        }
        return false;
    }
}

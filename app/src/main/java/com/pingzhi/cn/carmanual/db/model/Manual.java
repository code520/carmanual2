package com.pingzhi.cn.carmanual.db.model;

import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

/**
 * Created by ${fanjie} on 2017/7/4.
 * 手册
 */
@Entity(createInDb = false, nameInDb = "gs8_manual")
public class Manual implements ToShowPagerModel{
    @Property(nameInDb = "id")
    @Id(autoincrement = true)
    public long id;

    @Property(nameInDb = "grp")
    public String group;

    public String parent;

    public String std_q;

    public String part;

    public String function;

    public String property;

    public String position;

    public String category;

    public String operate;


    public String condition;

    public String method;

    public String description;

    public String image;

    @Generated(hash = 2103797530)
    public Manual(long id, String group, String parent, String std_q, String part,
            String function, String property, String position, String category,
            String operate, String condition, String method, String description,
            String image) {
        this.id = id;
        this.group = group;
        this.parent = parent;
        this.std_q = std_q;
        this.part = part;
        this.function = function;
        this.property = property;
        this.position = position;
        this.category = category;
        this.operate = operate;
        this.condition = condition;
        this.method = method;
        this.description = description;
        this.image = image;
    }

    @Generated(hash = 1094905587)
    public Manual() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroup() {
        return this.group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getFunction() {
        return this.function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getPosition() {
        return this.position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCondition() {
        return this.condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getMethod() {
        return this.method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public ShowPagerModel getShowPagerModel(AIAnswerModel aiAnswerModel) {
        ShowPagerModel showPagerModel = new ShowPagerModel();
        showPagerModel.uri = Constant.imageDir + image;
//        showPagerModel.question = aiAnswerModel.getText();
//        showPagerModel.content = aiAnswerModel.getAnswer().getResult().getData();
        showPagerModel.type = Constant.CHANG_SHI;
        return showPagerModel;
    }

    @Override
    public ShowPagerModel getShowPagerModel() {
        ShowPagerModel showPagerModel = new ShowPagerModel();
        showPagerModel.uri = Constant.imageDir + image;
        showPagerModel.question = std_q;
        showPagerModel.content = this.description;
        showPagerModel.type = Constant.CHANG_SHI;
        return showPagerModel;
    }

    public String getParent() {
        return this.parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getPart() {
        return this.part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getProperty() {
        return this.property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getOperate() {
        return this.operate;
    }

    public void setOperate(String operate) {
        this.operate = operate;
    }

    public String getStd_q() {
        return this.std_q;
    }

    public void setStd_q(String std_q) {
        this.std_q = std_q;
    }
}

package com.pingzhi.cn.carmanual.voicemanager.recognizer;

import android.content.Context;

/**
 * Created by ${fanjie} on 2017/6/26.
 */

public class RecognizerContext implements RecognizerAble {

    private RecognizerAble onlineRecognizerManager;

    private RecognizerAble offlineRecognizerManager;

    private RecognizerAble currentRecognizerManager;

    private Context mContext;

    public RecognizerAble getOnlineRecognizerManager(Context context) {
        if (onlineRecognizerManager == null){
            onlineRecognizerManager = new OnlineRecognizerManager(context);
        }
        return onlineRecognizerManager;
    }

    public RecognizerAble getOfflineRecognizerManager(Context context) {
        if (offlineRecognizerManager == null){
            offlineRecognizerManager = new OfflineRecognizerManager(context);
        }
        return offlineRecognizerManager;
    }

    public RecognizerContext(Context context) {
        mContext = context.getApplicationContext();
        currentRecognizerManager = getOnlineRecognizerManager(mContext);
    }

    @Override
    public void startRecord() {
        currentRecognizerManager.startRecord();
    }

    @Override
    public void stoprecord() {
        currentRecognizerManager.stoprecord();
    }

    @Override
    public void destory() {
        currentRecognizerManager.destory();
    }

    @Override
    public boolean isListening() {
        return currentRecognizerManager.isListening();
    }
}

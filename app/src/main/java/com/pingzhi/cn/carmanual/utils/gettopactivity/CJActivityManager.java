package com.pingzhi.cn.carmanual.utils.gettopactivity;

import android.app.Activity;

import java.lang.ref.WeakReference;

/**
 * Created by ${fanjie} on 16/9/18.
 */
public class CJActivityManager {
    //防止强引用使activity不能够被销毁
    private WeakReference<Activity> sCurrentActivityWeakRef;
    private CJActivityManager(){

    }

    public static CJActivityManager getInstance(){
        return NewInstance.manager;
    }

    public Activity getCurrentActivity(){
        Activity currentACtivity = null;
        if (sCurrentActivityWeakRef != null){
            currentACtivity = sCurrentActivityWeakRef.get();
        }
        return currentACtivity;
    }

    public void setCurrentActivity(Activity activity){
        sCurrentActivityWeakRef = new WeakReference<Activity>(activity);
    }

    private static class NewInstance{
        static CJActivityManager manager = new CJActivityManager();
    }
}

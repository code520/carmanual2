package com.pingzhi.cn.carmanual.secondpage.present;

import android.support.v4.app.Fragment;

import com.pingzhi.cn.carmanual.CARQUESTIONTYPE;
import com.pingzhi.cn.carmanual.Constant;

/**
 * Created by ${fanjie} on 2017/6/21.
 */

public class PresentContext implements BasePresent{

    private BasePresent mBasePresent;

    private BaoYangPresent mBaoYangPresent;

    public BaoYangPresent getBaoYangPresent() {
        if (mBaoYangPresent == null){
            mBaoYangPresent = new BaoYangPresent();
        }
        return mBaoYangPresent;
    }

    private ChangShiPresent mChangShiPresent;

    public ChangShiPresent getChangShiPresent() {
        if (mChangShiPresent == null){
            mChangShiPresent = new ChangShiPresent();
        }
        return mChangShiPresent;
    }

    private CheXianPresent mCheXianPresent;
    private CheXianPresent getCheXianPresent(){
        if (mCheXianPresent == null){
            mCheXianPresent = new CheXianPresent();
        }
        return mCheXianPresent;
    }

    private WeiXiuPresent mWeiXiuPresent;

    public WeiXiuPresent getWeiXiuPresent() {
        if (mWeiXiuPresent == null){
            mWeiXiuPresent = new WeiXiuPresent();
        }
        return mWeiXiuPresent;
    }

    public void setType(int type){
        switch (type){
            case Constant.BAO_YANG:{
                mBasePresent = getBaoYangPresent();
            }
            break;
            case Constant.CHANG_SHI:{
                mBasePresent = getChangShiPresent();
            }
            break;
            case Constant.CHE_XIAN:{
                mBasePresent = getCheXianPresent();
            }
            break;
            case Constant.WEI_XIU:{
                mBasePresent = getWeiXiuPresent();
            }
            break;
            case Constant.QA:{
                mBasePresent = getCheXianPresent();
            }
            break;

            default:
                break;
        }
    }

    public PresentContext() {
    }

    public PresentContext(@CARQUESTIONTYPE int type){
        switch (type){
            case Constant.BAO_YANG:{
                mBasePresent = getBaoYangPresent();
            }
            break;
            case Constant.CHANG_SHI:{
                mBasePresent = getChangShiPresent();
            }
            break;
            case Constant.CHE_XIAN:{
                mBasePresent = getCheXianPresent();
            }
            break;
            case Constant.WEI_XIU:{
                mBasePresent = getWeiXiuPresent();
            }
            break;
            case Constant.QA:{
                mBasePresent = getCheXianPresent();
            }
            break;

            default:
                break;
        }
    }

    @Override
    public String[] getTypeTitles() {
        return mBasePresent.getTypeTitles();
    }

    @Override
    public int[] getTypeIcons() {
        return mBasePresent.getTypeIcons();
    }

    @Override
    public int[] getSelectIcons() {
        return mBasePresent.getSelectIcons();
    }

    @Override
    public String getType() {
        return mBasePresent.getType();
    }

    @Override
    public void query(String world) {
        mBasePresent.query(world);
    }

    @Override
    public <T> Fragment getFragment(T datas) {
        return mBasePresent.getFragment(datas);
    }

    @Override
    public boolean canScroll() {
        return mBasePresent.canScroll();
    }

    @Override
    public boolean canScroll(int position) {
        return mBasePresent.canScroll(position);
    }

}

package com.pingzhi.cn.carmanual.firstpage.view.ownview;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.ownview.ScrollTextView;
import com.pingzhi.cn.carmanual.utils.ScreenUtils;

/**
 * Created by ${fanjie} on 2017/7/2.
 */

public class TipGrounp extends LinearLayout {
    public TipGrounp(Context context) {
        this(context, null);
    }

    public TipGrounp(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.tip, this, true);
        init(context, attrs);
        initView();
    }

    private ScrollTextView mScrollTextView;
    private TextView tipText;
    private void initView() {
        mScrollTextView = (ScrollTextView) findViewById(R.id.scroll_text);
        mScrollTextView.setText(text);
        tipText = (TextView) findViewById(R.id.tip_text);
        setBackgroundResource(R.drawable.tip_background);
        setPadding(ScreenUtils.dip2px(getContext(), 10), ScreenUtils.dip2px(getContext(), 5), ScreenUtils.dip2px(getContext(), 10), ScreenUtils.dip2px(getContext(), 5));
        tipText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnTipClickListener.onTipClick(v);
            }
        });
        tipText.setSingleLine();

    }

    private String text;
    private void init(Context context, AttributeSet attrs){
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TipGrounp);
        text = array.getString(R.styleable.TipGrounp_tip_content);
        array.recycle();
    }

    public void setContent(String text){
        mScrollTextView.setText(text);
    }

    public String getContent(){
        return mScrollTextView.getText().toString();
    }

    private OnTipClickListener mOnTipClickListener;

    public void setOnTipClickListener(OnTipClickListener onTipClickListener) {
        mOnTipClickListener = onTipClickListener;
    }

    public interface OnTipClickListener{
        void onTipClick(View view);
    }
}

package com.pingzhi.cn.carmanual.utils.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.pingzhi.cn.carmanual.utils.WifyChangeViewUtils;

public class NetworkConnectChangedReceiver extends BroadcastReceiver {

    private static final String TAG = "=NetwoChangedReceiver=";

    public NetworkConnectChangedReceiver() {

    }

    public void registerSelf(Context context){
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        filter.addAction("android.net.wifi.STATE_CHANGE");
        context.registerReceiver(this, filter);
    }
//    ViewGroup container;
//    NoWifyPage noWifyPage;
    private WifyChangeViewUtils mWifyChangeViewUtils;
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
//        Log.e(TAG, "onReceive: " + AppUtils.isBackground(context), null);
//        if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE") && !NetWorkRecongniseUtils.isNetworkConnected()){
//            mWifyChangeViewUtils = new WifyChangeViewUtils();
//            mWifyChangeViewUtils.show();
//        }

        if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE") && NetWorkRecongniseUtils.isNetworkConnected()){
//            mWifyChangeViewUtils.hide();
        }
    }
}

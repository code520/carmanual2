package com.pingzhi.cn.carmanual.utils.recycleclickhelper;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ${fanjie} on 17/4/13.
 */

public interface RecyclerViewItemLongPressUpListener {

    void onLognPressUp(RecyclerView recyclerView, View cell, int position);

}

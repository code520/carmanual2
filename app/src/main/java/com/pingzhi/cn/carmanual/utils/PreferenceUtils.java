package com.pingzhi.cn.carmanual.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.pingzhi.cn.carmanual.MainApplication;

/**
 * Created by ${fanjie} on 2017/6/27.
 */

public class PreferenceUtils {

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private static final String NAME = "PINGZHI";
    private PreferenceUtils(){
        mSharedPreferences = MainApplication.getAppContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public static PreferenceUtils getInstance(){
        return InstanceHolder.PREFERENCE_UTILS;
    }

    private static final String JPUSH_MESSAGE = "jpush_message";
    public void setJpushMessage(String text){
        putString(JPUSH_MESSAGE, text);
    }

    public String getJpushMessage(String defalut){
        return getString(JPUSH_MESSAGE, defalut);
    }


    private static final String UPLOADINFO = "uploadInfo";

    public boolean isUploadInfoSuccess(){
        return mSharedPreferences.getBoolean(UPLOADINFO, false);
    }

    public void setUploadInfoSuccess(){
        mEditor.putBoolean(UPLOADINFO, true).apply();
    }

    private static final String UID = "uid";

    public void setUID(long uid){
        putLong(UID, uid);
    }

    public long getUID(){
        return getLong(UID, 0);
    }

    private static final String IS_DOWN_IMG = "down_img";
    public void setDownloadImg(){
        putBool(IS_DOWN_IMG, false);
    }

    public boolean needDownloadImg(){
        return getBool(IS_DOWN_IMG, true);
    }

    private static final String VERSION_CODE = "version_code";
    public int getSqlVersion(){
        return getInt(VERSION_CODE, 0);
    }

    public void updateSqlVersion(){
        int version = getSqlVersion();
        putInt(VERSION_CODE, version + 1);
    }

    private static final String IMG_VERSION = "img_version";
    public int getImgVersion(){
        return getInt(IMG_VERSION, -1);
    }

    public void updateImgVersion(){
        int version = getImgVersion();
        putInt(IMG_VERSION, version + 1);
    }

    private void putInt(String key, int value){
        mEditor.putInt(key, value).apply();
    }

    private int getInt(String key, int defaltValue){
        return mSharedPreferences.getInt(key, defaltValue);
    }

    private void putBool(String key, boolean value){
        mEditor.putBoolean(key, value).apply();
    }

    private boolean getBool(String key, boolean defaultValue){
        return mSharedPreferences.getBoolean(key, defaultValue);
    }

    private void putLong(String key, long value){
        mEditor.putLong(key, value).apply();
    }

    private long getLong(String key, long deaultValue){
        return mSharedPreferences.getLong(key, deaultValue);
    }

    private void putString(String key, String str){
        mEditor.putString(key, str).apply();
    }

    private String getString(String key, String defaultStr){
        return mSharedPreferences.getString(key, defaultStr);
    }
    private static class InstanceHolder{
        private static final PreferenceUtils PREFERENCE_UTILS = new PreferenceUtils();
    }
}

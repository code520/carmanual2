package com.pingzhi.cn.carmanual.secondpage.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chechi.aiandroid.R;
import com.facebook.drawee.view.SimpleDraweeView;
import com.haozhang.lib.AnimatedRecordingView;
import com.pingzhi.cn.carmanual.CARQUESTIONTYPE;
import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.busmodel.AIAnswerListModel;
import com.pingzhi.cn.carmanual.busmodel.AIAnswerModel;
import com.pingzhi.cn.carmanual.busmodel.ActivityAIAnswerModel;
import com.pingzhi.cn.carmanual.busmodel.CurrentPagerModel;
import com.pingzhi.cn.carmanual.busmodel.NoDatasModel;
import com.pingzhi.cn.carmanual.present.ShowtipPresent;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.CheXianFragment;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;
import com.pingzhi.cn.carmanual.secondpage.present.PresentContext;
import com.pingzhi.cn.carmanual.secondpage.view.ownview.CircleMenuLayout;
import com.pingzhi.cn.carmanual.utils.OperatorUtils;
import com.pingzhi.cn.carmanual.utils.ViewGestuerHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class SecondActivity extends AppCompatActivity {

    private static final String TYPE = "TYPE";
    private static final String TAG = "SecondActivity";
    private static boolean shouldShowMenu = false;

    public static void start(Context context, @CARQUESTIONTYPE int type){
        Intent intent = new Intent(context, SecondActivity.class);
        intent.putExtra(TYPE, type);
        shouldShowMenu = true;
        context.startActivity(intent);
    }

    private static final String PAGERMODEL = "pagermodel";
    public static void start(Context context, ShowPagerModel showPagerModel){
        Intent intent = new Intent(context, SecondActivity.class);
        intent.putExtra(TYPE, showPagerModel.type);
        intent.putExtra(PAGERMODEL, showPagerModel);
        shouldShowMenu = false;
        context.startActivity(intent);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        EventBus.getDefault().register(this);
        initView();
        initData();
        initAction();
    }

    private void initAction() {
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SecondActivity.this.finish();
            }
        });

        homeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorUtils.stepToHome(SecondActivity.this);
            }
        });

        mCircleMenuLayout.setOnMenuItemClickListener(new CircleMenuLayout.OnMenuItemClickListener() {
            @Override
            public void itemClick(View view, int pos) {
                mShowtipPresent.stopVoice();
                if (mPresentContext.canScroll(pos)){
                    mCircleMenuLayout.scrollToCenter(pos);
                }
                mPresentContext.query(titles[pos]);
            }

            @Override
            public void itemCenterClick(View view) {

            }
        });

        slideView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operatorSwith();
            }
        });

        mRecordingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mShowtipPresent.isVoice()){
                    mShowtipPresent.stopVoice();
                    return;
                }

                if (mShowtipPresent.isRecord()){
                    mShowtipPresent.stopRecord();
                }else {
                    mShowtipPresent.startRecord();
                }
            }
        });
    }

    private PresentContext mPresentContext;
    private ShowtipPresent mShowtipPresent;
    private ViewGestuerHelper mGestuerHelper;
    private List<ShowPagerModel> datas = new ArrayList<>();
//    private MyPagerAdapter myPagerAdapter;
    private String[] titles;
    private void initData() {
        @CARQUESTIONTYPE int type = getIntent().getIntExtra(TYPE, Constant.CHANG_SHI);
        ShowPagerModel showPagerModel = getIntent().getParcelableExtra(PAGERMODEL);
//        type = Constant.CHE_XIAN;

        mPresentContext = new PresentContext(type);
        mShowtipPresent = new ShowtipPresent(mRecordingView);
        showFragment();
        if (mShowtipPresent != null && type == Constant.CHE_XIAN){
            hideNoData();
            if (!shouldShowMenu){
                CheXianFragment cheXianFragment = (CheXianFragment) mPresentContext.getFragment(mShowtipPresent);
                cheXianFragment.setType(CheXianFragment.CHE_XIAN_QA);
            }
        }
        titles = mPresentContext.getTypeTitles();
        if (showPagerModel != null){
            EventBus.getDefault().postSticky(new AIAnswerModel(showPagerModel));
        }else {
//            mPresentContext.query(titles[0]);
        }
        mCircleMenuLayout.setCanScroll(mPresentContext.canScroll());
        mGestuerHelper = new ViewGestuerHelper(mRoot, new ViewGestuerHelper.OpenMenuAble() {
            @Override
            public void openMenu() {
                if (!mCircleMenuLayout.isOpen()){
                    mCircleMenuLayout.open();
                }
            }
        });
        mCircleMenuLayout.setMenuItemIconsAndTexts(mPresentContext.getTypeIcons(), mPresentContext.getSelectIcons(), titles);
        if (shouldShowMenu){
            mCircleMenuLayout.open();
        }else {
            hideNoData();
        }
        mtitle.setText(mPresentContext.getType());
//        for (int i = 0; i < 10; i++){
//            ShowPagerModel showPagerModel = new ShowPagerModel();
//            showPagerModel.question = "question";
//            showPagerModel.content = "answer";
//            showPagerModel.uri = "file://"+ getDir("carmanual", MODE_PRIVATE) + "/" + "timg.jpg";
//            Log.e(TAG, "initData: " + showPagerModel.uri, null);
//            datas.add(showPagerModel);
//        }

//        myPagerAdapter = new MyPagerAdapter(datas);
//        mViewPager.setAdapter(myPagerAdapter);
//        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                Log.e(TAG, "onPageScrolled: " + position, null);
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                Log.e(TAG, "onPageSelected: " + position, null);
//                mShowtipPresent.stopRecord();
//                mShowtipPresent.stopVoice();
//                mShowtipPresent.startVoiceWithText(datas.get(position).content);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
    }

    private void hideNoData(){
        mConstraintSet.clone(mRoot);
        mConstraintSet.setVisibility(R.id.textView20, ConstraintSet.INVISIBLE);
        mConstraintSet.applyTo(this.mRoot);
    }

    private void showNoData(){
        mConstraintSet.clone(mRoot);
        mConstraintSet.setVisibility(R.id.textView20, ConstraintSet.VISIBLE);
        mConstraintSet.applyTo(this.mRoot);
    }

    private CircleMenuLayout mCircleMenuLayout;
    private ImageView huaTongImg;
    private ImageView backImg;
    private ImageView homeImg;
    private SimpleDraweeView mSimpleDraweeView;
    private TextView mtitle;
//    private ViewPager mViewPager;
    private AnimatedRecordingView mRecordingView;
    private ConstraintLayout mRoot;
    private ImageView slideView;
    private ImageView leftImage;
    private ImageView rightImage;
    private TextView nodatasText;
    private ConstraintSet mConstraintSet = new ConstraintSet();

    private void initView() {

        mCircleMenuLayout = (CircleMenuLayout) findViewById(R.id.id_second_circle_menulayout);
        huaTongImg = (ImageView) findViewById(R.id.id_second_huatong);
        backImg = (ImageView) findViewById(R.id.id_second_back);
        homeImg = (ImageView) findViewById(R.id.id_second_home);
//        mSimpleDraweeView = (SimpleDraweeView) findViewById(R.id.id_second_simple_draweeview);
        mtitle = (TextView) findViewById(R.id.id_second_title);
//        mViewPager = (ViewPager) findViewById(R.id.id_second_pager);
        mRecordingView = (AnimatedRecordingView) findViewById(R.id.id_second_voice_line);
        mRoot = (ConstraintLayout) findViewById(R.id.root);
        slideView = (ImageView) findViewById(R.id.slide);
        leftImage = (ImageView) findViewById(R.id.id_second_left_arror);
        rightImage = (ImageView) findViewById(R.id.id_second_right_arror);
        nodatasText = (TextView) findViewById(R.id.textView20);

//        mVoiceLineView.startRandow();
    }

    private Fragment mFragment;
    public void showFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (mFragment != mPresentContext.getFragment(mShowtipPresent)){
            mFragment = mPresentContext.getFragment(mShowtipPresent);
            transaction.replace(R.id.id_second_fragment, mFragment);
            transaction.commit();
        }
    }

    private void hideImage(){
        leftImage.setVisibility(View.INVISIBLE);
        rightImage.setVisibility(View.INVISIBLE);
    }

    private void showImage(){
        leftImage.setVisibility(View.VISIBLE);
        rightImage.setVisibility(View.VISIBLE);
    }

    private void operatorSwith(){
        if (mCircleMenuLayout.isOpen()){
            mCircleMenuLayout.close();
        }else {
            mCircleMenuLayout.open();
        }
    }

    @Subscribe
    public void handleCurrentPager(CurrentPagerModel currentPagerModel){
        mConstraintSet.clone(mRoot);
        if (currentPagerModel.totalpager <= 0){
            hideRightImg();
            hideLeftImg();
            mConstraintSet.setVisibility(R.id.textView20, ConstraintSet.VISIBLE);
            mConstraintSet.applyTo(this.mRoot);
            mShowtipPresent.startVoiceWithText("数据完善中，先看看其它分类吧。");
            return;
        }
        mConstraintSet.setVisibility(R.id.textView20, ConstraintSet.INVISIBLE);
        mConstraintSet.applyTo(this.mRoot);
        if (currentPagerModel.currentPager == 0){
            hideLeftImg();
        }else {
            showLeftImg();
        }

        if (currentPagerModel.currentPager == currentPagerModel.totalpager - 1){
            hideRightImg();
        }else {
            showRightImg();
        }
    }

    private void hideLeftImg(){
        leftImage.setVisibility(View.INVISIBLE);
    }

    private void showLeftImg(){
        leftImage.setVisibility(View.VISIBLE);
    }

    private void hideRightImg(){
        rightImage.setVisibility(View.INVISIBLE);
    }

    private void showRightImg(){
        rightImage.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void handleAIAnswer(ActivityAIAnswerModel aiAnswerModel){
//        datas.clear();
//        datas.add(aiAnswerModel.mShowPagerModel);
//        myPagerAdapter.notifyDataSetChanged();
//        mShowtipPresent.startVoiceWithText(datas.get(0).content);
        hideImage();
    }

    @Subscribe
    public void handleAIAnswerList(AIAnswerListModel aiAnswerList){
//        datas.clear();
//        datas.addAll(aiAnswerList.mShowPagerModelList);
//        myPagerAdapter.notifyDataSetChanged();
        if (aiAnswerList.mShowPagerModelList.size() > 0){
//            mShowtipPresent.startVoiceWithText(datas.get(0).content);
            if (aiAnswerList.mShowPagerModelList.size() > 1){
                showImage();
            }else {
                hideImage();
            }
        }else {
//            mShowtipPresent.stopVoice();
//            mShowtipPresent.stopRecord();
            hideImage();
        }
    }


    @Subscribe
    public void handleNoDatas(NoDatasModel noDatasModel){

    }

    private CheXianFragment cheXianFragment;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        @CARQUESTIONTYPE int type = getIntent().getIntExtra(TYPE, Constant.CHANG_SHI);
        ShowPagerModel showPagerModel = getIntent().getParcelableExtra(PAGERMODEL);
        mPresentContext.setType(type);
        showFragment();
        if (showPagerModel != null && type == Constant.CHE_XIAN){
            if (!shouldShowMenu){
                cheXianFragment = (CheXianFragment) mPresentContext.getFragment(mShowtipPresent);
                if (cheXianFragment.getType() != -1){
                    cheXianFragment.setFragment(CheXianFragment.CHE_XIAN_QA);
                }else {
                    cheXianFragment.setType(CheXianFragment.CHE_XIAN_QA);
                }
            }
        }
        if (showPagerModel != null){
            EventBus.getDefault().postSticky(new AIAnswerModel(showPagerModel));
        }
        mCircleMenuLayout.setCanScroll(mPresentContext.canScroll());
        mGestuerHelper = new ViewGestuerHelper(mRoot, new ViewGestuerHelper.OpenMenuAble() {
            @Override
            public void openMenu() {
                if (!mCircleMenuLayout.isOpen()){
                    mCircleMenuLayout.open();
                }
            }
        });
        titles = mPresentContext.getTypeTitles();
        mCircleMenuLayout.setMenuItemIconsAndTexts(mPresentContext.getTypeIcons(), mPresentContext.getSelectIcons(), titles);
        if (shouldShowMenu){
            mCircleMenuLayout.open();
        }else {
            mCircleMenuLayout.close();
            hideNoData();
        }
        mtitle.setText(mPresentContext.getType());
        hideImage();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mShowtipPresent.onResume();
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        mShowtipPresent.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mShowtipPresent.onDestroy();
        mRecordingView = null;
        mGestuerHelper.destroy();
        mGestuerHelper = null;
    }
}

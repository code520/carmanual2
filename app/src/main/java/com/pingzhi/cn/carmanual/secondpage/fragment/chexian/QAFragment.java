package com.pingzhi.cn.carmanual.secondpage.fragment.chexian;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.busmodel.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.fragment.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by ${fanjie} on 2017/7/14.
 */

public class QAFragment extends BaseFragment {

    private TextView title;

    private TextView content;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmemt_qa, container, false);
        title = (TextView) view.findViewById(R.id.textView16);
        content = (TextView) view.findViewById(R.id.textView17);
        EventBus.getDefault().register(this);
        return view;
    }

    @Subscribe(sticky = true)
    public void handleCheXianQA(AIAnswerModel cheXianQAModel){
//        if (cheXianQAModel.mShowPagerModel.changed == 0){
        if (cheXianQAModel.mShowPagerModel.type == Constant.CHE_XIAN){
            title.setText(cheXianQAModel.mShowPagerModel.question);
            content.setText(cheXianQAModel.mShowPagerModel.content);
            mShowtipPresent.startVoiceWithText(cheXianQAModel.mShowPagerModel.content);
            EventBus.getDefault().removeStickyEvent(cheXianQAModel);
        }
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
}

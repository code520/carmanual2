package com.pingzhi.cn.carmanual.secondpage.fragment.chexian;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.GonShiModel;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/7.
 */

public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.GonShiHolder> {

    private List<GonShiModel> datas;
    private LayoutInflater mLayoutInflater;

    public LayoutInflater getLayoutInflater(Context context) {
        if (mLayoutInflater == null){
            mLayoutInflater = LayoutInflater.from(context);
        }
        return mLayoutInflater;
    }

    public RecycleAdapter(List<GonShiModel> datas) {
        this.datas = datas;
    }

    @Override
    public GonShiHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = getLayoutInflater(parent.getContext()).inflate(R.layout.item_gonshi, parent, false);
        return new GonShiHolder(mView);
    }

    @Override
    public void onBindViewHolder(GonShiHolder holder, int position) {
        holder.setData(datas.get(position));
        holder.bindClick(datas.get(position));
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public class GonShiHolder extends RecyclerView.ViewHolder{

        private TextView name;
        private TextView kefuPhone;
        private TextView toubaoPhone;
        private TextView lipei;
        private TextView baoduan;
        private TextView baojia;
        public GonShiHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.textView7);
            kefuPhone = (TextView) itemView.findViewById(R.id.textView8);
            toubaoPhone = (TextView) itemView.findViewById(R.id.textView9);
            lipei = (TextView) itemView.findViewById(R.id.textView12);
            baoduan = (TextView) itemView.findViewById(R.id.textView4);
            baojia = (TextView) itemView.findViewById(R.id.textView5);
        }

        public void setData(GonShiModel gonShiModel){
            name.setText(gonShiModel.name);
            kefuPhone.setText(gonShiModel.kefuPhone);
            toubaoPhone.setText(gonShiModel.toubaoPhone);
        }

        public void bindClick(final GonShiModel gonShiModel){
            kefuPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    telephone(v.getContext(), gonShiModel.kefuPhone);
                }
            });

            toubaoPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    telephone(v.getContext(), gonShiModel.toubaoPhone);
                }
            });

            lipei.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stepToUrl(v.getContext(), gonShiModel.lipeiChaXunXiTong);
                }
            });

            baoduan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stepToUrl(v.getContext(), gonShiModel.baoDuanChaXunXiTong);
                }
            });

            baojia.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stepToUrl(v.getContext(), gonShiModel.cheXianBaoJiaoJiSuang);
                }
            });
        }


        private void telephone(Context context, String phone){
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+phone));
            context.startActivity(intent);
        }

        private void stepToUrl(Context context, String url){
            Uri uri = Uri.parse(url);
            Intent it = new Intent(Intent.ACTION_VIEW, uri);
            context.startActivity(it);
        }

    }
}

package com.pingzhi.cn.carmanual.callback;

import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.busmodel.NetWorkErrorModel;

import org.greenrobot.eventbus.EventBus;

import cn.fanjie.com.cjvolley.OnNetworkReturn;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public abstract class NetWorkCallbackSuccess<T> extends OnNetworkReturn<T> {

    @Override
    protected void onError(Exception e) {
        Constant.isMutibleVoice = false;
        EventBus.getDefault().post(new NetWorkErrorModel(e));
    }
}

package cn.fanjie.com.cjvolley;

import android.util.Log;

import com.android.volley.Request;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Objects;

import static android.content.ContentValues.TAG;

/**
 * Created by ${fanjie} on 16/10/3.
 */

public class GetClass {

    /**
     * 获取接口的泛型
     * */

    public static Class getSuperInterfacegenricType(Class clazz, int row, int index){
        Type[] types = clazz.getGenericInterfaces();
        if (types.length == 0){
            return Objects.class;
        }
        //在android里面不能用ParameterizedTypeImp, 其实是ParameterizedTypeImp这个对象
        if (types.length <= row || !(types[row] instanceof ParameterizedType) || ((ParameterizedType)types[row]).getActualTypeArguments().length <= index){
            return Objects.class;
        }

        Type[] params = ((ParameterizedType)types[row]).getActualTypeArguments();

        return (Class) params[index];
    }



    public static void main(String...args){
        Class clazz = new Operator<String>(){

            @Override
            public Request<String> operator(int cjMethod, String url, Map<String, String> params, OnNetworkReturn<String> callback) {
                return null;
            }
        }.getClass();
       Class clazz2 = getSuperInterfacegenricType(clazz, 0, 0);
        //sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl
        Log.e(TAG, "main: " + clazz2, null);
    }
    /**
     * 获取继承的类里面泛型
     * */
    public static Class<Object> getSuperClassGenricType(final Class clazz, final int index) {

        //返回表示此 Class 所表示的实体（类、接口、基本类型或 void）的直接超类的 Type。
        Type genType = clazz.getGenericSuperclass();

        if (!(genType instanceof ParameterizedType)) {
            return Object.class;
        }
        //返回表示此类型实际类型参数的 Type 对象的数组。
        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();

        if (index >= params.length || index < 0) {
            return Object.class;
        }
        if (!(params[index] instanceof Class)) {
            return Object.class;
        }

        return (Class) params[index];
    }


}

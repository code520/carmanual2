package com.pingzhi.cn.carmanual.helppage.model;

import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.utils.json.JsonUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.fanjie.com.cjvolley.NetworkFactory;
import cn.fanjie.com.cjvolley.OnNetworkReturn;

/**
 * Created by ${fanjie} on 2017/6/29.
 */

public class TalkModel {

    public static final String CHE_XIAN = "1";

    public static final String WEI_XIU = "2";

    public static final String BAO_YANG = "3";

    public static final String CHANG_SHI = "4";
    /**
     * code : 200
     * msg : success
     * content : [{"type":"1","talks":[{"id":765,"content":"双方事故非直接损失如何处理（折旧费误工费交通费精神损失）","updateDate":1498634213,"type":"1"},{"id":766,"content":"理赔最长时效？","updateDate":1498634213,"type":"1"},{"id":767,"content":"改装配件出险后如何理赔？","updateDate":1498634213,"type":"1"}]},{"type":"2","talks":[{"id":541,"content":"维修类的相关知识","updateDate":1498634200,"type":"2"},{"id":542,"content":"维修类的相关知识","updateDate":1498634200,"type":"2"},{"id":543,"content":"维修类的相关知识","updateDate":1498634200,"type":"2"}]},{"type":"3","talks":[{"id":1168,"content":"保养类的知识保养类的知识知识知识","updateDate":1498634467,"type":"3"},{"id":1169,"content":"保养类的知识保养类的知识知识知识","updateDate":1498634467,"type":"3"},{"id":1170,"content":"保养类的知识保养类的知识知识知识","updateDate":1498634467,"type":"3"}]},{"type":"4","talks":[{"id":1436,"content":"常识类的相关知识常识类的相关知识","updateDate":1498634477,"type":"4"},{"id":1437,"content":"常识类的相关知识常识类的相关知识","updateDate":1498634477,"type":"4"},{"id":1438,"content":"常识类的相关知识常识类的相关知识","updateDate":1498634477,"type":"4"}]}]
     */

    private int code;
    private String msg;
    private List<ContentBean> content;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ContentBean> getContent() {
        return content;
    }

    public void setContent(List<ContentBean> content) {
        this.content = content;
    }

    public static class ContentBean {
        /**
         * type : 1
         * talks : [{"id":765,"content":"双方事故非直接损失如何处理（折旧费误工费交通费精神损失）","updateDate":1498634213,"type":"1"},{"id":766,"content":"理赔最长时效？","updateDate":1498634213,"type":"1"},{"id":767,"content":"改装配件出险后如何理赔？","updateDate":1498634213,"type":"1"}]
         */

        private String type;
        private List<TalksBean> talks;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<TalksBean> getTalks() {
            return talks;
        }

        public void setTalks(List<TalksBean> talks) {
            this.talks = talks;
        }

        public static class TalksBean {
            /**
             * id : 765
             * content : 双方事故非直接损失如何处理（折旧费误工费交通费精神损失）
             * updateDate : 1498634213
             * type : 1
             */

            private int id;
            private String content;
            private int updateDate;
            private String type;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public int getUpdateDate() {
                return updateDate;
            }

            public void setUpdateDate(int updateDate) {
                this.updateDate = updateDate;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }
    }

    public void getTalkModel(){
        NetworkFactory.getInstance().request(Constant.RANDOM_TALK, null, new OnNetworkReturn<String>() {
            @Override
            protected void onSuccess(String data) {
                TalkModel talkModel = JsonUtils.parseObject(data, TalkModel.class);
                if (talkModel.code == 200){
                    List<ContentBean> lists = talkModel.content;
                    Map<String, String> talks = new HashMap<String, String>(4);
                    for (ContentBean content : lists){
                        String temp = "";
                        for (ContentBean.TalksBean talkBean : content.getTalks()){
                            temp += talkBean.content + "\n";
                        }
                        talks.put(content.getType(), temp);
                    }
                    com.pingzhi.cn.carmanual.busmodel.TalkModel talk = new com.pingzhi.cn.carmanual.busmodel.TalkModel();
                    talk.talkMap = talks;
                    EventBus.getDefault().post(talk);
                }
            }

            @Override
            protected void onError(Exception e) {

            }
        });
    }
}

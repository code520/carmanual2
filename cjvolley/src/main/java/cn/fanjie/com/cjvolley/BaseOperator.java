package cn.fanjie.com.cjvolley;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.lang.ref.WeakReference;
import java.util.Map;

/**
 * Created by ${fanjie} on 16/10/3.
 */

public abstract class BaseOperator<T> implements Operator {

    protected WeakReference<Response.ErrorListener> errorListener;

    protected WeakReference<Response.Listener> listener;

//    abstract Request<T> request(int method, String url, Map<String, String> params, OnNetworkReturn callback);
    abstract Request<T> requestWithNoReturnParams(int Method, String url, OnNetworkReturn callback);

    abstract Request<T> postWithParams(String url, Map<String, String> params, OnNetworkReturn callback);

    @Override
    public Request operator(int cjMethod, String url, Map params, final OnNetworkReturn callback) {
        errorListener = new WeakReference<Response.ErrorListener>(new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error);
            }
        });

        listener = new WeakReference<Response.Listener>(new Response.Listener<T>() {
            @Override
            public void onResponse(T response) {
                callback.onSuccess(response);
            }
        });

        if (params != null && cjMethod == Request.Method.POST){
            return postWithParams(url, params, callback);
        }

        if (params != null && cjMethod == Request.Method.GET){
            url = operatUrlWithParams(url, params);
        }
        return requestWithNoReturnParams(cjMethod, url, callback);
    }


    private String operatUrlWithParams(String url, Map<String, Object> params){
        url += "?";
        String tempParams = "";
        for (Map.Entry<String, Object> entry : params.entrySet()){
            tempParams += entry.getKey() + "=" + entry.getValue() + "&";
        }
        tempParams = tempParams.substring(0, tempParams.length() - 1);
        url += tempParams;
        Log.e("行车报告",url);
        return url;
    }

}




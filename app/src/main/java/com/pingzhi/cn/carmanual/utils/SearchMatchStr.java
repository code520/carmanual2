package com.pingzhi.cn.carmanual.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ${fanjie} on 2017/7/17.
 */

public class SearchMatchStr {

    public static List<String> getPhoneByCloseTag(String str, String tag){

        return match(str, tag, "((\\d|-)*)");
    }

    public static List<String> getURLByCloseTag(String str, String tag){
        return match(str, tag, "((http|ftp|https):\\/\\/[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?)");
    }

    private static List<String> match(String str, String tag, String match){
        List<String> strs = new ArrayList<>();
        String patten = "<" + tag + ">" + match + "</" + tag + ">";
        Pattern pattern = Pattern.compile(patten);
        Matcher matcher = pattern.matcher(str);
        int j = 0;
        while (matcher.find(j)){
            strs.add(matcher.group(1));
            j = matcher.end();
        }
        return strs;
    }
}

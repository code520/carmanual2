package com.pingzhi.cn.carmanual.secondpage.fragment.chexian.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.LiPeiAdapter;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.LiPeiModel;
import com.pingzhi.cn.carmanual.utils.ScreenUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/10.
 */

public class LiPeiPageradapter extends PagerAdapter {

    private List<LiPeiModel> datas;
    public List<List<LiPeiModel>> mLists = new ArrayList<>();

    public LiPeiPageradapter(List<LiPeiModel> datas) {
        this.datas = datas;
        int count = datas.size() / 3;
        for (int i = 0; i < count; i++){
            List<LiPeiModel> liPeiModelList = new ArrayList<>();
            liPeiModelList.add(datas.get(3 * i));
            liPeiModelList.add(datas.get(3 * i + 1));
            liPeiModelList.add(datas.get(3 * i + 2));
            mLists.add(liPeiModelList);
        }

        List<LiPeiModel> liPeiModelList = null;
        for (int i = 0; i < datas.size() % 3; i++){
            liPeiModelList = new ArrayList<>();
            liPeiModelList.add(datas.get(3 * count + i));
        }
        if (liPeiModelList != null){
            mLists.add(liPeiModelList);
        }

    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return mLists.size();
    }

    /**
     * Determines whether a page View is associated with a specific key object
     * as returned by {@link #instantiateItem(ViewGroup, int)}. This method is
     * required for a PagerAdapter to function properly.
     *
     * @param view   Page View to check for association with <code>object</code>
     * @param object Object to check for association with <code>view</code>
     * @return true if <code>view</code> is associated with the key object <code>object</code>
     */
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    private LayoutInflater mLayoutInflater;

    public LayoutInflater getLayoutInflater(Context context) {
        if (mLayoutInflater == null){
            mLayoutInflater = LayoutInflater.from(context);
        }
        return mLayoutInflater;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        RecyclerView recyclerView = (RecyclerView) getLayoutInflater(container.getContext()).inflate(R.layout.pager_item_lipei, container, false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(container.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        LiPeiAdapter liPeiAdapter = new LiPeiAdapter(mLists.get(position));
        recyclerView.setAdapter(liPeiAdapter);
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {

            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.set(0, ScreenUtils.dip2px(parent.getContext(), 5), 0, ScreenUtils.dip2px(parent.getContext(), 5));
            }
        });
        container.addView(recyclerView);
        recyclerView.setTag(mLists.get(position));
        return recyclerView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        if (view.getParent() == container){
            container.removeView(view);
        }
    }

    @Override
    public int getItemPosition(Object object) {
        View view = (View) object;
        if (!datas.contains(view.getTag())){
            return POSITION_NONE;
        }
        return super.getItemPosition(object);
    }

    @Override
    public void notifyDataSetChanged() {
        int count = datas.size() / 3;
        for (int i = 0; i < count; i++){
            List<LiPeiModel> liPeiModelList = new ArrayList<>();
            liPeiModelList.add(datas.get(3 * i));
            liPeiModelList.add(datas.get(3 * i + 1));
            liPeiModelList.add(datas.get(3 * i + 2));
            mLists.add(liPeiModelList);
        }

        List<LiPeiModel> liPeiModelList = null;
        for (int i = 0; i < datas.size() % 3; i++){
            liPeiModelList = new ArrayList<>();
            liPeiModelList.add(datas.get(3 * count + i));
        }
        if (liPeiModelList != null){
            mLists.add(liPeiModelList);
        }
        super.notifyDataSetChanged();
    }
}

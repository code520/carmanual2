package com.pingzhi.cn.carmanual.db.zipdownload;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import static android.content.Context.MODE_PRIVATE;

public class DownLoaderTask extends AsyncTask<Void, Integer, Long> {
	private final String TAG = "DownLoaderTask";
	private URL mUrl;
	private File mFile;
	private ProgressDialog mDialog;
	private int mProgress = 0;
	private ProgressReportingOutputStream mOutputStream;
	private Context mContext;
	private String fileName;
	private String out;
	public DownLoaderTask(String url, String out, Context context){
		super();
		if(context!=null){
			mDialog = new ProgressDialog(context);
			mDialog.setCancelable(false);
			mContext = context;
		}
		else{
			mDialog = null;
		}
		
		try {
			mUrl = new URL(url);
			fileName = new File(mUrl.getFile()).getName();
			mFile = new File(out, fileName);
			this.out = out;
			Log.d(TAG, "out="+out+", name="+fileName+",mUrl.getFile()="+mUrl.getFile());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		//super.onPreExecute();
		if(mDialog!=null){
			mDialog.setTitle("Downloading...");
			mDialog.setMessage(mFile.getName());
			mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mDialog.setOnCancelListener(new OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					cancel(true);
				}
			});
			mDialog.show();
		}
	}

	@Override
	protected Long doInBackground(Void... params) {
		// TODO Auto-generated method stub
		return download();
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		// TODO Auto-generated method stub
		//super.onProgressUpdate(values);
		if(mDialog==null)
			return;
		if(values.length>1){
			int contentLength = values[1];
			if(contentLength==-1){
				mDialog.setIndeterminate(true);
			}
			else{
				mDialog.setMax(contentLength);
			}
		}
		else{
			mDialog.setProgress(values[0].intValue());
		}
	}

	@Override
	protected void onPostExecute(Long result) {
		// TODO Auto-generated method stub
		//super.onPostExecute(result);
		if(mDialog!=null&&mDialog.isShowing()){
			mDialog.dismiss();
		}
		if(isCancelled())
			return;
//		((MainActivity)mContext).showUnzipDialog();
		doZipExtractorWork();
	}

	public void doZipExtractorWork(){
		ZipExtractorTask task = new ZipExtractorTask(mContext.getDir("carmanual", MODE_PRIVATE) + "/" + fileName, mContext.getDir("carmanual", MODE_PRIVATE) + "/", mContext, true);
		task.execute();
	}

	private long download(){
//		URLConnection connection = null;
//		int bytesCopied = 0;
//		try {
//			connection = mUrl.openConnection();
//			int length = connection.getContentLength();
//			if(mFile.exists()&&length == mFile.length()){
//				Log.d(TAG, "file "+mFile.getName()+" already exits!!");
//				return 0l;
//			}
//			mOutputStream = new ProgressReportingOutputStream(mFile);
//			publishProgress(0,length);
//			bytesCopied =copy(connection.getInputStream(),mOutputStream);
//			if(bytesCopied!=length&&length!=-1){
//				Log.e(TAG, "Download incomplete bytesCopied="+bytesCopied+", length"+length);
//			}
//			mOutputStream.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return bytesCopied;
		try {
			return download("test", "47.93.162.158", "ftp", 21, "12345flp", "img", fileName, "");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0l;
	}

	/**
	 * 从ftp服务器下载文件 根目录/download/3.sql
	 *
	 * @param logSeq
	 * @param ftpUrl
	 *            ftp地址
	 * @param userName
	 *            用户名
	 * @param port
	 *            ftp服务的端口默认是21
	 * @param password
	 *            用户名
	 * @param directory
	 *            从ftp目录开始的文件夹 如download 如果不止一层文件夹请写成 "test/0703"
	 *            若写成"test"+File.Seperator+"0703"则会出错
	 * @param destFileName
	 *            目标文件的名字 如2.sql
	 * @param downloadName
	 *            存储到本地的文件的完整路径
	 *            如"E:"+File.separator+"2"+File.separator+"test75.sql"
	 * @return
	 * @throws IOException
	 */
	public  long download(String logSeq, String ftpUrl, String userName, int port, String password,
						  String directory, String destFileName, String downloadName) throws Exception {
		FTPClient ftpClient = new FTPClient();
		boolean result = false;
		try {
			ftpClient.connect(ftpUrl, port);
			ftpClient.login(userName, password);
//			ftpClient.enterLocalPassiveMode();
			ftpClient.enterLocalActiveMode();

			ftpClient.setBufferSize(1024);
			// 设置文件类型（二进制）
			ftpClient.changeWorkingDirectory(directory);
			ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
			long length = 0;
			FTPFile[] ftpFiles = ftpClient.listFiles(fileName);
			if (ftpFiles != null && ftpFiles.length > 0){
				length = ftpFiles[0].getSize();
			}
//			if(mFile.exists()&&length == mFile.length()){
//				Log.d(TAG, "file "+mFile.getName()+" already exits!!");
//				return false;
//			}


			System.out.println("destFileName:" + destFileName + ",downloadName:" + downloadName);
			try {
				mOutputStream = new ProgressReportingOutputStream(mFile);
				publishProgress(0, (int)length);
				result = ftpClient.retrieveFile(destFileName, mOutputStream);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("**result***:" + result);
			return length;
		} catch (NumberFormatException e) {
			throw e;
		} catch (FileNotFoundException e) {
			throw new FileNotFoundException();
		} catch (IOException e) {
			throw new Exception(e);
		} finally {
			try {
				ftpClient.disconnect();
			} catch (IOException e) {
				throw new RuntimeException("关闭FTP连接发生异常！", e);
			}
		}
	}
	private int copy(InputStream input, OutputStream output){
		byte[] buffer = new byte[1024*8];
		BufferedInputStream in = new BufferedInputStream(input, 1024*8);
		BufferedOutputStream out  = new BufferedOutputStream(output, 1024*8);
		int count =0,n=0;
		try {
			while((n=in.read(buffer, 0, 1024*8))!=-1){
				out.write(buffer, 0, n);
				count+=n;
			}
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return count;
	}
	private final class ProgressReportingOutputStream extends FileOutputStream {

		public ProgressReportingOutputStream(File file)
				throws FileNotFoundException {
			super(file);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void write(byte[] buffer, int byteOffset, int byteCount)
				throws IOException {
			// TODO Auto-generated method stub
			super.write(buffer, byteOffset, byteCount);
		    mProgress += byteCount;
		    publishProgress(mProgress);
		}
		
	}
}

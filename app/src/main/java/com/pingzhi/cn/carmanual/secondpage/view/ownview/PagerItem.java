package com.pingzhi.cn.carmanual.secondpage.view.ownview;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chechi.aiandroid.R;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

/**
 * Created by ${fanjie} on 2017/6/28.
 */

public class PagerItem extends LinearLayout {

    public PagerItem(Context context) {
        this(context, null);
    }

    public PagerItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
        LayoutInflater.from(context).inflate(R.layout.pager_item, this, true);
        mSimpleDraweeView = (SimpleDraweeView) findViewById(R.id.simpleDraweeView);
        mContent = (TextView) findViewById(R.id.content);
        mQuestion = (TextView) findViewById(R.id.question);
    }

    private SimpleDraweeView mSimpleDraweeView;
    private TextView mContent;
    private TextView mQuestion;
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    public void setItemData(ShowPagerModel showPagerModel){
        if (showPagerModel.uri != null){
            Uri uri = Uri.parse(showPagerModel.uri);
            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setUri(uri)
                    .setAutoPlayAnimations(true)
                    .build();
            mSimpleDraweeView.setController(controller);
            mSimpleDraweeView.setVisibility(VISIBLE);
        }else {
            mSimpleDraweeView.setVisibility(GONE);
        }
        mContent.setText(showPagerModel.content);
        mContent.setMovementMethod(new ScrollingMovementMethod());
        mQuestion.setText(showPagerModel.question);
    }


}

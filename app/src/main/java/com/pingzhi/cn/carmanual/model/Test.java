package com.pingzhi.cn.carmanual.model;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public class Test {
    /**
     * semantic : {"slots":{}}
     * text :
     * topic :
     * intent :
     * rc : 0
     * answer : {"result":{}}
     * data : {"result":["怎么调节安全带高度","怎么调节空调温度","如何提高巡航车速","节气门多长时间需要清洗","车辆使用多长时间需要换刹车片","机油机滤（小保养）的作用","汽滤有什么作用","不按周期更换空滤有什么隐患","座椅通风失灵怎么回事","踩油门变速箱明显抖动","急刹车有咔咔的响","人保车险的客服电话是多少","太平洋车险购买车险的电话是多少","平安车险保费计算网址是什么","交强险投保金额是多少","什么是车损险","车损险赔付限额是否累加计算","车身划痕险免赔率是多少"]}
     */

    private SemanticBean semantic;
    private String text;
    private String topic;
    private String intent;
    private int rc;
    private AnswerBean answer;
    private DataBean data;

    public SemanticBean getSemantic() {
        return semantic;
    }

    public void setSemantic(SemanticBean semantic) {
        this.semantic = semantic;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public int getRc() {
        return rc;
    }

    public void setRc(int rc) {
        this.rc = rc;
    }

    public AnswerBean getAnswer() {
        return answer;
    }

    public void setAnswer(AnswerBean answer) {
        this.answer = answer;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class SemanticBean {
        /**
         * slots : {}
         */

        private SlotsBean slots;

        public SlotsBean getSlots() {
            return slots;
        }

        public void setSlots(SlotsBean slots) {
            this.slots = slots;
        }

        public static class SlotsBean {
        }
    }

    public static class AnswerBean {
        /**
         * result : {}
         */

        private ResultBean result;

        public ResultBean getResult() {
            return result;
        }

        public void setResult(ResultBean result) {
            this.result = result;
        }

        public static class ResultBean {
        }
    }

    public static class DataBean {
        private List<String> result;

        public List<String> getResult() {
            return result;
        }

        public void setResult(List<String> result) {
            this.result = result;
        }
    }


    /**
     * semantic : {"slots":{}}
     * text :
     * topic :
     * intent :
     * rc : 0
     * answer : {"result":{}}
     * data : {"result":["怎么调节安全带高度","怎么调节空调温度","如何提高巡航车速","节气门多长时间需要清洗","车辆使用多长时间需要换刹车片","机油机滤（小保养）的作用","汽滤有什么作用","不按周期更换空滤有什么隐患","座椅通风失灵怎么回事","踩油门变速箱明显抖动","急刹车有咔咔的响","人保车险的客服电话是多少","太平洋车险购买车险的电话是多少","平安车险保费计算网址是什么","交强险投保金额是多少","什么是车损险","车损险赔付限额是否累加计算","车身划痕险免赔率是多少"]}
     */


}

package com.pingzhi.cn.carmanual.present;

import android.util.Log;

import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.MainApplication;
import com.pingzhi.cn.carmanual.model.UpLoadInfoModel;
import com.pingzhi.cn.carmanual.utils.AppUtils;
import com.pingzhi.cn.carmanual.utils.PreferenceUtils;
import com.pingzhi.cn.carmanual.utils.json.JsonUtils;

import java.util.HashMap;
import java.util.Map;

import cn.fanjie.com.cjvolley.NetworkFactory;
import cn.fanjie.com.cjvolley.OnNetworkReturn;
import cn.jpush.android.api.JPushInterface;

/**
 * Created by ${fanjie} on 2017/6/28.
 */

public class UploadUserInfo {

    private static final String TAG = "UploadUserInfo";
    public static void uploadInfo(){
        String jpushId = JPushInterface.getRegistrationID(MainApplication.getAppContext());
        String deviceId = AppUtils.getDeviceId();
        Map<String, Object> params = new HashMap<>();
        params.put("jpushid", jpushId);
        params.put("deviceId", deviceId);
        NetworkFactory.getInstance().request(Constant.UPLOAD_USERINFO, params, new OnNetworkReturn<String>() {
            @Override
            protected void onSuccess(String data) {
                UpLoadInfoModel upLoadInfoModel = JsonUtils.parseObject(data, UpLoadInfoModel.class);
                if (upLoadInfoModel.getCode() == 200 && "success".equals(upLoadInfoModel.getMsg())){
                    Log.e(TAG, "onSuccess: " + upLoadInfoModel.getContent().getId(), null);
                    PreferenceUtils.getInstance().setUID(upLoadInfoModel.getContent().getId());
                }
            }

            @Override
            protected void onError(Exception e) {

            }
        });
    }
}

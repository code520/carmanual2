package cn.fanjie.com.cjvolley;


import android.content.Context;

/**
 * Created by ${fanjie} on 16/10/3.
 */

public class NetworkFactory {

    private static Context mContext;

    public static void init(Context context){
        mContext = context;
    }

    public  static CJBaseNetworkInterface getInstance(){
        return ClassHolder.instance;
    }

    private static class ClassHolder{
        public static CJBaseNetworkInterface instance = new CJBaseNetworkImp(mContext);
    }
}

package com.pingzhi.cn.carmanual.utils.gettopactivity;

import android.app.Activity;

/**
 * Created by ${fanjie} on 16/10/10.
 */

public interface GetTopActivity {

//    void initInstance(Application application);

    Activity getTopActivity();
}

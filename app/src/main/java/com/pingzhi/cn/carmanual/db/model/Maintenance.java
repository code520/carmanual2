package com.pingzhi.cn.carmanual.db.model;

import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/4.
 * 保养
 * CREATE TABLE `gs8_maintenance` (
 `id` int(11) NOT NULL auto_increment,
 `grp` varchar(255) default NULL COMMENT '分组部件',
 `name` varchar(255) default NULL COMMENT '保养项目名称',
 `type` varchar(255) default NULL COMMENT '类型',
 `function` varchar(255) default NULL COMMENT '作用',
 `consequence` varchar(255) default NULL COMMENT '不按时保养的隐患',
 `period` varchar(255) default NULL COMMENT '保养周期',
 `image` varchar(255) default NULL COMMENT '图片',
 `function_stdq` varchar(255) default NULL COMMENT '作用标准问题',
 `consequence_stdq` varchar(255) default NULL,
 `period_stdq` varchar(255) default NULL,
 PRIMARY KEY  (`id`),
 UNIQUE KEY `id` (`id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='保养';
 */
@Entity(createInDb = false, nameInDb = "gs8_maintenance")
public class Maintenance implements ToShowPagerModel{
    @Property(nameInDb = "id")
    @Id(autoincrement = true)
    public long id;

    @Property(nameInDb = "grp")
    public String group;

    public String name;

    public String type;

    public String function;

    public String consequence;

    public String period;

    public String function_stdq;

    public String consequence_stdq;

    public String period_stdq;

    public String image;

    @Generated(hash = 1524938064)
    public Maintenance(long id, String group, String name, String type,
            String function, String consequence, String period,
            String function_stdq, String consequence_stdq, String period_stdq,
            String image) {
        this.id = id;
        this.group = group;
        this.name = name;
        this.type = type;
        this.function = function;
        this.consequence = consequence;
        this.period = period;
        this.function_stdq = function_stdq;
        this.consequence_stdq = consequence_stdq;
        this.period_stdq = period_stdq;
        this.image = image;
    }

    @Generated(hash = 1111935477)
    public Maintenance() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroup() {
        return this.group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFunction() {
        return this.function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getConsequence() {
        return this.consequence;
    }

    public void setConsequence(String consequence) {
        this.consequence = consequence;
    }

    public String getPeriod() {
        return this.period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public ShowPagerModel getShowPagerModel(AIAnswerModel aiAnswerModel) {
        ShowPagerModel showPagerModel = new ShowPagerModel();
        showPagerModel.uri = Constant.imageDir + aiAnswerModel.getResult().getImage();
        showPagerModel.question = aiAnswerModel.getSemantic().getText();
//        if ("function".equals(aiAnswerModel.getAnswer().getResult().getField())){
////            showPagerModel.question = function_stdq;
//            showPagerModel.content = function;
//        }else if ("consequence".equals(aiAnswerModel.getAnswer().getResult().getField())){
////            showPagerModel.question = consequence_stdq;
//            showPagerModel.content = consequence;
//        }else if ("period".equals(aiAnswerModel.getAnswer().getResult().getField())){
////            showPagerModel.question = period_stdq;
//            showPagerModel.content = period;
//        }
        showPagerModel.content = aiAnswerModel.getResult().getAnswer();
        showPagerModel.type = Constant.BAO_YANG;
        return showPagerModel;
    }

    @Override
    public ShowPagerModel getShowPagerModel() {
        ShowPagerModel showPagerModel = new ShowPagerModel();
        showPagerModel.uri = Constant.imageDir + image;
        if (function != null && !function.equals("")){
            showPagerModel.question = function_stdq;
            showPagerModel.content = function;
        }else if (consequence != null && !consequence.equals("")){
            showPagerModel.question = consequence_stdq;
            showPagerModel.content = consequence;
        }else {
            showPagerModel.question = period_stdq;
            showPagerModel.content = period;
        }
        showPagerModel.type = Constant.BAO_YANG;
        return showPagerModel;
    }

    public List<ShowPagerModel> getShowPagerModelList() {
        List<ShowPagerModel> showPagerModelList = new ArrayList<>();
        if (function != null && !function.equals("")){
            ShowPagerModel showPagerModel = new ShowPagerModel();
            showPagerModel.uri = Constant.imageDir + image;
            showPagerModel.question = function_stdq;
            showPagerModel.content = function;
            showPagerModel.type = Constant.BAO_YANG;
            showPagerModelList.add(showPagerModel);
        }
        if (consequence != null && !consequence.equals("")){
            ShowPagerModel showPagerModel = new ShowPagerModel();
            showPagerModel.uri = Constant.imageDir + image;
            showPagerModel.question = consequence_stdq;
            showPagerModel.content = consequence;
            showPagerModel.type = Constant.BAO_YANG;
            showPagerModelList.add(showPagerModel);
        }
        if (period != null && !period.equals("")){
            ShowPagerModel showPagerModel = new ShowPagerModel();
            showPagerModel.uri = Constant.imageDir + image;
            showPagerModel.question = period_stdq;
            showPagerModel.content = period;
            showPagerModel.type = Constant.BAO_YANG;
            showPagerModelList.add(showPagerModel);
        }

        return showPagerModelList;
    }

    public String getFunction_stdq() {
        return this.function_stdq;
    }

    public void setFunction_stdq(String function_stdq) {
        this.function_stdq = function_stdq;
    }

    public String getConsequence_stdq() {
        return this.consequence_stdq;
    }

    public void setConsequence_stdq(String consequence_stdq) {
        this.consequence_stdq = consequence_stdq;
    }

    public String getPeriod_stdq() {
        return this.period_stdq;
    }

    public void setPeriod_stdq(String period_stdq) {
        this.period_stdq = period_stdq;
    }
}

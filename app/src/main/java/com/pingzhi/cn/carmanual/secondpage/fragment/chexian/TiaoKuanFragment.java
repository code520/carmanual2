package com.pingzhi.cn.carmanual.secondpage.fragment.chexian;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.busmodel.CurrentPagerModel;
import com.pingzhi.cn.carmanual.busmodel.TiaoKuanDataModel;
import com.pingzhi.cn.carmanual.secondpage.fragment.BaseFragment;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.adapter.TiaoKuanPagerAdapter;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.TiaoKuanModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/9.
 */

public class TiaoKuanFragment extends BaseFragment {

//    private RecyclerView mRecyclerView;
    private List<TiaoKuanModel> datas = new ArrayList<>();
    private ViewPager mViewPager;
    private TiaoKuanPagerAdapter tiaoKuanPagerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tiao_kuan, container, false);
        mViewPager = (ViewPager) view;

//        for (int i = 0; i < 5; i++){
//            TiaoKuanModel tiaoKuanModel = new TiaoKuanModel();
//            tiaoKuanModel.title = "交强险";
//            tiaoKuanModel.define = "险种定义险种定义险种定义险种定义险种定义险种定义险种定义险种定义";
//            tiaoKuanModel.touBaoPrice = "家庭自用汽车6座以下小客车每年950，家庭自用汽车6座以下小客车每年950";
//            tiaoKuanModel.peiFuPrice = "当有责任时，无论责任如何划分：\n" +
//                    "把人撞死了交强险最多赔110,000元； 把人撞伤了交强险最多陪10,000元； 把别人的东西（车，物等）撞坏了交强险最多陪2000元，如果赔偿金额大于以上金额，那么由有三者险赔付，如果你没上三者险，那么只能自己掏钱把赔偿超出交强险限额的钱\n" +
//                    "无责任时，赔偿限额比有责任的低许多，其中死亡伤残赔偿的限额为11,000元，医疗费用赔偿的限额为1,000元，而财产损失赔偿的限额只有100元，最高的赔偿限额为1.21万元。实际赔付金额请与保险公司协商。";
//            tiaoKuanModel.priceAdd = "交强险对出险无数无限制";
//            tiaoKuanModel.mianPeiLv = "交强险不存在不计免赔的说法";
//            datas.add(tiaoKuanModel);
//        }

        tiaoKuanPagerAdapter = new TiaoKuanPagerAdapter(datas);
        mViewPager.setAdapter(tiaoKuanPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                startVoice(position);
                EventBus.getDefault().post(new CurrentPagerModel(position, datas.size()));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
//        mRecyclerView = (RecyclerView) view.findViewById(R.id.tiao_kuan_recycle);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
//        mRecyclerView.setLayoutManager(linearLayoutManager);
//        for (int i = 0; i < 5; i++){
//            TiaoKuanModel tiaoKuanModel = new TiaoKuanModel();
//            tiaoKuanModel.title = "险种定义";
//            tiaoKuanModel.content = "险种定义险种定义险种定义险种定义险种定义险种定义险种定义险种定义";
//            datas.add(tiaoKuanModel);
//        }
//        TiaoKuanPagerAdapter tiaoKuanAdapter = new TiaoKuanPagerAdapter(datas);
//        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
//            /**
//             * Draw any appropriate decorations into the Canvas supplied to the RecyclerView.
//             * Any content drawn by this method will be drawn before the item views are drawn,
//             * and will thus appear underneath the views.
//             *
//             * @param c      Canvas to draw into
//             * @param parent RecyclerView this ItemDecoration is drawing into
//             * @param state  The current state of RecyclerView
//             */
//            @Override
//            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
//                super.onDraw(c, parent, state);
//                drawHorizontalLine(c, parent, state);
//            }
//
//            /**
//             * Retrieve any offsets for the given item. Each field of <code>outRect</code> specifies
//             * the number of pixels that the item view should be inset by, similar to padding or margin.
//             * The default implementation sets the bounds of outRect to 0 and returns.
//             * <p>
//             * <p>
//             * If this ItemDecoration does not affect the positioning of item views, it should set
//             * all four fields of <code>outRect</code> (left, top, right, bottom) to zero
//             * before returning.
//             * <p>
//             * <p>
//             * If you need to access Adapter for additional data, you can call
//             * {@link RecyclerView#getChildAdapterPosition(View)} to get the adapter position of the
//             * View.
//             *
//             * @param outRect Rect to receive the output.
//             * @param view    The child view to decorate
//             * @param parent  RecyclerView this ItemDecoration is decorating
//             * @param state   The current state of RecyclerView.
//             */
//            @Override
//            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//                outRect.set(0, 0, 0, ScreenUtils.dip2px(view.getContext(), 1));
//            }
//
//            //画横线, 这里的parent其实是显示在屏幕显示的这部分
//            public void drawHorizontalLine(Canvas c, RecyclerView parent, RecyclerView.State state){
//                int left = parent.getPaddingLeft();
//                int right = parent.getWidth() - parent.getPaddingRight() - ScreenUtils.dip2px(getContext(), 5);
//                final int childCount = parent.getChildCount();
//                Paint paint = new Paint();
//                paint.setColor(Color.BLACK);
//                paint.setAntiAlias(true);
//                for (int i = 0; i < childCount; i++){
//                    final View child = parent.getChildAt(i);
//
//                    //获得child的布局信息
//                    final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)child.getLayoutParams();
//                    final int top = child.getBottom();
//                    final int bottom = top + ScreenUtils.dip2px(getContext(), 1);
//                    c.drawLine(left, top, right, bottom, paint);
//                }
//            }
//        });
//        mRecyclerView.setAdapter(tiaoKuanAdapter);
        EventBus.getDefault().register(this);
        return view;
    }

    private void startVoice(int position){
        TiaoKuanModel tiaoKuanModel = datas.get(position);
        String str = tiaoKuanModel.title;
        for (TiaoKuanModel.TiaoKuanItem item : tiaoKuanModel.getDatas()){
            str += item.toString();
        }
        mShowtipPresent.startVoiceWithText(str);
    }
    @Subscribe(sticky = true)
    public void handleTiaoKuan(TiaoKuanDataModel tiaoKuanDataModel){
        datas.clear();
        datas.addAll(tiaoKuanDataModel.datas);
        tiaoKuanPagerAdapter.notifyDataSetChanged();
        mViewPager.setCurrentItem(0, true);
        startVoice(0);
        EventBus.getDefault().removeStickyEvent(tiaoKuanDataModel);
        EventBus.getDefault().post(new CurrentPagerModel(0, datas.size()));
    }


    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
}

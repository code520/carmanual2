package com.pingzhi.cn.carmanual.voicemanager.wake;

import android.content.Context;

/**
 * Created by ${fanjie} on 2017/6/23.
 */

public class WakeContext implements WakeupAble {

    private WakeupAble onlineWakeManager;

    private WakeupAble offlineWakeManager;

    private WakeupAble currentWakeManager;

    private Context mContext;

    private WakeupAble getOnlineWakeManager(Context context) {
        if (onlineWakeManager == null){
            onlineWakeManager = new OnlineWakeManager(context);
        }
        return onlineWakeManager;
    }

    private WakeupAble getOfflineWakeManager(Context context) {
        if (offlineWakeManager == null){
            offlineWakeManager = new OfflineWakeManager(context);
        }
        return offlineWakeManager;
    }

    public WakeContext(Context context) {
        mContext = context.getApplicationContext();
        currentWakeManager = getOnlineWakeManager(mContext);
    }

    public void switchToOffline(){
        currentWakeManager = getOfflineWakeManager(mContext);
    }

    public void switchToOnline(){
        currentWakeManager = getOnlineWakeManager(mContext);
    }

    @Override
    public void registerListener(WakeUpCallback wakeUpCallback) {
        currentWakeManager.registerListener(wakeUpCallback);
    }

    @Override
    public void unRegisterListener() {
        currentWakeManager.unRegisterListener();
    }

    @Override
    public void wakeUpStart() {
        currentWakeManager.wakeUpStart();
    }

    @Override
    public void wakeUpStop() {
        currentWakeManager.wakeUpStop();
    }


}

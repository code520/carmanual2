package com.pingzhi.cn.carmanual.utils;

import com.pingzhi.cn.carmanual.ownview.NoWifyPage;
import com.pingzhi.cn.carmanual.utils.gettopactivity.CJTools;

/**
 * Created by ${fanjie} on 2017/6/28.
 */

public class WifyChangeViewUtils {

    private AddViewToFontUtils mAddViewToFontUtils;

    public WifyChangeViewUtils() {
        mAddViewToFontUtils = new AddViewToFontUtils();
    }

    public void show(){
        mAddViewToFontUtils.showView(new NoWifyPage(CJTools.getTopActivity()));
    }

    public void hide(){
        if (mAddViewToFontUtils != null){
            mAddViewToFontUtils.hideView();
        }
    }
}

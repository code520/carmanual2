package com.pingzhi.cn.carmanual.utils;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by ${fanjie} on 2017/6/19.
 */

public class ScreenUtils {

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static DisplayMetrics sDisplayMetrics;

    public static DisplayMetrics getDisplayMetrics(Context context) {
        if (sDisplayMetrics == null){
            sDisplayMetrics = context.getResources().getDisplayMetrics();
        }
        return sDisplayMetrics;
    }

    public static float getScreenWidth(Context context){
        return getDisplayMetrics(context).widthPixels;
    }

    public static float getScreenHeight(Context context){
        return getDisplayMetrics(context).heightPixels;
    }

}

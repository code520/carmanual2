package com.pingzhi.cn.carmanual.ownview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.busmodel.LastPager;
import com.pingzhi.cn.carmanual.model.NItemModel;
import com.pingzhi.cn.carmanual.utils.ScreenUtils;
import com.pingzhi.cn.carmanual.utils.StrUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/12.
 */

public class NItems extends LinearLayout {

    public NItems(Context context) {
        this(context, null);
    }

    private int count;
    public NItems(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initAttrs(context, attrs);
        initViews();
    }

    private ArrayList<TextView> mTextViewList;
    private ArrayList<View> mDividerList;
    private void initViews(){
        setOrientation(VERTICAL);
        datas = new ArrayList<>();
        mTextViewList = new ArrayList<>(count);
        mDividerList = new ArrayList<>(count);
        for (int i = 0; i < count; i++){
            TextView textView = new TextView(getContext());
            textView.setTag(i);
            textView.setGravity(Gravity.CENTER_VERTICAL);
            textView.setTextSize(20);
            LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, 0);
            params.weight = 1;
            addView(textView, params);
            mTextViewList.add(textView);
            View divider = new View(getContext());
            LayoutParams dividerParam = new LayoutParams(LayoutParams.MATCH_PARENT, ScreenUtils.dip2px(getContext(), 0.5f));
            divider.setBackgroundColor(Color.parseColor("#22FFFFFF"));
//            dividerParam.topMargin = ScreenUtils.dip2px(getContext(), 2);
            addView(divider, dividerParam);
            mDividerList.add(divider);
            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    setSelectItem(position);
                    mOnNItemClick.onItemClick(v, datas.get(currentPage * count + position).uploadTitle, position);
                }
            });
        }

    }

    private void resetAllItem() {
        for (int i = 0; i < count; i++){
            resetItemColor(i);
        }
    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.NItems);
        count = array.getInteger(R.styleable.NItems_item_count, 0);
        array.recycle();
    }

    private List<NItemModel> datas;

    public void setDatas(List<NItemModel> datas) {
        this.datas.clear();
        this.datas.addAll(datas);
        setCurrentPage(0);
        canNextPager();
    }

    public void clearData(){
        this.datas.clear();
    }

    public int contain(@NonNull String world){
        world = StrUtils.format(world);
        int start = currentPage * count;
        int end = (currentPage + 1) * count;
        if (end >= datas.size()){
            end = datas.size();
        }
        int index = -1;
        for (int i = start; i < end; i++){
            String title = datas.get(i).showTitle;
            if (title.length() <= 3){
                if (datas.get(i).showTitle.equals(world)){
                    index = i + 1 - start;
                    break;
                }
            }else {
                double simary = StrUtils.getSimilarity(title, world);
                if (simary >= 0.8){
                    index = i + 1 - start;
                    break;
                }
            }

        }
        return index;
    }



    private int currentPage;

    public boolean setCurrentPage(int currentPage) {
        if (!canSetPage(currentPage)){
            return false;
        }
        this.currentPage = currentPage;
        for (int i = 0; i < count; i++){
            int index = i + currentPage * count;
            if (index >= datas.size()){
                mDividerList.get(i).setVisibility(INVISIBLE);
                mTextViewList.get(i).setVisibility(INVISIBLE);
                mTextViewList.get(i).setClickable(false);
                continue;
            }
            String text = (i + 1) + "  " + datas.get(index).showTitle;
            mTextViewList.get(i).setText(text);
            mTextViewList.get(i).setVisibility(VISIBLE);
            mDividerList.get(i).setVisibility(VISIBLE);
            resetClickable(i);
            resetItemColor(i);
        }

        return true;
    }

    private boolean canSetPage(int currentPage){
        return currentPage * count < datas.size() && currentPage >= 0;
    }

    public boolean nextPager(){
        setCurrentPage(currentPage + 1);
        return canNextPager();
    }


    public boolean prePager(){
        boolean hasPre = canPrePager();
        if (hasPre){
            setCurrentPage(currentPage - 1);
        }
        return hasPre;
    }

    public boolean canPrePager(){
        boolean hasPre = canSetPage(currentPage - 1);
        return hasPre;
    }

    public boolean canNextPager(){
        boolean canNext = canSetPage(currentPage + 1);
        if (!canNext){
            LastPager lastPager = new LastPager();
            lastPager.currentPager = currentPage;
            EventBus.getDefault().post(lastPager);
        }
        return canNext;
    }

    public void setSelectItem(int item){
        resetAllItem();
        mTextViewList.get(item).setBackgroundColor(Color.parseColor("#15FFFFFF"));
        mTextViewList.get(item).setTextColor(Color.parseColor("#ed9239"));
    }

    public void setClickItemPosition(int position){
        if ((currentPage * count + position - 1) < datas.size()){
            setSelectItem(position - 1);
            mTextViewList.get(position - 1).performClick();
        }
    }

    private void resetItemColor(int item){

        mTextViewList.get(item).setBackgroundColor(Color.TRANSPARENT);
        mTextViewList.get(item).setTextColor(Color.WHITE);
    }

    private void resetClickable(int item){
        mTextViewList.get(item).setClickable(true);
    }

    public boolean isAdd(){
        return getParent() != null;
    }

    private OnNItemClick mOnNItemClick;

    public void setOnNItemClick(OnNItemClick onNItemClick) {
        mOnNItemClick = onNItemClick;
    }

    public interface OnNItemClick{
        void onItemClick(View view, String content, int position);
    }
}

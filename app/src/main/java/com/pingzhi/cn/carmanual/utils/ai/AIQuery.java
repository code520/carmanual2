package com.pingzhi.cn.carmanual.utils.ai;

import android.util.Base64;

import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.busmodel.AICanNotSolveModel;
import com.pingzhi.cn.carmanual.busmodel.AIMutualModel;
import com.pingzhi.cn.carmanual.busmodel.ActivityAIAnswerModel;
import com.pingzhi.cn.carmanual.callback.NetWorkCallbackSuccess;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.model.NItemModel;
import com.pingzhi.cn.carmanual.router.ActionOperatorFactory;
import com.pingzhi.cn.carmanual.router.ObtainActionOperatorAble;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;
import com.pingzhi.cn.carmanual.utils.PreferenceUtils;
import com.pingzhi.cn.carmanual.utils.json.JsonUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.fanjie.com.cjvolley.NetworkFactory;
import cn.fanjie.com.cjvolley.OnNetworkReturn;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public class AIQuery {
    private  ObtainActionOperatorAble<TableOperatorAble> obtainActionOperatorAble;

    private  TableOperatorAble getTableOperatorAble(String table){
        if (obtainActionOperatorAble == null){
            obtainActionOperatorAble = ActionOperatorFactory.getActionOperator(new TableRouter());
        }
        return obtainActionOperatorAble.getActionOperator(table);
    }
    //fs
    public  void queryWithQuestion(String answer, int fs){
        Map<String, Object> params = new HashMap<>();
        params.put("fs", fs);
        params.put("query", baseAndUTFEncoude(answer));
        params.put("uid", baseAndUTFEncoude(PreferenceUtils.getInstance().getUID() + ""));
        params.put("appid", "c6a2e15a69bac3c5899b2de58231fefc");
        NetworkFactory.getInstance().request(Constant.AI_QUESTION_QUERY, params, new NetWorkCallbackSuccess<String>() {
            @Override
            protected void onSuccess(String data) {
//                data = "{\"ac\": 0, \"semantic\": {\"text\": \"全景\", \"svb\": {\"object\": \"placeholder\", \"relation\": \"全景\", \"subject\": \"placeholder\"}, \"slots\": {\"category\": \"全景\", \"part\": \"天窗\", \"condition\": \"\", \"operate\": \"打开\"}, \"segment\": [{\"cont\": \"全景\", \"pos\": \"n\", \"ne\": \"O\"}], \"original\": \"全景\", \"revise\": \"全景\"}, \"rc\": 0, \"topic\": \"manual\", \"appendix\": {}, \"intent\": \"query\", \"result\": {\"image\": \"g26.gif\", \"method\": \"1、若想将天窗稍微打开，短促地按下开关1，天窗开始运动一小段距离后即停止运动。2、若想将天窗稍微关闭，短促地按下开关2，天窗开始运动一小段距离后即停止运动。3、如果想完全打开天窗，请用力按下开关1，天窗会打开至完全打开的位置。\"}, \"relation\": {\"answer\": [], \"question\": [], \"qtopic\": [], \"atype\": []}, \"ic\": 0, \"data\": {\"result\": {}, \"interactive\": {\"list\": [], \"uplist\": [], \"title\": \"\"}}, \"tips\": {}}";
                AIAnswerModel aiAnswer = JsonUtils.parseObject(data, AIAnswerModel.class);

                if (aiAnswer.getAc() == 0 && aiAnswer.getIc() == 0){
                   ShowPagerModel showPagerModel = getTableOperatorAble(aiAnswer.getTopic()).getPagerModelByTableAndIntent(aiAnswer);
                    EventBus.getDefault().post(new ActivityAIAnswerModel(showPagerModel));
                    Constant.isMutibleVoice = false;
                }else if (aiAnswer.getIc() == 1){
                    Constant.isMutibleVoice = true;
                    AIMutualModel aiMutualModel = new AIMutualModel();
                    List<String> showList = aiAnswer.getData().getInteractive().getList();
                    List<String> upList = aiAnswer.getData().getInteractive().getUplist();
                    List<NItemModel> datas = new ArrayList<NItemModel>();
                    for (int i = 0; i < showList.size(); i++){
                        String show = showList.get(i);
                        String up = upList.get(i);
                        NItemModel nItemModel = new NItemModel();
                        nItemModel.showTitle = show;
                        nItemModel.uploadTitle = up;
                        datas.add(nItemModel);
                    }
                    aiMutualModel.title = aiAnswer.getData().getInteractive().getTitle();
                    aiMutualModel.datas = datas;
                    EventBus.getDefault().post(aiMutualModel);
                } else if (aiAnswer.getRc() != 4){
                    AICanNotSolveModel aiCanNotSolveModel = new AICanNotSolveModel();
                    aiCanNotSolveModel.isRelativeToCar = true;
                    Constant.isMutibleVoice = false;
                    EventBus.getDefault().post(aiCanNotSolveModel);
                }else {
                    EventBus.getDefault().post(new AICanNotSolveModel());
                    Constant.isMutibleVoice = false;
                }
            }
        });
    }

    public void queryWithQuestion(String answer){
        queryWithQuestion(answer, 0);
    }

    public void cancleQuestion(){
        Map<String, Object> params = new HashMap<>();
        params.put("fs", 1);
        params.put("query", baseAndUTFEncoude(""));
        params.put("uid", baseAndUTFEncoude(PreferenceUtils.getInstance().getUID() + ""));
        params.put("appid", "c6a2e15a69bac3c5899b2de58231fefc");
        NetworkFactory.getInstance().request(Constant.AI_QUESTION_QUERY, params, new OnNetworkReturn<String>() {
            @Override
            protected void onSuccess(String data) {

            }

            @Override
            protected void onError(Exception e) {

            }
        });
    }


    public  String baseAndUTFEncoude(String content){
        final String base64Question = Base64.encodeToString(content.getBytes(), Base64.DEFAULT);
        String utf8Question = null;
        try {
            utf8Question = URLEncoder.encode(base64Question, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return utf8Question;
    }

}

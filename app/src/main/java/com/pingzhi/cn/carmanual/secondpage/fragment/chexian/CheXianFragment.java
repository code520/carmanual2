package com.pingzhi.cn.carmanual.secondpage.fragment.chexian;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.secondpage.fragment.BaseFragment;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.GonShiModel;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/7.
 */

public class CheXianFragment extends BaseFragment {

    public static final int GON_SHI = 0;

    public static final int LI_PEI = 1;

    public static final int TIAO_KUAN = 2;

    public static final int TOU_BAO = 3;

    public static final int CHE_XIAN_QA = 4;

    private int type = -1;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    private FrameLayout mFrameLayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFrameLayout = (FrameLayout) inflater.inflate(R.layout.fragment_che_xian, container, false);

        return mFrameLayout;
    }

    private BaseFragment mCurrentFragment;
    public void setFragment(int type){
        this.type = type;
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (mCurrentFragment == null){
            mCurrentFragment = getFragmentByType(type);
            fragmentTransaction.add(R.id.root_fragment, mCurrentFragment);
        }else {
            mCurrentFragment = getFragmentByType(type);
            fragmentTransaction.replace(R.id.root_fragment, mCurrentFragment);
        }
        fragmentTransaction.commit();
    }

    private LiPieFragment mLiPieFragment;

    public LiPieFragment getLiPieFragment() {
        if (mLiPieFragment == null){
            mLiPieFragment = new LiPieFragment();
            mLiPieFragment.mShowtipPresent = mShowtipPresent;
        }
        return mLiPieFragment;
    }

    private LiPieFragment mLiPieFragment2;

    public LiPieFragment getLiPieFragment2() {
        if (mLiPieFragment2 == null){
            mLiPieFragment2 = new LiPieFragment();
            mLiPieFragment2.mShowtipPresent = mShowtipPresent;
        }
        return mLiPieFragment2;
    }

    private TiaoKuanFragment mTiaoKuanFragment;

    public TiaoKuanFragment getTiaoKuanFragment() {
        if (mTiaoKuanFragment == null){
            mTiaoKuanFragment = new TiaoKuanFragment();
            mTiaoKuanFragment.mShowtipPresent = mShowtipPresent;
        }
        return mTiaoKuanFragment;
    }

    private GonShiFragment mGonShiFragment;

    public GonShiFragment getGonShiFragment() {
        if (mGonShiFragment == null){
            mGonShiFragment = new GonShiFragment();
            mGonShiFragment.mShowtipPresent = mShowtipPresent;
        }
        return mGonShiFragment;
    }

    private QAFragment mQAFragment;

    public QAFragment getQAFragment() {
        if (mQAFragment == null){
            mQAFragment = new QAFragment();
            mQAFragment.mShowtipPresent = mShowtipPresent;
        }
        return mQAFragment;
    }

    private BaseFragment getFragmentByType(int type){
        BaseFragment baseFragment = null;
        switch (type){
            case GON_SHI:{
                GonShiFragment gonShiFragment = getGonShiFragment();
                baseFragment = gonShiFragment;
//                gonShiFragment.setDatas(datas);
            }
            break;

            case LI_PEI:{
                baseFragment = getLiPieFragment();
            }
            break;

            case TIAO_KUAN:{
                baseFragment = getTiaoKuanFragment();
            }
            break;
            case TOU_BAO:{
                baseFragment = getLiPieFragment2();
            }
            break;
            case CHE_XIAN_QA:{
                baseFragment = getQAFragment();
            }
            break;
            default:
                break;
        }

        return baseFragment;
    }

    List<GonShiModel> datas;
    public void setRecycleDatas(List<GonShiModel> datas){
        this.datas = datas;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        EventBus.getDefault().register(this);
        if (type != -1){
            setFragment(type);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        EventBus.getDefault().unregister(this);
    }
}

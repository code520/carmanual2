package com.pingzhi.cn.carmanual.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by ${fanjie} on 2017/7/18.
 */

public class StrUtils {

    /**
     * 去标点
     * */
    public static String format(String s){
        String str=s.replaceAll("[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……& amp;*（）——+|{}【】‘；：”“’。，、？|-]", "");
        return str;
    }

    public static double getSimilarity(String doc1, String doc2) {
        if (doc1 != null && doc1.trim().length() > 0 && doc2 != null
                && doc2.trim().length() > 0) {

            Map<Integer, int[]> AlgorithmMap = new HashMap<Integer, int[]>();

            //将两个字符串中的中文字符以及出现的总数封装到，AlgorithmMap中
            for (int i = 0; i < doc1.length(); i++) {
                char d1 = doc1.charAt(i);
                if(isHanZi(d1)){
                    int charIndex = getGB2312Id(d1);
                    if(charIndex != -1){
                        int[] fq = AlgorithmMap.get(charIndex);
                        if(fq != null && fq.length == 2){
                            fq[0]++;
                        }else {
                            fq = new int[2];
                            fq[0] = 1;
                            fq[1] = 0;
                            AlgorithmMap.put(charIndex, fq);
                        }
                    }
                }
            }

            for (int i = 0; i < doc2.length(); i++) {
                char d2 = doc2.charAt(i);
                if(isHanZi(d2)){
                    int charIndex = getGB2312Id(d2);
                    if(charIndex != -1){
                        int[] fq = AlgorithmMap.get(charIndex);
                        if(fq != null && fq.length == 2){
                            fq[1]++;
                        }else {
                            fq = new int[2];
                            fq[0] = 0;
                            fq[1] = 1;
                            AlgorithmMap.put(charIndex, fq);
                        }
                    }
                }
            }

            Iterator<Integer> iterator = AlgorithmMap.keySet().iterator();
            double sqdoc1 = 0;
            double sqdoc2 = 0;
            double denominator = 0;
            while(iterator.hasNext()){
                int[] c = AlgorithmMap.get(iterator.next());
                denominator += c[0]*c[1];
                sqdoc1 += c[0]*c[0];
                sqdoc2 += c[1]*c[1];
            }

            return denominator / Math.sqrt(sqdoc1*sqdoc2);
        } else {
            throw new NullPointerException(
                    " the Document is null or have not cahrs!!");
        }
    }

    public static boolean isHanZi(char ch) {
        // 判断是否汉字
        return (ch >= 0x4E00 && ch <= 0x9FA5);

    }

    /**
     * 根据输入的Unicode字符，获取它的GB2312编码或者ascii编码，
     *
     * @param ch
     *            输入的GB2312中文字符或者ASCII字符(128个)
     * @return ch在GB2312中的位置，-1表示该字符不认识
     */
    public static short getGB2312Id(char ch) {
        try {
            byte[] buffer = Character.toString(ch).getBytes("GB2312");
            if (buffer.length != 2) {
                // 正常情况下buffer应该是两个字节，否则说明ch不属于GB2312编码，故返回'?'，此时说明不认识该字符
                return -1;
            }
            int b0 = (int) (buffer[0] & 0x0FF) - 161; // 编码从A1开始，因此减去0xA1=161
            int b1 = (int) (buffer[1] & 0x0FF) - 161; // 第一个字符和最后一个字符没有汉字，因此每个区只收16*6-2=94个汉字
            return (short) (b0 * 94 + b1);
        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return -1;
    }


//    /**
//
//     * 相似度转百分比
//
//     */
//
//    public static String similarityResult(double resule){
//
//        return  NumberFormat.getPercentInstance(new Locale( "en ", "US ")).format(resule);
//
//    }
//
//
//
//    /**
//
//     * 相似度比较
//
//     * @param strA
//
//     * @param strB
//
//     * @return
//
//     */
//
//    public static double SimilarDegree(String strA, String strB){
//
//        String newStrA = removeSign(strA);
//
//        String newStrB = removeSign(strB);
//
//        int temp = Math.max(newStrA.length(), newStrB.length());
//
//        int temp2 = longestCommonSubstring(newStrA, newStrB).length();
//
//        return temp2 * 1.0 / temp;
//
//    }
//
//
//
//    private static String removeSign(String str) {
//
//        StringBuffer sb = new StringBuffer();
//
//        for (char item : str.toCharArray())
//
//            if (charReg(item)){
//
//                //System.out.println("--"+item);
//
//                sb.append(item);
//
//            }
//
//        return sb.toString();
//    }
//
//
//
//    private static boolean charReg(char charValue) {
//
//        return (charValue >= 0x4E00 && charValue <= 0X9FA5)
//
//                || (charValue >= 'a' && charValue <= 'z')
//
//                || (charValue >= 'A' && charValue <= 'Z')
//
//                || (charValue >= '0' && charValue <= '9');
//
//    }
//
//
//
//    private static String longestCommonSubstring(String strA, String strB) {
//
//        char[] chars_strA = strA.toCharArray();
//
//        char[] chars_strB = strB.toCharArray();
//
//        int m = chars_strA.length;
//
//        int n = chars_strB.length;
//
//        int[][] matrix = new int[m + 1][n + 1];
//
//        for (int i = 1; i <= m; i++) {
//
//            for (int j = 1; j <= n; j++) {
//
//                if (chars_strA[i - 1] == chars_strB[j - 1])
//
//                    matrix[i][j] = matrix[i - 1][j - 1] + 1;
//
//                else
//
//                    matrix[i][j] = Math.max(matrix[i][j - 1], matrix[i - 1][j]);
//
//            }
//
//        }
//
//        char[] result = new char[matrix[m][n]];
//
//        int currentIndex = result.length - 1;
//
//        while (matrix[m][n] != 0) {
//
//            if (matrix[m][n] == matrix[m][n - 1])
//
//                n--;
//
//            else if (matrix[m][n] == matrix[m - 1][n])
//
//                m--;
//
//            else {
//
//                result[currentIndex] = chars_strA[m - 1];
//
//                currentIndex--;
//
//                n--;
//
//                m--;
//
//            }
//        }
//
//        return new String(result);
//    }

    public static String cn2Spell(String chinese) {
        StringBuffer pybf = new StringBuffer();
        char[] arr = chinese.toCharArray();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 128) {
                try {
                    pybf.append(PinyinHelper.toHanyuPinyinStringArray(arr[i], defaultFormat)[0]);
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
            } else {
                pybf.append(arr[i]);
            }
        }
        return pybf.toString();
    }

}

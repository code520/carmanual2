package com.pingzhi.cn.carmanual.secondpage.present;

import android.support.v4.app.Fragment;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.busmodel.AIAnswerListModel;
import com.pingzhi.cn.carmanual.db.DbHelper;
import com.pingzhi.cn.carmanual.db.model.Brokedown;
import com.pingzhi.cn.carmanual.db.model.BrokedownDao;
import com.pingzhi.cn.carmanual.present.ShowtipPresent;
import com.pingzhi.cn.carmanual.secondpage.fragment.guzhang.GuZhangFragment;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ${fanjie} on 2017/6/21.
 */

public class WeiXiuPresent implements BasePresent {
    @Override
    public String[] getTypeTitles() {
        return new String[]{
                "仪表台", "轮胎", "车联网", "钥匙", "方向盘", "发动机", "底盘", "其它", "座椅", "灯", "车顶"//, "天窗"
        };
    }

    @Override
    public int[] getTypeIcons() {
        return new int[]{
                R.mipmap.yibiaotai_, R.mipmap.luntai_, R.mipmap.lianwang_, R.mipmap.yaoshi_, R.mipmap.fangxiangp_, R.mipmap.fadongji_,
                R.mipmap.dipan_, R.mipmap.qita_, R.mipmap.zuoyi_, R.mipmap.chedeng_, R.mipmap.cheding_//, R.mipmap.tianchuang_
        };
    }

    @Override
    public int[] getSelectIcons() {
        return new int[]{
                R.mipmap.yibiaotai, R.mipmap.luntai, R.mipmap.lianwang, R.mipmap.yaoshi, R.mipmap.fangxiangpan, R.mipmap.fadongji,
                R.mipmap.dipan, R.mipmap.qita, R.mipmap.zuoyi, R.mipmap.chedeng, R.mipmap.cheding//, R.mipmap.tianchuang
        };
    }

    @Override
    public String getType() {
        return "故 障";
    }

    private Map<String, String> replaceTypes;



    @Override
    public void query(String world) {
        List<ShowPagerModel> datas = new ArrayList<>();
        List<Brokedown> brokedownList = DbHelper.getInstance().getDaoSession().getBrokedownDao().queryBuilder().where(BrokedownDao.Properties.Group.like("%" + world + "%")).list();
        for (int i = 0; i < brokedownList.size(); i++){
            Brokedown brokedown = brokedownList.get(i);
            datas.add(brokedown.getShowPagerModel());
        }
        EventBus.getDefault().postSticky(new AIAnswerListModel(datas));
    }

    private GuZhangFragment mGuZhangFragment;

    public GuZhangFragment getGuZhangFragment() {
        if (mGuZhangFragment == null){
            mGuZhangFragment = new GuZhangFragment();
        }
        return mGuZhangFragment;
    }

    @Override
    public <T> Fragment getFragment(T datas) {
        getGuZhangFragment().mShowtipPresent = (ShowtipPresent) datas;
        return getGuZhangFragment();
    }

    @Override
    public boolean canScroll() {
        return true;
    }

    @Override
    public boolean canScroll(int position) {
        return true;
    }
}

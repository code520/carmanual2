package com.pingzhi.cn.carmanual.db.model;

import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.GonShiModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;
import com.pingzhi.cn.carmanual.utils.SearchMatchStr;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/4.
 * 车险公司
 */
@Entity(createInDb = false, nameInDb = "gs8_insurance_company")
public class InsuranceCompany implements ToShowPagerModel{
    @Property(nameInDb = "id")
    @Id(autoincrement = true)
    public long id;

    @Property(nameInDb = "grp")
    public String group;

    public String name;

    public String service_line;//客服

    public String insurance_line;

    public String query_policy;//理赔

    public String query_compensate;//保单

    public String insurance_count;//车险报价计算

    public String image;

    @Generated(hash = 1263846207)
    public InsuranceCompany(long id, String group, String name, String service_line,
            String insurance_line, String query_policy, String query_compensate,
            String insurance_count, String image) {
        this.id = id;
        this.group = group;
        this.name = name;
        this.service_line = service_line;
        this.insurance_line = insurance_line;
        this.query_policy = query_policy;
        this.query_compensate = query_compensate;
        this.insurance_count = insurance_count;
        this.image = image;
    }

    @Generated(hash = 1027761734)
    public InsuranceCompany() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroup() {
        return this.group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getService_line() {
        return this.service_line;
    }

    public void setService_line(String service_line) {
        this.service_line = service_line;
    }

    public String getQuery_policy() {
        return this.query_policy;
    }

    public void setQuery_policy(String query_policy) {
        this.query_policy = query_policy;
    }

    public String getQuery_compensate() {
        return this.query_compensate;
    }

    public void setQuery_compensate(String query_compensate) {
        this.query_compensate = query_compensate;
    }

    public String getInsurance_count() {
        return this.insurance_count;
    }

    public void setInsurance_count(String insurance_count) {
        this.insurance_count = insurance_count;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public GonShiModel toGonShiModel(){
        GonShiModel gonShiModel = new GonShiModel();
        gonShiModel.name = name;
        return gonShiModel;
    }


    @Override
    public ShowPagerModel getShowPagerModel(AIAnswerModel aiAnswerModel) {
        ShowPagerModel showPagerModel = new ShowPagerModel();

        showPagerModel.question = aiAnswerModel.getSemantic().getText();
        showPagerModel.content = aiAnswerModel.getResult().getAnswer().replaceAll("<tel>", "").replaceAll("</tel>", "").replaceAll("<url>", "").replaceAll("</url>", "");
        showPagerModel.type = Constant.CHE_XIAN;
        return showPagerModel;
    }

    @Override
    public ShowPagerModel getShowPagerModel() {
        ShowPagerModel showPagerModel = new ShowPagerModel();

        return showPagerModel;
    }

    public GonShiModel getGonShiModel(){
        GonShiModel gonShiModel = new GonShiModel();
        gonShiModel.name = name;

        List<String> kefus = SearchMatchStr.getPhoneByCloseTag(service_line, "tel");
        if (kefus.size() > 0){
            gonShiModel.kefuPhone = kefus.get(0);
        }

        List<String> toubaos = SearchMatchStr.getPhoneByCloseTag(insurance_line, "tel");
        if (toubaos.size() > 0){
            gonShiModel.toubaoPhone = toubaos.get(0);
        }

        List<String> lipeis = SearchMatchStr.getURLByCloseTag(query_policy, "url");
        if (lipeis.size() > 0){
            gonShiModel.lipeiChaXunXiTong = lipeis.get(0);
        }

        List<String> baodans = SearchMatchStr.getURLByCloseTag(query_compensate, "url");
        if (baodans.size() > 0){
            gonShiModel.baoDuanChaXunXiTong = baodans.get(0);
        }

        List<String> chexians = SearchMatchStr.getURLByCloseTag(insurance_count, "url");
        if (chexians.size() > 0){
            gonShiModel.cheXianBaoJiaoJiSuang = chexians.get(0);
        }
        return gonShiModel;
    }


    public String getInsurance_line() {
        return this.insurance_line;
    }

    public void setInsurance_line(String insurance_line) {
        this.insurance_line = insurance_line;
    }
}

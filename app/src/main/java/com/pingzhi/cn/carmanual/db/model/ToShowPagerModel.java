package com.pingzhi.cn.carmanual.db.model;

import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public interface ToShowPagerModel {

    ShowPagerModel getShowPagerModel(AIAnswerModel aiAnswerModel);

    ShowPagerModel getShowPagerModel();
}

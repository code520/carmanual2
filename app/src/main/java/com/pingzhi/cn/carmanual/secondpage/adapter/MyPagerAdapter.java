package com.pingzhi.cn.carmanual.secondpage.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;
import com.pingzhi.cn.carmanual.secondpage.view.ownview.PagerItem;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/6/28.
 */

public class MyPagerAdapter extends PagerAdapter {

    private List<ShowPagerModel> datas;


    public MyPagerAdapter(List<ShowPagerModel> datas) {
        this.datas = datas;
    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return datas.size();
    }

    /**
     * Determines whether a page View is associated with a specific key object
     * as returned by {@link #instantiateItem(ViewGroup, int)}. This method is
     * required for a PagerAdapter to function properly.
     *
     * @param view   Page View to check for association with <code>object</code>
     * @param object Object to check for association with <code>view</code>
     * @return true if <code>view</code> is associated with the key object <code>object</code>
     */
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    /**
     * Create the page for the given position.  The adapter is responsible
     * for adding the view to the container given here, although it only
     * must ensure this is done by the time it returns from
     * {@link #finishUpdate(ViewGroup)}.
     *
     * @param container The containing View in which the page will be shown.
     * @param position  The page position to be instantiated.
     * @return Returns an Object representing the new page.  This does not
     * need to be a View, but can be some other container of the page.
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ShowPagerModel showPagerModel = datas.get(position);
        PagerItem pagerItem = new PagerItem(container.getContext());
        pagerItem.setItemData(showPagerModel);
        container.addView(pagerItem);
        pagerItem.setTag(showPagerModel);
        return pagerItem;
    }

    /**
     * Remove a page for the given position.  The adapter is responsible
     * for removing the view from its container, although it only must ensure
     * this is done by the time it returns from {@link #finishUpdate(ViewGroup)}.
     *
     * @param container The containing View from which the page will be removed.
     * @param position  The page position to be removed.
     * @param object    The same object that was returned by
     *                  {@link #instantiateItem(View, int)}.
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        View view = (View) object;
        if (view.getParent() == container){
            container.removeView(view);
        }
    }

    @Override public int getItemPosition(Object object) {
        View view = (View) object;
        if (!datas.contains(view.getTag())){
            return POSITION_NONE;
        }
        return super.getItemPosition(object);
    }
}

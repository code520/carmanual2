package com.pingzhi.cn.carmanual.db.model;

import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

/**
 * Created by ${fanjie} on 2017/7/24.
 */
@Entity(nameInDb = "gs8_manual_v2", createInDb = false)
public class ManualV2 implements ToShowPagerModel{

    @Property(nameInDb = "id")
    @Id(autoincrement = true)
    public long id;

    @Property(nameInDb = "grp")
    public String group;

    public String std_q;

    public long parent_grp;

    public long parent_default;

    public long part_grp;

    public long part_default;

    public String parent;

    public String part;

    public String function;

    public String property;

    public String position;

    public String category;

    public String operate;

    public String condition;

    public String method;

    public String attention;

    public String image;

    @Generated(hash = 175303943)
    public ManualV2(long id, String group, String std_q, long parent_grp,
            long parent_default, long part_grp, long part_default, String parent,
            String part, String function, String property, String position,
            String category, String operate, String condition, String method,
            String attention, String image) {
        this.id = id;
        this.group = group;
        this.std_q = std_q;
        this.parent_grp = parent_grp;
        this.parent_default = parent_default;
        this.part_grp = part_grp;
        this.part_default = part_default;
        this.parent = parent;
        this.part = part;
        this.function = function;
        this.property = property;
        this.position = position;
        this.category = category;
        this.operate = operate;
        this.condition = condition;
        this.method = method;
        this.attention = attention;
        this.image = image;
    }

    @Generated(hash = 681430022)
    public ManualV2() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroup() {
        return this.group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getStd_q() {
        return this.std_q;
    }

    public void setStd_q(String std_q) {
        this.std_q = std_q;
    }

    public long getParent_grp() {
        return this.parent_grp;
    }

    public void setParent_grp(long parent_grp) {
        this.parent_grp = parent_grp;
    }

    public long getParent_default() {
        return this.parent_default;
    }

    public void setParent_default(long parent_default) {
        this.parent_default = parent_default;
    }

    public long getPart_grp() {
        return this.part_grp;
    }

    public void setPart_grp(long part_grp) {
        this.part_grp = part_grp;
    }

    public long getPart_default() {
        return this.part_default;
    }

    public void setPart_default(long part_default) {
        this.part_default = part_default;
    }

    public String getParent() {
        return this.parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getPart() {
        return this.part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getFunction() {
        return this.function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getProperty() {
        return this.property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getPosition() {
        return this.position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getOperate() {
        return this.operate;
    }

    public void setOperate(String operate) {
        this.operate = operate;
    }

    public String getCondition() {
        return this.condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getMethod() {
        return this.method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getAttention() {
        return this.attention;
    }

    public void setAttention(String attention) {
        this.attention = attention;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public ShowPagerModel getShowPagerModel(AIAnswerModel aiAnswerModel) {
        ShowPagerModel showPagerModel = new ShowPagerModel();
        showPagerModel.uri = Constant.imageDir + image;
//        showPagerModel.question = aiAnswerModel.getText();
//        showPagerModel.content = aiAnswerModel.getAnswer().getResult().getMethod();
        showPagerModel.type = Constant.CHANG_SHI;
        return showPagerModel;
    }

    @Override
    public ShowPagerModel getShowPagerModel() {
        ShowPagerModel showPagerModel = new ShowPagerModel();
        showPagerModel.uri = Constant.imageDir + image;
        showPagerModel.question = std_q;
        showPagerModel.content = this.method;
        showPagerModel.type = Constant.CHANG_SHI;
        return showPagerModel;
    }
}

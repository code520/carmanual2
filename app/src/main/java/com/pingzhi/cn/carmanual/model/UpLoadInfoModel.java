package com.pingzhi.cn.carmanual.model;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/6/28.
 */

public class UpLoadInfoModel {


    /**
     * code : 200
     * msg : success
     * content : {"id":8425,"userName":"","phone":"","userImage":"","userGender":"保密","userCarType":"","userTags":[],"userType":"user","jpushId":"1300","monthCount":6,"mileage":0,"carTypeId":"","carTypeLogo":"","deviceModel":"","deviceOSName":"","deviceOSVersion":"","appVersion":"","selfRegister":"0","engine":null,"carNumber":null,"proviceCode":null,"cityCode":null,"carCity":null,"carCityCode":null,"carName":null,"frameNumber":null,"maintenanceDate":null,"maintenanceMileage":null,"deviceId":"1700"}
     */

    private int code;
    private String msg;
    private ContentBean content;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ContentBean getContent() {
        return content;
    }

    public void setContent(ContentBean content) {
        this.content = content;
    }

    public static class ContentBean {
        /**
         * id : 8425
         * userName :
         * phone :
         * userImage :
         * userGender : 保密
         * userCarType :
         * userTags : []
         * userType : user
         * jpushId : 1300
         * monthCount : 6
         * mileage : 0
         * carTypeId :
         * carTypeLogo :
         * deviceModel :
         * deviceOSName :
         * deviceOSVersion :
         * appVersion :
         * selfRegister : 0
         * engine : null
         * carNumber : null
         * proviceCode : null
         * cityCode : null
         * carCity : null
         * carCityCode : null
         * carName : null
         * frameNumber : null
         * maintenanceDate : null
         * maintenanceMileage : null
         * deviceId : 1700
         */

        private long id;
        private String userName;
        private String phone;
        private String userImage;
        private String userGender;
        private String userCarType;
        private String userType;
        private String jpushId;
        private int monthCount;
        private int mileage;
        private String carTypeId;
        private String carTypeLogo;
        private String deviceModel;
        private String deviceOSName;
        private String deviceOSVersion;
        private String appVersion;
        private String selfRegister;
        private Object engine;
        private Object carNumber;
        private Object proviceCode;
        private Object cityCode;
        private Object carCity;
        private Object carCityCode;
        private Object carName;
        private Object frameNumber;
        private Object maintenanceDate;
        private Object maintenanceMileage;
        private String deviceId;
        private List<?> userTags;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }

        public String getUserGender() {
            return userGender;
        }

        public void setUserGender(String userGender) {
            this.userGender = userGender;
        }

        public String getUserCarType() {
            return userCarType;
        }

        public void setUserCarType(String userCarType) {
            this.userCarType = userCarType;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getJpushId() {
            return jpushId;
        }

        public void setJpushId(String jpushId) {
            this.jpushId = jpushId;
        }

        public int getMonthCount() {
            return monthCount;
        }

        public void setMonthCount(int monthCount) {
            this.monthCount = monthCount;
        }

        public int getMileage() {
            return mileage;
        }

        public void setMileage(int mileage) {
            this.mileage = mileage;
        }

        public String getCarTypeId() {
            return carTypeId;
        }

        public void setCarTypeId(String carTypeId) {
            this.carTypeId = carTypeId;
        }

        public String getCarTypeLogo() {
            return carTypeLogo;
        }

        public void setCarTypeLogo(String carTypeLogo) {
            this.carTypeLogo = carTypeLogo;
        }

        public String getDeviceModel() {
            return deviceModel;
        }

        public void setDeviceModel(String deviceModel) {
            this.deviceModel = deviceModel;
        }

        public String getDeviceOSName() {
            return deviceOSName;
        }

        public void setDeviceOSName(String deviceOSName) {
            this.deviceOSName = deviceOSName;
        }

        public String getDeviceOSVersion() {
            return deviceOSVersion;
        }

        public void setDeviceOSVersion(String deviceOSVersion) {
            this.deviceOSVersion = deviceOSVersion;
        }

        public String getAppVersion() {
            return appVersion;
        }

        public void setAppVersion(String appVersion) {
            this.appVersion = appVersion;
        }

        public String getSelfRegister() {
            return selfRegister;
        }

        public void setSelfRegister(String selfRegister) {
            this.selfRegister = selfRegister;
        }

        public Object getEngine() {
            return engine;
        }

        public void setEngine(Object engine) {
            this.engine = engine;
        }

        public Object getCarNumber() {
            return carNumber;
        }

        public void setCarNumber(Object carNumber) {
            this.carNumber = carNumber;
        }

        public Object getProviceCode() {
            return proviceCode;
        }

        public void setProviceCode(Object proviceCode) {
            this.proviceCode = proviceCode;
        }

        public Object getCityCode() {
            return cityCode;
        }

        public void setCityCode(Object cityCode) {
            this.cityCode = cityCode;
        }

        public Object getCarCity() {
            return carCity;
        }

        public void setCarCity(Object carCity) {
            this.carCity = carCity;
        }

        public Object getCarCityCode() {
            return carCityCode;
        }

        public void setCarCityCode(Object carCityCode) {
            this.carCityCode = carCityCode;
        }

        public Object getCarName() {
            return carName;
        }

        public void setCarName(Object carName) {
            this.carName = carName;
        }

        public Object getFrameNumber() {
            return frameNumber;
        }

        public void setFrameNumber(Object frameNumber) {
            this.frameNumber = frameNumber;
        }

        public Object getMaintenanceDate() {
            return maintenanceDate;
        }

        public void setMaintenanceDate(Object maintenanceDate) {
            this.maintenanceDate = maintenanceDate;
        }

        public Object getMaintenanceMileage() {
            return maintenanceMileage;
        }

        public void setMaintenanceMileage(Object maintenanceMileage) {
            this.maintenanceMileage = maintenanceMileage;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public List<?> getUserTags() {
            return userTags;
        }

        public void setUserTags(List<?> userTags) {
            this.userTags = userTags;
        }
    }
}

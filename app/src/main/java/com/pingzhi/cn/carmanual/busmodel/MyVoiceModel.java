package com.pingzhi.cn.carmanual.busmodel;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public class MyVoiceModel {

    public String content;

    public MyVoiceModel(String content) {
        this.content = content;
    }

    public MyVoiceModel() {
    }
}

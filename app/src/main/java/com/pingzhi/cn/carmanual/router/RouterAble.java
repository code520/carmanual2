package com.pingzhi.cn.carmanual.router;

/**
 * Created by tusm on 17/5/12.
 */
public interface RouterAble<T> {

    void initRouter(RegisterOperator<T> registerOperator);
}

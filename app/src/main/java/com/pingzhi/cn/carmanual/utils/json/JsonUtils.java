package com.pingzhi.cn.carmanual.utils.json;

import com.alibaba.fastjson.JSON;

/**
 * Created by ${fanjie} on 2017/6/27.
 */

public class JsonUtils {

    public static <T> String toJsonString(T t){
        return JSON.toJSONString(t);
    }

    public static <T> T parseObject(String json, Class<T> clazz){
        return JSON.parseObject(json, clazz);
    }
}

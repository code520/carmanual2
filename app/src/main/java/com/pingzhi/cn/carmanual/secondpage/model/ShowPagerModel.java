package com.pingzhi.cn.carmanual.secondpage.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.pingzhi.cn.carmanual.model.AIAnswerModel;

/**
 * Created by ${fanjie} on 2017/6/28.
 */

public class ShowPagerModel implements Parcelable{

    public String uri;

    public String content;

    public String question;

    public int type;

    public int changed;

    public ShowPagerModel(AIAnswerModel aiAnswerModel){
//        content = aiAnswerModel.getAnswer().getResult().getData();
//        question = aiAnswerModel.getText();
    }

    public ShowPagerModel() {
    }

    protected ShowPagerModel(Parcel in) {
        uri = in.readString();
        content = in.readString();
        question = in.readString();
        type = in.readInt();
        changed = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uri);
        dest.writeString(content);
        dest.writeString(question);
        dest.writeInt(type);
        dest.writeInt(changed);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ShowPagerModel> CREATOR = new Creator<ShowPagerModel>() {
        @Override
        public ShowPagerModel createFromParcel(Parcel in) {
            return new ShowPagerModel(in);
        }

        @Override
        public ShowPagerModel[] newArray(int size) {
            return new ShowPagerModel[size];
        }
    };
}

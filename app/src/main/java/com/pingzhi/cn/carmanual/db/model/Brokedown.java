package com.pingzhi.cn.carmanual.db.model;

import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Property;

/**
 * Created by ${fanjie} on 2017/7/4.
 * 故障
 */
@Entity(createInDb = false, nameInDb = "gs8_fault")
public class Brokedown implements ToShowPagerModel{
    @Property(nameInDb = "id")
    @Id(autoincrement = true)
    public long id;

    @Property(nameInDb = "grp")
    public String group;

    public String query;

    public String part;

    public String phen;

    public String reason;

    public String method;

    public String image;

    public String kind;

    public String position;

    public String ope;

    public String weather;

    public String running;

    public String gear;

    public String speed;

    public String gav;

    public String start;

    public String road;

    @Generated(hash = 1318709318)
    public Brokedown(long id, String group, String query, String part, String phen,
            String reason, String method, String image, String kind,
            String position, String ope, String weather, String running,
            String gear, String speed, String gav, String start, String road) {
        this.id = id;
        this.group = group;
        this.query = query;
        this.part = part;
        this.phen = phen;
        this.reason = reason;
        this.method = method;
        this.image = image;
        this.kind = kind;
        this.position = position;
        this.ope = ope;
        this.weather = weather;
        this.running = running;
        this.gear = gear;
        this.speed = speed;
        this.gav = gav;
        this.start = start;
        this.road = road;
    }

    @Generated(hash = 666305755)
    public Brokedown() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroup() {
        return this.group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getPart() {
        return this.part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getPhen() {
        return this.phen;
    }

    public void setPhen(String phen) {
        this.phen = phen;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMethod() {
        return this.method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getKind() {
        return this.kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getPosition() {
        return this.position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getOpe() {
        return this.ope;
    }

    public void setOpe(String ope) {
        this.ope = ope;
    }

    public String getWeather() {
        return this.weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getRunning() {
        return this.running;
    }

    public void setRunning(String running) {
        this.running = running;
    }

    public String getGear() {
        return this.gear;
    }

    public void setGear(String gear) {
        this.gear = gear;
    }

    public String getSpeed() {
        return this.speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getGav() {
        return this.gav;
    }

    public void setGav(String gav) {
        this.gav = gav;
    }

    public String getStart() {
        return this.start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getRoad() {
        return this.road;
    }

    public void setRoad(String road) {
        this.road = road;
    }


    public ShowPagerModel getShowPagerModel(AIAnswerModel aiAnswerModel){
        ShowPagerModel showPagerModel = new ShowPagerModel();
        showPagerModel.uri = Constant.imageDir + aiAnswerModel.getResult().getImage();
        showPagerModel.question = aiAnswerModel.getSemantic().getText();
        showPagerModel.content = getContent(aiAnswerModel.getResult().getReason(), aiAnswerModel.getResult().getMethod());
        showPagerModel.type = Constant.WEI_XIU;
        return showPagerModel;
    }

    @Override
    public ShowPagerModel getShowPagerModel() {
        ShowPagerModel showPagerModel = new ShowPagerModel();
        showPagerModel.uri = Constant.imageDir + this.image;
        showPagerModel.question = this.query;
        showPagerModel.content = getContent(reason, method);
        showPagerModel.type = Constant.WEI_XIU;
        return showPagerModel;
    }

    private String getContent(String reason, String method){
        String[] reasons = reason.split(";");
        String[] methods = method.split(";");
        int count = reasons.length;
        String content = "";
        if (count == 1){
            content += "原因" + ":" + reasons[0] + ".\n";
            if (methods[0].contains("正常")){
                content += "属于" + methods[0];
            }else {
                content += "建议" + methods[0];
            }
            return content;
        }

        for (int i = 0; i < count; i++){
            content += "原因" + (i + 1)  + ":" + reasons[i] + ".\n";
            String method2 = methods[i];
            if (method2.contains("正常")){
                content += "属于" + method2;
            }else {
                content += "建议" + method2;
            }
            content += ".\n";
        }

        return content;
    }

}

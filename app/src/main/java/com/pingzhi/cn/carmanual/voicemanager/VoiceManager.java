package com.pingzhi.cn.carmanual.voicemanager;

import android.content.Context;
import android.util.Log;

import com.pingzhi.cn.carmanual.busmodel.HelloFinishModel;
import com.pingzhi.cn.carmanual.busmodel.NotStopModel;
import com.pingzhi.cn.carmanual.busmodel.RecordErrorModel;
import com.pingzhi.cn.carmanual.busmodel.RecordFinishModel;
import com.pingzhi.cn.carmanual.busmodel.RecordResultModel;
import com.pingzhi.cn.carmanual.busmodel.VoiceFinishModel;
import com.pingzhi.cn.carmanual.voicemanager.recognizer.RecognizerContext;
import com.pingzhi.cn.carmanual.voicemanager.voicesynthesizer.VoiceSynthesizerContext;
import com.pingzhi.cn.carmanual.voicemanager.wake.WakeContext;
import com.pingzhi.cn.carmanual.voicemanager.wake.WakeUpCallback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by ${fanjie} on 2017/6/23.
 */

public class VoiceManager {

    private static final int WAKE_STOP_RECODE = 0;

    private static final int WAKE_STOP_VOICE = 1;

    private static final int WAKE_STOP_VOICE_TYPE = 2;

    private static final int WAKE_START_RECORD_HELLO = 3;

    private static final int WAKE_START_RECORD_TEXT = 4;

    private int stopType = -1;

    private Context mContext;

    private WakeContext mWakeContext;

    private RecognizerContext mRecognizerContext;

    private VoiceSynthesizerContext mVoiceSynthesizerContext;

    public VoiceManager(Context context) {
        mContext = context.getApplicationContext();
        mWakeContext = new WakeContext(mContext);
        mRecognizerContext = new RecognizerContext(mContext);
        mVoiceSynthesizerContext = new VoiceSynthesizerContext(mContext);
        mWakeContext.registerListener(new MyWakeupCallback());
//        mWakeContext.wakeUpStart();
    }

    public void startWakeup(){
        mWakeContext.wakeUpStart();
    }

    public void startRecord(){
        stopType = WAKE_STOP_RECODE;
        mWakeContext.wakeUpStop();
        mRecognizerContext.startRecord();
    }

    public void stoprecord(){
        mRecognizerContext.stoprecord();
        startWakeup();
    }

    private String text;
    public void startVoice(String text){
        stopType = WAKE_STOP_VOICE;
        this.text = text;
        mWakeContext.wakeUpStop();

        mVoiceSynthesizerContext.startVoice(text);
    }

    private int type;
    public void startVoice(String text, int type){
        stopType = WAKE_STOP_VOICE_TYPE;
        this.text = text;
        this.type = type;
        mWakeContext.wakeUpStop();

        mVoiceSynthesizerContext.startHelloVoice(text, type);
    }

    public void stopVoice(){
        mVoiceSynthesizerContext.stopVoice();
        startWakeup();
    }

    public boolean isSpeaking(){
        return mVoiceSynthesizerContext.isSpeaking();
    }

    public boolean isListening(){
        return mRecognizerContext.isListening();
    }

    private class MyWakeupCallback implements WakeUpCallback {

        @Override
        public void wakeupSuccess(String world, byte[] data, int offset, int length) {
//            Toast.makeText(mContext, "success", Toast.LENGTH_SHORT).show();
//            mRecognizerContext.startRecord();
            startRecordWithHello();
        }

        @Override
        public void wakeupEixt() {
            Log.e(TAG, "wakeupEixt: ", null);
//            switch (stopType){
//                case WAKE_STOP_RECODE:{
//                    mRecognizerContext.startRecord();
//                }
//                break;
//                case WAKE_STOP_VOICE:{
//                    mVoiceSynthesizerContext.startVoice(text);
//                }
//                break;
//                case WAKE_STOP_VOICE_TYPE:{
//                    mVoiceSynthesizerContext.startHelloVoice(text, type);
//                }
//                break;
//                case WAKE_START_RECORD_HELLO:{
//                    mVoiceSynthesizerContext.startHelloVoice("你好，请说");
//                }
//                break;
//                case WAKE_START_RECORD_TEXT:{
//                    mVoiceSynthesizerContext.startHelloVoice(text);
//                }
//                break;
//                default:
//                    break;
//            }
        }
    }



    public void startRecordWithHello(){
        EventBus.getDefault().post(new RecordResultModel("请说"));
        stopType = WAKE_START_RECORD_HELLO;
        text = "请说";
        mWakeContext.wakeUpStop();

        mVoiceSynthesizerContext.startHelloVoice("请说");
    }

//    @Subscribe
    public void handleNotStop(NotStopModel notStopModel){
        switch (stopType){
            case WAKE_STOP_RECODE:{
                mRecognizerContext.startRecord();
            }
            break;
            case WAKE_STOP_VOICE:{
                mVoiceSynthesizerContext.startVoice(text);
            }
            break;
            case WAKE_STOP_VOICE_TYPE:{
                mVoiceSynthesizerContext.startHelloVoice(text, type);
            }
            break;
            case WAKE_START_RECORD_HELLO:{
                mVoiceSynthesizerContext.startHelloVoice("请说");
            }
            break;
            case WAKE_START_RECORD_TEXT:{
                mVoiceSynthesizerContext.startHelloVoice(text);
            }
            break;
            default:
                break;
        }

    }

    public void startRecordWithHelloText(String text){
        EventBus.getDefault().post(new RecordResultModel(text));
        stopType = WAKE_START_RECORD_TEXT;
        this.text = text;
        mWakeContext.wakeUpStop();

        mVoiceSynthesizerContext.startHelloVoice(text);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleVoiceFinish(VoiceFinishModel voiceFinishModel){
        startWakeup();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleHelloFinish(HelloFinishModel helloFinishModel){
//        startRecord();
        mWakeContext.wakeUpStop();
//        mVoiceSynthesizerContext.stopVoice();
        mRecognizerContext.startRecord();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleRecordFinish(RecordFinishModel recordFinishModel){
        startWakeup();
    }

    private static final String TAG = "=VoiceManager=";
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleResultResult(RecordResultModel recordResultModel){
        Log.e(TAG, "handleResultResult: " + recordResultModel.content, null);
        startWakeup();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleRecordError(RecordErrorModel recordErrorModel){
        startWakeup();
    }

    public void onResume(){
        startWakeup();
        EventBus.getDefault().register(this);
    }

    public void onPause(){
        mVoiceSynthesizerContext.stopVoice();
        mRecognizerContext.stoprecord();
        mWakeContext.wakeUpStop();
        EventBus.getDefault().unregister(this);
    }

    public void desaroy(){
        mVoiceSynthesizerContext.destory();
        mRecognizerContext.destory();
        mWakeContext.unRegisterListener();
    }

}

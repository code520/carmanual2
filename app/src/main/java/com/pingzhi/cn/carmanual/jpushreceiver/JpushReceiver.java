package com.pingzhi.cn.carmanual.jpushreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.pingzhi.cn.carmanual.router.ActionOperatorFactory;
import com.pingzhi.cn.carmanual.router.ObtainActionOperatorAble;

public class JpushReceiver extends BroadcastReceiver {

    private static final String TAG = "JpushReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        String action = intent.getAction();
        Log.e(TAG, "onReceive: " + action, null);
        ObtainActionOperatorAble<JpushMsgReceiveable> operatorAble = ActionOperatorFactory.getActionOperator(new JpushMsgRouter());
        JpushMsgReceiveable receiveable = operatorAble.getActionOperator(action);
        receiveable.operatorMsg(context, intent);
    }
}

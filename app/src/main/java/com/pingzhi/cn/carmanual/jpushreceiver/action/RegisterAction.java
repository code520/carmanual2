package com.pingzhi.cn.carmanual.jpushreceiver.action;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.pingzhi.cn.carmanual.jpushreceiver.JpushMsgReceiveable;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by ${fanjie} on 2017/6/22.
 */

public class RegisterAction implements JpushMsgReceiveable {
    @Override
    public void operatorMsg(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        String title = bundle.getString(JPushInterface.EXTRA_REGISTRATION_ID);
        Log.e("RegisterAction", "operatorMsg: " + title, null);
    }
}

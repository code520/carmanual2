package com.pingzhi.cn.carmanual.model;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public class AIAnswerModel {

    /**
     * ac : 0有答案 1没有答案
     * semantic : {"text":"GS8的天窗怎么打开","svb":{"object":"placeholder","relation":"打开","subject":"天窗"},"slots":{"category":"","part":"天窗","condition":"","operate":"打开"},"segment":[{"cont":"GS8","pos":"n","ne":"MODEL"},{"cont":"天窗","pos":"n","ne":"PART"},{"cont":"怎么","pos":"r","ne":"O"},{"cont":"打开","pos":"v","ne":"O"}],"original":"GS8的天窗怎么打开","revise":"GS8天窗怎么打开"}
     * rc : 0正常
     * topic : manual
     * appendix : {}
     * intent : query
     * result : {"title":"","url":"","image":"","attention":"","reason":"","answer":"","method":""}
     * relation : {"answer":[],"question":[],"qtopic":[],"atype":[]}
     * ic : 1 交互  0正常
     * data : {"result":{},"interactive":{"list":["全景","非全景"],"uplist":["全景","非全景"],"title":"您的天窗是全景还是非全景天窗"}}
     * tips : {}
     */

    private int ac;
    private SemanticBean semantic;
    private int rc;
    private String topic;
    private AppendixBean appendix;
    private String intent;
    private ResultBean result;
    private RelationBean relation;
    private int ic;
    private DataBean data;
    private TipsBean tips;

    public int getAc() {
        return ac;
    }

    public void setAc(int ac) {
        this.ac = ac;
    }

    public SemanticBean getSemantic() {
        return semantic;
    }

    public void setSemantic(SemanticBean semantic) {
        this.semantic = semantic;
    }

    public int getRc() {
        return rc;
    }

    public void setRc(int rc) {
        this.rc = rc;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public AppendixBean getAppendix() {
        return appendix;
    }

    public void setAppendix(AppendixBean appendix) {
        this.appendix = appendix;
    }

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public RelationBean getRelation() {
        return relation;
    }

    public void setRelation(RelationBean relation) {
        this.relation = relation;
    }

    public int getIc() {
        return ic;
    }

    public void setIc(int ic) {
        this.ic = ic;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public TipsBean getTips() {
        return tips;
    }

    public void setTips(TipsBean tips) {
        this.tips = tips;
    }

    public static class SemanticBean {
        /**
         * text : GS8的天窗怎么打开
         * svb : {"object":"placeholder","relation":"打开","subject":"天窗"}
         * slots : {"category":"","part":"天窗","condition":"","operate":"打开"}
         * segment : [{"cont":"GS8","pos":"n","ne":"MODEL"},{"cont":"天窗","pos":"n","ne":"PART"},{"cont":"怎么","pos":"r","ne":"O"},{"cont":"打开","pos":"v","ne":"O"}]
         * original : GS8的天窗怎么打开
         * revise : GS8天窗怎么打开
         */

        private String text;
        private SvbBean svb;
        private SlotsBean slots;
        private String original;
        private String revise;
        private List<SegmentBean> segment;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public SvbBean getSvb() {
            return svb;
        }

        public void setSvb(SvbBean svb) {
            this.svb = svb;
        }

        public SlotsBean getSlots() {
            return slots;
        }

        public void setSlots(SlotsBean slots) {
            this.slots = slots;
        }

        public String getOriginal() {
            return original;
        }

        public void setOriginal(String original) {
            this.original = original;
        }

        public String getRevise() {
            return revise;
        }

        public void setRevise(String revise) {
            this.revise = revise;
        }

        public List<SegmentBean> getSegment() {
            return segment;
        }

        public void setSegment(List<SegmentBean> segment) {
            this.segment = segment;
        }

        public static class SvbBean {
            /**
             * object : placeholder
             * relation : 打开
             * subject : 天窗
             */

            private String object;
            private String relation;
            private String subject;

            public String getObject() {
                return object;
            }

            public void setObject(String object) {
                this.object = object;
            }

            public String getRelation() {
                return relation;
            }

            public void setRelation(String relation) {
                this.relation = relation;
            }

            public String getSubject() {
                return subject;
            }

            public void setSubject(String subject) {
                this.subject = subject;
            }
        }

        public static class SlotsBean {
            /**
             * category :
             * part : 天窗
             * condition :
             * operate : 打开
             */

            private String category;
            private String part;
            private String condition;
            private String operate;

            public String getCategory() {
                return category;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public String getPart() {
                return part;
            }

            public void setPart(String part) {
                this.part = part;
            }

            public String getCondition() {
                return condition;
            }

            public void setCondition(String condition) {
                this.condition = condition;
            }

            public String getOperate() {
                return operate;
            }

            public void setOperate(String operate) {
                this.operate = operate;
            }
        }

        public static class SegmentBean {
            /**
             * cont : GS8
             * pos : n
             * ne : MODEL
             */

            private String cont;
            private String pos;
            private String ne;

            public String getCont() {
                return cont;
            }

            public void setCont(String cont) {
                this.cont = cont;
            }

            public String getPos() {
                return pos;
            }

            public void setPos(String pos) {
                this.pos = pos;
            }

            public String getNe() {
                return ne;
            }

            public void setNe(String ne) {
                this.ne = ne;
            }
        }
    }

    public static class AppendixBean {
    }

    public static class ResultBean {
        /**
         * title :
         * url :
         * image :
         * attention :
         * reason :
         * answer :
         * method :
         */

        private String title;
        private String url;
        private String image;
        private String attention;
        private String reason;
        private String answer;
        private String method;
        private String category;

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getAttention() {
            return attention;
        }

        public void setAttention(String attention) {
            this.attention = attention;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }
    }

    public static class RelationBean {
        private List<?> answer;
        private List<?> question;
        private List<?> qtopic;
        private List<?> atype;

        public List<?> getAnswer() {
            return answer;
        }

        public void setAnswer(List<?> answer) {
            this.answer = answer;
        }

        public List<?> getQuestion() {
            return question;
        }

        public void setQuestion(List<?> question) {
            this.question = question;
        }

        public List<?> getQtopic() {
            return qtopic;
        }

        public void setQtopic(List<?> qtopic) {
            this.qtopic = qtopic;
        }

        public List<?> getAtype() {
            return atype;
        }

        public void setAtype(List<?> atype) {
            this.atype = atype;
        }
    }

    public static class DataBean {
        /**
         * result : {}
         * interactive : {"list":["全景","非全景"],"uplist":["全景","非全景"],"title":"您的天窗是全景还是非全景天窗"}
         */

        private ResultBeanX result;
        private InteractiveBean interactive;

        public ResultBeanX getResult() {
            return result;
        }

        public void setResult(ResultBeanX result) {
            this.result = result;
        }

        public InteractiveBean getInteractive() {
            return interactive;
        }

        public void setInteractive(InteractiveBean interactive) {
            this.interactive = interactive;
        }

        public static class ResultBeanX {
        }

        public static class InteractiveBean {
            /**
             * list : ["全景","非全景"]
             * uplist : ["全景","非全景"]
             * title : 您的天窗是全景还是非全景天窗
             */

            private String title;
            private List<String> list;
            private List<String> uplist;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public List<String> getList() {
                return list;
            }

            public void setList(List<String> list) {
                this.list = list;
            }

            public List<String> getUplist() {
                return uplist;
            }

            public void setUplist(List<String> uplist) {
                this.uplist = uplist;
            }
        }
    }

    public static class TipsBean {
    }

//
//    /**
//     * semantic : {"slots":{"property":"function","type":"PROJECT","name":"机油机滤"}}
//     * text : 机油机滤（小保养）有什么作用？
//     * topic : maintenance
//     * intent : query
//     * rc : 0
//     * answer : {"result":{"data":"机油：对发动机起到清洁、冷却、防锈、密封、抗氧化、缓冲的作用，良好的机油品质能避免发动机早期磨损，延长发动机寿命 机滤：过滤机油中的杂质（发动机运行时产生的铁屑等）保证机油纯度","id":5}}
//     */
//
//    private SemanticBean semantic;
//    private String text;
//    private String topic;
//    private String intent;
//    private int rc;
//    private int sc;
//
//    public int getSc() {
//        return sc;
//    }
//
//    public void setSc(int sc) {
//        this.sc = sc;
//    }
//
//    private AnswerBean answer;
//    private DataBean data;
//
//    public SemanticBean getSemantic() {
//        return semantic;
//    }
//
//    public void setSemantic(SemanticBean semantic) {
//        this.semantic = semantic;
//    }
//
//    public String getText() {
//        return text;
//    }
//
//    public void setText(String text) {
//        this.text = text;
//    }
//
//    public String getTopic() {
//        return topic;
//    }
//
//    public void setTopic(String topic) {
//        this.topic = topic;
//    }
//
//    public String getIntent() {
//        return intent;
//    }
//
//    public void setIntent(String intent) {
//        this.intent = intent;
//    }
//
//    public int getRc() {
//        return rc;
//    }
//
//    public void setRc(int rc) {
//        this.rc = rc;
//    }
//
//    public AnswerBean getAnswer() {
//        return answer;
//    }
//
//    public void setAnswer(AnswerBean answer) {
//        this.answer = answer;
//    }
//
//    public DataBean getData() {
//        return data;
//    }
//
//    public void setData(DataBean data) {
//        this.data = data;
//    }
//
//    public static class SemanticBean {
//        /**
//         * slots : {"property":"function","type":"PROJECT","name":"机油机滤"}
//         */
//
//        private SlotsBean slots;
//
//        public SlotsBean getSlots() {
//            return slots;
//        }
//
//        public void setSlots(SlotsBean slots) {
//            this.slots = slots;
//        }
//
//        public static class SlotsBean {
//            /**
//             * property : function
//             * type : PROJECT
//             * name : 机油机滤
//             */
//
//            private String property;
//            private String function;
//            private String category;
//            private String type;
//
//            public String getFunction() {
//                return function;
//            }
//
//            public void setFunction(String function) {
//                this.function = function;
//            }
//
//            public String getCategory() {
//                return category;
//            }
//
//            public void setCategory(String category) {
//                this.category = category;
//            }
//
//            public String getPart() {
//                return part;
//            }
//
//            public void setPart(String part) {
//                this.part = part;
//            }
//
//            public String getOperate() {
//                return operate;
//            }
//
//            public void setOperate(String operate) {
//                this.operate = operate;
//            }
//
//            private String name;
//            private String part;
//            private String operate;
//            private String position;
//
//            public String getPosition() {
//                return position;
//            }
//
//            public void setPosition(String position) {
//                this.position = position;
//            }
//
//            public String getProperty() {
//                return property;
//            }
//
//            public void setProperty(String property) {
//                this.property = property;
//            }
//
//            public String getType() {
//                return type;
//            }
//
//            public void setType(String type) {
//                this.type = type;
//            }
//
//            public String getName() {
//                return name;
//            }
//
//            public void setName(String name) {
//                this.name = name;
//            }
//        }
//    }
//
//    public static class AnswerBean {
//        /**
//         * result : {"data":"机油：对发动机起到清洁、冷却、防锈、密封、抗氧化、缓冲的作用，良好的机油品质能避免发动机早期磨损，延长发动机寿命 机滤：过滤机油中的杂质（发动机运行时产生的铁屑等）保证机油纯度","id":5}
//         */
//
//        private ResultBean result;
//
//        private int state;
//
//        public int getState() {
//            return state;
//        }
//
//        public void setState(int state) {
//            this.state = state;
//        }
//
//        public ResultBean getResult() {
//            return result;
//        }
//
//        public void setResult(ResultBean result) {
//            this.result = result;
//        }
//
//        public static class ResultBean {
//            /**
//             * data : 机油：对发动机起到清洁、冷却、防锈、密封、抗氧化、缓冲的作用，良好的机油品质能避免发动机早期磨损，延长发动机寿命 机滤：过滤机油中的杂质（发动机运行时产生的铁屑等）保证机油纯度
//             * id : 5
//             */
//
//            private String data;
//
//            public String getImage() {
//                return image;
//            }
//
//            public void setImage(String image) {
//                this.image = image;
//            }
//
//            private String image;
//            private String field;
//            private String reason;
//            private String method;
//
//            public String getReason() {
//                return reason;
//            }
//
//            public void setReason(String reason) {
//                this.reason = reason;
//            }
//
//            public String getMethod() {
//                return method;
//            }
//
//            public void setMethod(String methos) {
//                this.method = methos;
//            }
//
//            public String getData() {
//                return data;
//            }
//
//            public void setData(String data) {
//                this.data = data;
//            }
//
//
//            public String getField() {
//                return field;
//            }
//
//            public void setField(String field) {
//                this.field = field;
//            }
//        }
//    }
//
//    public static class DataBean {
//        private ResultBean result;
//
//        public ResultBean getResult() {
//            return result;
//        }
//
//        public void setResult(ResultBean result) {
//            this.result = result;
//        }
//    }
//
//        public static class ResultBean{
//            public String title;
//
//            public List<String> list;
//
//            public List<String> uplist;
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public List<String> getUplist() {
//                return uplist;
//            }
//
//            public void setUplist(List<String> uplist) {
//                this.uplist = uplist;
//            }
//
//            public List<String> getList() {
//                return list;
//            }
//
//            public void setList(List<String> list) {
//                this.list = list;
//            }
//        }
}

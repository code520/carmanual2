package com.pingzhi.cn.carmanual.busmodel;

import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.TiaoKuanModel;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/14.
 */

public class TiaoKuanDataModel {

    public List<TiaoKuanModel> datas;

    public TiaoKuanDataModel() {
    }

    public TiaoKuanDataModel(List<TiaoKuanModel> datas) {
        this.datas = datas;
    }
}

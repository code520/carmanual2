package com.pingzhi.cn.carmanual.ownview;

import android.view.View;
import android.view.ViewGroup;

/**
 * Created by ${fanjie} on 2017/6/29.
 */

public class MyItem {

    public View mView;

    public int mHeight;

    public int addHeight;

    public boolean needAnima;

    public void setHeight(int height) {
        mHeight = height;
        ViewGroup.LayoutParams params = mView.getLayoutParams();
        params.height = mHeight;
        mView.setLayoutParams(params);
    }

    public void setAddHeight(int height){
        mHeight = height + mHeight;
        ViewGroup.LayoutParams params = mView.getLayoutParams();
        params.height = mHeight;
        mView.setLayoutParams(params);
    }
}

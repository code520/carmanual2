package cn.fanjie.com.cjvolley;

import android.net.Uri;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by ${fanjie} on 16/10/3.
 */

public class JsonObjectRequestOperator extends BaseOperator {

    @Override
    Request postWithParams(final String url, final Map params, OnNetworkReturn callback) {
        return new JsonObjectRequest(Request.Method.POST, url, (Response.Listener)listener.get(), (Response.ErrorListener)errorListener.get()){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=" + getParamsEncoding();
            }

            @Override
            public byte[] getBody() {
                super.getBody();
                try {
                    String urlWithParams = appendParameter(url,params);
                    return urlWithParams == null ? null :urlWithParams.getBytes(PROTOCOL_CHARSET);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
    }

    @Override
    Request requestWithNoReturnParams(int Method, String url, OnNetworkReturn callback) {
        return new JsonObjectRequest(Method, url, (Response.Listener)listener.get(), (Response.ErrorListener)errorListener.get());
    }

    private String appendParameter(String url, Map<String, String> params) {
        Uri uri = Uri.parse(url);
        Uri.Builder builder = uri.buildUpon();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            builder.appendQueryParameter(entry.getKey(), entry.getValue());
        }
        String str = builder.build().getEncodedQuery();
        return str;
    }
}

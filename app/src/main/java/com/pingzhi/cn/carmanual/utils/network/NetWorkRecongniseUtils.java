package com.pingzhi.cn.carmanual.utils.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Parcel;

/**
 * Created by ${fanjie} on 17/3/16.
 */

public class NetWorkRecongniseUtils {

    private static ConnectivityManager connectivityManager;
    /**
     * 建议使用application的context避免造成内存泄露
     * */
    public static void init(Context context){
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public static boolean isNetworkConnected(){
        if (connectivityManager != null){
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info != null){
                return info.isAvailable();
            }
        }
        return false;
    }

    public static boolean isWifiConnected(){
        return isTypeNetWorkConnected(ConnectivityManager.TYPE_WIFI);
    }

    public static boolean isMobileConnected(){
        return isTypeNetWorkConnected(ConnectivityManager.TYPE_MOBILE);
    }

    public static int getConnectedType(){
        if (connectivityManager != null){
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable()){
                return networkInfo.getType();
            }
        }
        return -1;
    }

    private static boolean isTypeNetWorkConnected(int type){
        if (connectivityManager != null){
            NetworkInfo networkInfo = getNetWorkInfoByType(type);
            if (networkInfo != null){
                return networkInfo.isAvailable();
            }
        }
        return false;
    }

    private static NetworkInfo getNetWorkInfoByType(int netWorkInfoType){
        NetworkInfo networkInfo;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Parcel parcel = Parcel.obtain();
            parcel.writeInt(netWorkInfoType);
            Network network = Network.CREATOR.createFromParcel(parcel);
            networkInfo = connectivityManager.getNetworkInfo(network);
        }else {
            networkInfo = connectivityManager.getNetworkInfo(netWorkInfoType);
        }
        return networkInfo;
    }

    public static void destory(){
        connectivityManager = null;
    }
}

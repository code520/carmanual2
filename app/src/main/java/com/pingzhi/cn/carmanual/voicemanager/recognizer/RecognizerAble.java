package com.pingzhi.cn.carmanual.voicemanager.recognizer;

/**
 * Created by ${fanjie} on 2017/6/23.
 */

public interface RecognizerAble {

    void startRecord();

    void stoprecord();

    void destory();

    boolean isListening();

}

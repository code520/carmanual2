package com.pingzhi.cn.carmanual.jpushreceiver;

import android.content.Context;
import android.content.Intent;

/**
 * Created by ${fanjie} on 2017/6/22.
 */

public interface JpushMsgReceiveable {

    void operatorMsg(Context context, Intent intent);

}

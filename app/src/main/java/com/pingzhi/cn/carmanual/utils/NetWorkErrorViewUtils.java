package com.pingzhi.cn.carmanual.utils;

import android.os.Handler;
import android.view.View;

import com.pingzhi.cn.carmanual.busmodel.NetWorkErrorDisappearModel;
import com.pingzhi.cn.carmanual.ownview.NetWorkErrorPage;
import com.pingzhi.cn.carmanual.utils.gettopactivity.CJTools;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public class NetWorkErrorViewUtils {

    private AddViewToFontUtils mAddViewToFontUtils;

    public NetWorkErrorViewUtils() {
        mAddViewToFontUtils = new AddViewToFontUtils();
    }

    public void show(){
        View view = new NetWorkErrorPage(CJTools.getTopActivity());
        mAddViewToFontUtils.showView(view);
        view.bringToFront();
        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mAddViewToFontUtils != null){
                    mAddViewToFontUtils.hideView();
                }
                EventBus.getDefault().post(new NetWorkErrorDisappearModel());
            }
        }, 3000);
    }


}

package com.pingzhi.cn.carmanual.utils.gettopactivity;

import android.app.Activity;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Created by ${fanjie} on 16/10/10.
 */

public class GetTopActivityImpl2 implements GetTopActivity {

    private GetTopActivityImpl2(){

    }

    @Override
    public Activity getTopActivity() {
        return getActivity();
    }

    public static GetTopActivityImpl2 getInstance(){
        return InstanceHolder.IMPL_2;
    }

    private Class activityThreadClass = null;
    private Map activities;
    private Activity getActivity(){
        try {
            if (activityThreadClass == null) {
                //activityThread只有一个实例
                activityThreadClass = Class.forName("android.app.ActivityThread");
                //sCurrentActivityThread是一个static变量,我们就可以获取原来的值
                Object activityThread = activityThreadClass.getMethod("currentActivityThread").invoke(null);
                Field activitiesField = activityThreadClass.getDeclaredField("mActivities");
                activitiesField.setAccessible(true);
                //是一个ArrayMap<IBinder, ActivityClientRecord>
                activities = (Map) activitiesField.get(activityThread);
            }
            //我们只需要获取其value就可以了
            for (Object activityRecord : activities.values()){
                Class activityRecordClass = activityRecord.getClass();
                //获取是否paused的bool值
                Field pausedField = activityRecordClass.getDeclaredField("paused");
                //是default的所以要强制获取
                pausedField.setAccessible(true);
                //如果没有停止
                if (!pausedField.getBoolean(activityRecord)){
                    //在ActivityClientRecord包含activity及其很多信息
                    Field activityField = activityRecordClass.getDeclaredField("activity");
                    activityField.setAccessible(true);
                    Activity activity = (Activity) activityField.get(activityRecord);
                    return activity;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class InstanceHolder{
        public static final GetTopActivityImpl2 IMPL_2 = new GetTopActivityImpl2();
    }
}

package com.pingzhi.cn.carmanual.busmodel;

import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.LiPeiModel;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/13.
 */

public class LiPeiDataModel {

    public List<LiPeiModel> datas;

    public LiPeiDataModel(List<LiPeiModel> datas) {
        this.datas = datas;
    }

    public LiPeiDataModel() {
    }
}

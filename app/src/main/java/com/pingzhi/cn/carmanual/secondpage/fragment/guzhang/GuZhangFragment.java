package com.pingzhi.cn.carmanual.secondpage.fragment.guzhang;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.busmodel.AIAnswerListModel;
import com.pingzhi.cn.carmanual.busmodel.AIAnswerModel;
import com.pingzhi.cn.carmanual.busmodel.CurrentPagerModel;
import com.pingzhi.cn.carmanual.secondpage.adapter.MyPagerAdapter;
import com.pingzhi.cn.carmanual.secondpage.fragment.BaseFragment;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/11.
 */

public class GuZhangFragment extends BaseFragment {

    private ViewPager mViewPager;
    private List<ShowPagerModel> datas = new ArrayList<>();
    private MyPagerAdapter mMyPagerAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mViewPager = (ViewPager) inflater.inflate(R.layout.fragment_chang_shi, container, false);
        mMyPagerAdapter = new MyPagerAdapter(datas);
        mViewPager.setAdapter(mMyPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mShowtipPresent.stopRecord();
                mShowtipPresent.stopVoice();
                mShowtipPresent.startVoiceWithText(datas.get(position).question + "?" + datas.get(position).content);
                EventBus.getDefault().post(new CurrentPagerModel(position, datas.size()));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        EventBus.getDefault().register(this);
        return mViewPager;
    }

    public void addData(ShowPagerModel showPagerModel){
        datas.add(showPagerModel);
        mMyPagerAdapter.notifyDataSetChanged();
    }

    public void addDatas(List<ShowPagerModel> datas){
        this.datas.addAll(datas);
        mMyPagerAdapter.notifyDataSetChanged();
    }

    @Subscribe(sticky = true)
    public void handleAIAnswer(AIAnswerModel aiAnswerModel){
        if (aiAnswerModel.mShowPagerModel.type == Constant.WEI_XIU){
            EventBus.getDefault().post(new CurrentPagerModel(0, 1));
            datas.clear();
            datas.add(aiAnswerModel.mShowPagerModel);
            mMyPagerAdapter.notifyDataSetChanged();
            mShowtipPresent.startVoiceWithText(datas.get(0).content);
            EventBus.getDefault().removeStickyEvent(aiAnswerModel);
        }
    }

    @Subscribe(sticky = true)
    public void handleAIAnswerList(AIAnswerListModel aiAnswerList){
        EventBus.getDefault().post(new CurrentPagerModel(0, aiAnswerList.mShowPagerModelList.size()));
        if (aiAnswerList.mShowPagerModelList != null && aiAnswerList.mShowPagerModelList.size() > 0 && aiAnswerList.mShowPagerModelList.get(0).type == Constant.WEI_XIU) {
            datas.clear();
            datas.addAll(aiAnswerList.mShowPagerModelList);
            mMyPagerAdapter.notifyDataSetChanged();
            if (datas.size() > 0) {
                mShowtipPresent.startVoiceWithText(datas.get(0).question + "?" + datas.get(0).content);
                mViewPager.setCurrentItem(0, true);
            } else {
                mShowtipPresent.stopVoice();
                mShowtipPresent.stopRecord();
            }
            EventBus.getDefault().removeStickyEvent(aiAnswerList);
        }

        if (aiAnswerList.mShowPagerModelList != null && aiAnswerList.mShowPagerModelList.size() == 0 ){
            datas.clear();
            mMyPagerAdapter.notifyDataSetChanged();
            mShowtipPresent.stopVoice();
            mShowtipPresent.stopRecord();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }



}

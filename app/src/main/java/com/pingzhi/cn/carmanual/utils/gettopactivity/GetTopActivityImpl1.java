package com.pingzhi.cn.carmanual.utils.gettopactivity;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/**
 * Created by ${fanjie} on 16/10/10.
 */

public class GetTopActivityImpl1 implements GetTopActivity {

    private GetTopActivityImpl1(){

    }
    public static void initInstance(Application application) {
        application.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {
                CJActivityManager.getInstance().setCurrentActivity(activity);
            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
    }

    public static GetTopActivityImpl1 getInstance(){
        return InstanceHolder.IMPL_1;
    }

    @Override
    public Activity getTopActivity() {
        return CJActivityManager.getInstance().getCurrentActivity();
    }

    private static class InstanceHolder{
        public static final GetTopActivityImpl1 IMPL_1 = new GetTopActivityImpl1();
    }
}

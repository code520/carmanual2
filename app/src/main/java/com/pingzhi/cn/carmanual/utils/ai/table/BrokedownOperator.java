package com.pingzhi.cn.carmanual.utils.ai.table;

import com.pingzhi.cn.carmanual.db.model.Brokedown;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;
import com.pingzhi.cn.carmanual.utils.ai.TableOperatorAble;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public class BrokedownOperator implements TableOperatorAble {
    @Override
    public ShowPagerModel getPagerModelByTableAndIntent(AIAnswerModel aiAnswerModel) {
//        if (brokedownList != null && brokedownList.size() > 0){
//            Brokedown brokedown = brokedownList.get(0);
//            return brokedown.getShowPagerModel(aiAnswerModel);
//        }else {
//            ShowPagerModel showPagerModel = new ShowPagerModel(aiAnswerModel);
//            return showPagerModel;
//        }
        Brokedown brokedown = new Brokedown();
        return brokedown.getShowPagerModel(aiAnswerModel);
    }
}

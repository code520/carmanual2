package com.pingzhi.cn.carmanual.utils.ai;

import com.pingzhi.cn.carmanual.router.ActionOperator;
import com.pingzhi.cn.carmanual.router.RegisterOperator;
import com.pingzhi.cn.carmanual.router.RouterAble;
import com.pingzhi.cn.carmanual.utils.ai.table.BrokedownOperator;
import com.pingzhi.cn.carmanual.utils.ai.table.InsuranceCompanyOperator;
import com.pingzhi.cn.carmanual.utils.ai.table.InsuranceOperator;
import com.pingzhi.cn.carmanual.utils.ai.table.MaintenanceOperator;
import com.pingzhi.cn.carmanual.utils.ai.table.ManualOperator;
import com.pingzhi.cn.carmanual.utils.ai.table.QaOperator;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public class TableRouter implements RouterAble<TableOperatorAble> {

    public static final String INSURANCE = "insurance";

    public static final String INSURANCE_COMPANY = "insurance_company";

    public static final String MAINTENCE = "maintenance";

    public static final String MANUAL = "manual";

    public static final String BROKENDOWN = "brokendown";

    public static final String QA = "qa";
    @Override
    public void initRouter(RegisterOperator<TableOperatorAble> registerOperator) {
        registerOperator.registerOperator(INSURANCE, new ActionOperator<TableOperatorAble>(){
            @Override
            public TableOperatorAble createService() {
                return new InsuranceOperator();
            }
        });

        registerOperator.registerOperator(INSURANCE_COMPANY, new ActionOperator<TableOperatorAble>(){
            @Override
            public TableOperatorAble createService() {
                return new InsuranceCompanyOperator();
            }
        });

        registerOperator.registerOperator(MAINTENCE, new ActionOperator<TableOperatorAble>(){
            @Override
            public TableOperatorAble createService() {
                return new MaintenanceOperator();
            }
        });

        registerOperator.registerOperator(MANUAL, new ActionOperator<TableOperatorAble>(){
            @Override
            public TableOperatorAble createService() {
                return new ManualOperator();
            }
        });

        registerOperator.registerOperator(BROKENDOWN, new ActionOperator<TableOperatorAble>(){
            @Override
            public TableOperatorAble createService() {
                return new BrokedownOperator();
            }
        });

        registerOperator.registerOperator(QA, new ActionOperator<TableOperatorAble>(){
            @Override
            public TableOperatorAble createService() {
                return new QaOperator();
            }
        });
    }
}

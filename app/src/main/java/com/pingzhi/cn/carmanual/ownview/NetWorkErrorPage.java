package com.pingzhi.cn.carmanual.ownview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.chechi.aiandroid.R;


/**
 * Created by ${fanjie} on 2017/7/4.
 */

public class NetWorkErrorPage extends RelativeLayout {

    public NetWorkErrorPage(@NonNull Context context) {
        this(context, null);
    }
//@mipmap/background
    public NetWorkErrorPage(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setBackgroundResource(R.mipmap.background);
        LayoutInflater.from(context).inflate(R.layout.networkerror_page, this, true);
    }


}

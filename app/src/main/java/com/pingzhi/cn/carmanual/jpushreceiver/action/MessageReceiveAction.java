package com.pingzhi.cn.carmanual.jpushreceiver.action;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.pingzhi.cn.carmanual.busmodel.ReceiveJpushModel;
import com.pingzhi.cn.carmanual.jpushreceiver.JpushMessage;
import com.pingzhi.cn.carmanual.jpushreceiver.JpushMsgReceiveable;
import com.pingzhi.cn.carmanual.utils.PreferenceUtils;
import com.pingzhi.cn.carmanual.utils.json.JsonUtils;

import org.greenrobot.eventbus.EventBus;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by ${fanjie} on 2017/6/22.
 */

public class MessageReceiveAction implements JpushMsgReceiveable {
    @Override
    public void operatorMsg(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        String jsonStr = bundle.getString(JPushInterface.EXTRA_EXTRA);
        JpushMessage jpushMessage = JsonUtils.parseObject(jsonStr, JpushMessage.class);
        PreferenceUtils.getInstance().setJpushMessage(jpushMessage.content);
        EventBus.getDefault().post(new ReceiveJpushModel(jpushMessage.content));
    }
}

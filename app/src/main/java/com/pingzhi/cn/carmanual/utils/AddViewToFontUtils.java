package com.pingzhi.cn.carmanual.utils;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

/**
 * Created by ${fanjie} on 2017/6/28.
 */

public class AddViewToFontUtils {

    private ViewGroup mContainer;
    private View mView;
    public void showView(View view, ViewGroup.LayoutParams params){
        Activity activity = (Activity) view.getContext();
        Window window = activity.getWindow();
        mContainer = (ViewGroup) window.getDecorView();
        mView = view;
        mView.bringToFront();
        mContainer.addView(mView, params);
    }

    public void hideView(){
        if (mView != null && mView.getParent() != null){
            mContainer.removeView(mView);
            mView = null;
            mContainer = null;
        }
    }

    public void showView(View view){
        showView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

}

package com.pingzhi.cn.carmanual.utils.recycleclickhelper;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ${fanjie} on 17/4/13.
 */

public interface RecyclerViewItemLongPressListener {

    void onLongPress(RecyclerView recyclerView, View item, int position);
}

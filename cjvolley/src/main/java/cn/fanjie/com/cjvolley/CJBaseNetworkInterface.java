package cn.fanjie.com.cjvolley;

import android.support.annotation.NonNull;

import java.util.Map;

/**
 * Created by ${fanjie} on 16/10/3.
 */

public interface CJBaseNetworkInterface {

//    <T> void get(@NonNull String url, OnNetworkReturn<T> callback);
//
//    <T> void get(@NonNull String url, Map<String, String> params, OnNetworkReturn<T> callback);

//    void getArray(@NonNull String url, OnNetworkReturn<JSONArray> callback);
//
//    void getArray(@NonNull String url, Map<String, String> params, OnNetworkReturn<JSONArray> callback);
//
//    void getObject(@NonNull String url, OnNetworkReturn<JSONObject> callback);
//
//    void getObject(@NonNull String url, Map<String, String> params, OnNetworkReturn<JSONObject> callback);

//    <T> void post(@NonNull String url, OnNetworkReturn<T> callback);
//
//    <T> void post(@NonNull String url, Map<String, String> params, OnNetworkReturn<T> callback);

//    void postArray(@NonNull String url, OnNetworkReturn<JSONArray> callback);
//
//    void postArray(@NonNull String url, Map<String, String> params, OnNetworkReturn<JSONArray> callback);

//    void postObject(@NonNull String url, OnNetworkReturn<JSONObject> callback);
//
//    void postObject(@NonNull String url, Map<String, String> params, OnNetworkReturn<JSONObject> callback);

    <T> void request(int method, @NonNull String url, Map<String, Object> params, OnNetworkReturn<T> callback);

    <T> void request(@NonNull String url, Map<String, Object> params, OnNetworkReturn<T> callback);

}

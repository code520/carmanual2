package com.pingzhi.cn.carmanual.voicemanager.recognizer;

import android.content.Context;

/**
 * Created by ${fanjie} on 2017/6/23.
 */

public class OfflineRecognizerManager implements RecognizerAble {
    private Context mContext;

    public OfflineRecognizerManager(Context context) {
        mContext = context;
    }

    @Override
    public void startRecord() {

    }

    @Override
    public void stoprecord() {

    }

    @Override
    public void destory() {

    }

    @Override
    public boolean isListening() {
        return false;
    }
}

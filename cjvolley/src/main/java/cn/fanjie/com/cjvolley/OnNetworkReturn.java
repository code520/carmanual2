package cn.fanjie.com.cjvolley;

/**
 * Created by ${fanjie} on 16/10/3.
 */

public abstract class OnNetworkReturn<T> {

    protected abstract void onSuccess(T data);

    protected abstract void onError(Exception e);
}

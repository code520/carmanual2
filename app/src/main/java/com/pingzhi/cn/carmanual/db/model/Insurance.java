package com.pingzhi.cn.carmanual.db.model;

import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.TiaoKuanModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

/**
 * Created by ${fanjie} on 2017/7/4.
 * 车险条款
 */
@Entity(createInDb = false, nameInDb = "gs8_insurance")
public class Insurance implements ToShowPagerModel{
    @Property(nameInDb = "id")
    @Id(autoincrement = true)
    public long id;

    @Property(nameInDb = "grp")
    public String group;

    public String name;

    public String define;

    public String price;

    public String repeat;

    public String compensate;

    public String free;

    public String image;

    @Generated(hash = 672641797)
    public Insurance(long id, String group, String name, String define,
            String price, String repeat, String compensate, String free,
            String image) {
        this.id = id;
        this.group = group;
        this.name = name;
        this.define = define;
        this.price = price;
        this.repeat = repeat;
        this.compensate = compensate;
        this.free = free;
        this.image = image;
    }

    @Generated(hash = 247287177)
    public Insurance() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroup() {
        return this.group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDefine() {
        return this.define;
    }

    public void setDefine(String define) {
        this.define = define;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRepeat() {
        return this.repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public String getCompensate() {
        return this.compensate;
    }

    public void setCompensate(String compensate) {
        this.compensate = compensate;
    }

    public String getFree() {
        return this.free;
    }

    public void setFree(String free) {
        this.free = free;
    }


    public ShowPagerModel getShowPagerModel(AIAnswerModel aiAnswerModel) {
        ShowPagerModel showPagerModel = new ShowPagerModel();
        showPagerModel.question = aiAnswerModel.getSemantic().getText();
        showPagerModel.content = aiAnswerModel.getResult().getAnswer();
        showPagerModel.uri = Constant.imageDir + aiAnswerModel.getResult().getImage();
        showPagerModel.type = Constant.CHE_XIAN;
        return showPagerModel;
    }

    @Override
    public ShowPagerModel getShowPagerModel() {
        ShowPagerModel showPagerModel = new ShowPagerModel();
//        showPagerModel.question = aiAnswerModel.getText();
//        showPagerModel.content = aiAnswerModel.getAnswer().getResult().getData();
//        showPagerModel.uri = Constant.imageDir + image;
//        showPagerModel.type = Constant.CHE_XIAN;
        return showPagerModel;
    }

    public TiaoKuanModel getTiaokuanModel(){
        TiaoKuanModel tiaoKuanModel = new TiaoKuanModel();
        tiaoKuanModel.title = name;
        tiaoKuanModel.define = define;
        tiaoKuanModel.mianPeiLv = free;
        tiaoKuanModel.peiFuPrice = compensate;
        tiaoKuanModel.priceAdd = repeat;
        tiaoKuanModel.touBaoPrice = price;
        return tiaoKuanModel;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

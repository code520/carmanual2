package com.pingzhi.cn.carmanual.utils;

import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by ${fanjie} on 2017/7/2.
 */

public class ViewGestuerHelper {

    private View mView;
    private GestureDetector mGestureDetector;
    private boolean isLeftEdge;
    private Context mContext;
    private OpenMenuAble mOpenMenuAble;

    public ViewGestuerHelper(View view, OpenMenuAble openMenuAble) {
        mView = view;
        this.mOpenMenuAble = openMenuAble;
        mContext = view.getContext();
        mGestureDetector = new GestureDetector(mContext, mOnGestureListener);
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mGestureDetector.onTouchEvent(event);
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    if (event.getX() <= ScreenUtils.dip2px(mContext, 50)){
                        isLeftEdge = true;
                    }else {
                        isLeftEdge = false;
                    }
                }

                return isLeftEdge;
            }
        });
    }

    private static final String TAG = "ViewGestuerHelper";
    private GestureDetector.OnGestureListener mOnGestureListener = new GestureDetector.OnGestureListener() {
        @Override
        public boolean onDown(MotionEvent e) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {

        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            Log.e(TAG, "onScroll: " + distanceX, null);
            if (distanceX <= -100){
                mOpenMenuAble.openMenu();
            }
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {

        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            Log.e(TAG, "onFling: " + velocityX, null);
            if (velocityX > 1000){
                mOpenMenuAble.openMenu();
            }
            return true;
        }
    };

    public interface OpenMenuAble{
        void openMenu();
    }

    public void destroy(){
        mView = null;
        mGestureDetector = null;
        mOnGestureListener = null;
        mContext = null;
        mOpenMenuAble = null;
    }
}

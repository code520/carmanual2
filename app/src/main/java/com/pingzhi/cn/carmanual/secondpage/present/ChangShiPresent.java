package com.pingzhi.cn.carmanual.secondpage.present;


import android.support.v4.app.Fragment;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.busmodel.AIAnswerListModel;
import com.pingzhi.cn.carmanual.db.DbHelper;
import com.pingzhi.cn.carmanual.db.model.ManualV3;
import com.pingzhi.cn.carmanual.db.model.ManualV3Dao;
import com.pingzhi.cn.carmanual.present.ShowtipPresent;
import com.pingzhi.cn.carmanual.secondpage.fragment.changshi.ChangShiFragment;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${fanjie} on 2017/6/21.
 */

public class ChangShiPresent implements BasePresent {
    @Override
    public String[] getTypeTitles() {
        return new String[]{
                "座椅", "仪表台", "车顶", "雨刮", "钥匙", "发动机", "保险杠", "车门", "扶手", "方向盘", "灯", "轮胎", "车联网"
        };
    }

    @Override
    public int[] getTypeIcons() {
        return new int[]{
                R.mipmap.zuoyi_, R.mipmap.yibiaotai_, R.mipmap.cheding_, R.mipmap.yugua_, R.mipmap.yaoshi_, R.mipmap.fadongji_, R.mipmap.baoxiangang_, R.mipmap.chemen_,
                R.mipmap.fushou_, R.mipmap.fangxiangp_, R.mipmap.chedeng_, R.mipmap.luntai_, R.mipmap.lianwang_
//                R.drawable.zuoyi_selector, R.drawable.yibiaotai_selector, R.drawable.cheding_selector, R.drawable.yugua_selector,
//                R.drawable.yaoshi_selector, R.drawable.fadonjichang_selector, R.drawable.baoxiangang_selector, R.drawable.chemeng_selector,
//                R.drawable.fushou_selector, R.drawable.faxiangpan_selector, R.drawable.chedeng_selector, R.drawable.luntai_selector,
//                R.drawable.liangwang
        };
    }

    @Override
    public int[] getSelectIcons() {
        return new int[]{
                R.mipmap.zuoyi, R.mipmap.yibiaotai, R.mipmap.cheding, R.mipmap.yugua, R.mipmap.yaoshi, R.mipmap.fadongji, R.mipmap.baoxiangang, R.mipmap.chemen,
                R.mipmap.fushou, R.mipmap.fangxiangpan, R.mipmap.chedeng, R.mipmap.luntai, R.mipmap.lianwang
        };
    }

    @Override
    public String getType() {
        return "手 册";
    }

    @Override
    public void query(String world) {
        List<ManualV3> manualList = DbHelper.getInstance().getDaoSession().getManualV3Dao().queryBuilder().where(ManualV3Dao.Properties.Group.like("%" + world + "%")).list();
        List<ShowPagerModel> showPagerModels = new ArrayList<>(manualList.size());
        for (ManualV3 manual : manualList) {
            ShowPagerModel showPagerModel = manual.getShowPagerModel();
            showPagerModels.add(showPagerModel);
        }
        EventBus.getDefault().postSticky(new AIAnswerListModel(showPagerModels));
    }

    private ChangShiFragment mChangShiFragment;

    public ChangShiFragment getChangShiFragment(ShowtipPresent showtipPresent) {
        if (mChangShiFragment == null){
            mChangShiFragment = new ChangShiFragment();
            mChangShiFragment.mShowtipPresent = showtipPresent;
        }
        return mChangShiFragment;
    }

    @Override
    public <T> Fragment getFragment(T showPresent) {
        ChangShiFragment changShiFragment = getChangShiFragment((ShowtipPresent) showPresent);
        return changShiFragment;
    }

    @Override
    public boolean canScroll() {
        return true;
    }

    @Override
    public boolean canScroll(int position) {
        return true;
    }

}

package com.pingzhi.cn.carmanual.secondpage.fragment.chexian;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.LiPeiModel;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.ownview.MoreTextView;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/9.
 */

public class LiPeiAdapter extends RecyclerView.Adapter<LiPeiAdapter.ViewHolder> {

    private List<LiPeiModel> datas;

    public LiPeiAdapter(List<LiPeiModel> datas) {
        this.datas = datas;
    }

    private LayoutInflater mLayoutInflater;

    public LayoutInflater getLayoutInflater(Context context) {
        if (mLayoutInflater == null){
            mLayoutInflater = LayoutInflater.from(context);
        }
        return mLayoutInflater;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = getLayoutInflater(parent.getContext()).inflate(R.layout.item_lipei, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindData(datas.get(position));
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView question;
        private MoreTextView mMoreTextView;
        public ViewHolder(View itemView) {
            super(itemView);
            question = (TextView) itemView.findViewById(R.id.question);
            mMoreTextView = (MoreTextView) itemView.findViewById(R.id.li_pei_content);
        }

        public void bindData(LiPeiModel liPeiModel){
            question.setText(liPeiModel.question);
            mMoreTextView.setText(liPeiModel.content);
        }
    }
}

package com.pingzhi.cn.carmanual.voicemanager.wake;

/**
 * Created by ${fanjie} on 2017/6/23.
 */

public interface WakeupAble {

    void registerListener(WakeUpCallback wakeUpCallback);

    void unRegisterListener();

    void wakeUpStart();

    void wakeUpStop();

}

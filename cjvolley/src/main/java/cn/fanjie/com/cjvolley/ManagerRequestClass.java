package cn.fanjie.com.cjvolley;

import android.support.annotation.NonNull;

import com.android.volley.Request;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ${fanjie} on 16/10/3.
 */

public class ManagerRequestClass {
    private static Map<String, Operator> map = new HashMap<>();

    public static void registerRequestClass(String name, Operator operator){
        map.put(name, operator);
    }

    public  static <T> Request startNetwork(int method, @NonNull String url, Map<String, Object> params, OnNetworkReturn<T> callback){
        Class<T> clazz = (Class<T>) GetClass.getSuperClassGenricType(callback.getClass(), 0);
        String name = clazz.getName();
        Operator operator = map.get(name);
        if (operator == null){
            throw new RuntimeException("请先注册再使用");
        }
        return operator.operator(method, url, params, callback);
    }
}

package com.pingzhi.cn.carmanual.ownview;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.chechi.aiandroid.R;


/**
 * 封装了显示Popupwindow的方法.
 * @author fanjie
 * @create time 2015-10-27
 */
public class Util implements TipRelativeLayout.AnimationEndCallback {
	private PopupWindow reportVideoPopwindow;
	private TipRelativeLayout tvTips;
	private int translateHeight;
	private View parent;
	private View popView;
	private int statusBar;
	private TextView mContent;
	
	public void showTips(final Activity activity){
		initView(activity);
		if (!activity.isFinishing()){
			parent.post(new Runnable() {
				@Override
				public void run() {
					if (!activity.isFinishing() && reportVideoPopwindow != null){
						reportVideoPopwindow.showAtLocation(parent, Gravity.TOP, 0, 0);
						tvTips.showTips();//显示提示RelativeLayout,移动动画.
					}
				}
			});
		}

	}

	private void initView(Activity activity){
		if (reportVideoPopwindow == null){
			translateHeight = (int) dip2px(activity,52);
			parent = ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);
			popView = LayoutInflater.from(activity).inflate(R.layout.activity_popupwindow_tips, null);
			mContent = (TextView) popView.findViewById(R.id.content);
			statusBar=getStatusBarHeight(activity);
			reportVideoPopwindow = new PopupWindow(popView, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);//translateHeight*2
			tvTips=(TipRelativeLayout) popView.findViewById(R.id.rl_tips);
//			tvTips.setTitleHeight(0);//移动状态栏的高度
			tvTips.setAnimationEnd(this);//设置动画结束监听函数
		}
	}
	
	public int getStatusBarHeight(Context context) {
		  int result = 0;
		  int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
		  if (resourceId > 0) {
		      result = context.getResources().getDimensionPixelSize(resourceId);
		  }
		  return result;
	}
	
	private  float dip2px(Context context, float dpValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		float result = dpValue * scale + 0.5f;
		return result;
	}

	public void closePopWindow(){
		if (tvTips != null){
			tvTips.topTranslateAnimation();
		}
	}

	public boolean isShow(){
		if (reportVideoPopwindow == null){
			return false;
		}

		return reportVideoPopwindow.isShowing();
	}

	public void  setContent(String text){
		mContent.setText(text);
	}
	
	@Override
	public void onAnimationEnd() {
		if (reportVideoPopwindow != null && reportVideoPopwindow.isShowing()){
			reportVideoPopwindow.dismiss();//动画结束，隐藏popupwindow
			reportVideoPopwindow = null;
		}
	}
}

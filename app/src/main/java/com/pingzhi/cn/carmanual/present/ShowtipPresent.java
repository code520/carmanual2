package com.pingzhi.cn.carmanual.present;

import android.app.Activity;
import android.view.View;

import com.haozhang.lib.AnimatedRecordingView;
import com.pingzhi.cn.carmanual.busmodel.AICanNotSolveModel;
import com.pingzhi.cn.carmanual.busmodel.AIMutualModel;
import com.pingzhi.cn.carmanual.busmodel.AIMutualVoiceFinishModel;
import com.pingzhi.cn.carmanual.busmodel.ActivityAIAnswerModel;
import com.pingzhi.cn.carmanual.busmodel.HelloFinishModel;
import com.pingzhi.cn.carmanual.busmodel.LastPager;
import com.pingzhi.cn.carmanual.busmodel.MyVoiceModel;
import com.pingzhi.cn.carmanual.busmodel.NeedWifyModel;
import com.pingzhi.cn.carmanual.busmodel.NetWorkErrorDisappearModel;
import com.pingzhi.cn.carmanual.busmodel.NetWorkErrorModel;
import com.pingzhi.cn.carmanual.busmodel.RecordBeginModel;
import com.pingzhi.cn.carmanual.busmodel.RecordErrorModel;
import com.pingzhi.cn.carmanual.busmodel.RecordResultModel;
import com.pingzhi.cn.carmanual.busmodel.RobotSpeakBegin;
import com.pingzhi.cn.carmanual.busmodel.VoiceFinishModel;
import com.pingzhi.cn.carmanual.busmodel.VoiceNotifyModel;
import com.pingzhi.cn.carmanual.busmodel.WakeupSuccessModel;
import com.pingzhi.cn.carmanual.busmodel.WifyConnectModel;
import com.pingzhi.cn.carmanual.helppage.view.HelpActivity;
import com.pingzhi.cn.carmanual.ownview.AIMutualUtils;
import com.pingzhi.cn.carmanual.ownview.NItems;
import com.pingzhi.cn.carmanual.ownview.Util;
import com.pingzhi.cn.carmanual.secondpage.view.SecondActivity;
import com.pingzhi.cn.carmanual.utils.NetWorkErrorViewUtils;
import com.pingzhi.cn.carmanual.utils.WifyChangeViewUtils;
import com.pingzhi.cn.carmanual.utils.ai.AIQuery;
import com.pingzhi.cn.carmanual.utils.network.NetWorkRecongniseUtils;
import com.pingzhi.cn.carmanual.voicemanager.VoiceManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by ${fanjie} on 2017/6/27.
 */

public class ShowtipPresent {
    private Activity mActivity;
    private VoiceManager mVoiceManager;
    private AnimatedRecordingView mRecordingView;

    public ShowtipPresent(AnimatedRecordingView animatedRecordingView) {
        this((Activity) animatedRecordingView.getContext());
        this.mRecordingView = animatedRecordingView;
    }

    public ShowtipPresent(Activity activity) {
        mActivity = activity;
        mVoiceManager = new VoiceManager(mActivity);
    }

    private AIMutualUtils mAIMutualUtils;

    public AIMutualUtils getAIMutualUtils() {
        if (mAIMutualUtils == null){
            mAIMutualUtils = new AIMutualUtils();
            mAIMutualUtils.setOnItemClickListener(new NItems.OnNItemClick() {
                @Override
                public void onItemClick(View view, String content, int position) {
                    getAIQuery().queryWithQuestion(content);
                }
            });
        }
        return mAIMutualUtils;
    }


    private VoiceManager getVoiceManager(){
        return mVoiceManager;
    }

    private Util windowUtils;

    private Util getWindowUtils(){
        if (windowUtils == null){
            windowUtils = new Util();
        }
        return windowUtils;
    }

    @Subscribe
    public void handleAIMutualFinish(AIMutualVoiceFinishModel aiMutualVoiceFinishModel){
        int index = -1;
        if (aiMutualVoiceFinishModel.content == null && aiMutualVoiceFinishModel.content.equals("")){
            return;
        }
        if (aiMutualVoiceFinishModel.content.contains("下一")){
            if (getAIMutualUtils().nextPager()){
                mVoiceManager.startRecord();
            }
        }else if (aiMutualVoiceFinishModel.content.contains("一") || aiMutualVoiceFinishModel.content.contains("1")){
            getAIMutualUtils().setSelectItem(1);
        }else if (aiMutualVoiceFinishModel.content.contains("二") || aiMutualVoiceFinishModel.content.contains("2")){
            getAIMutualUtils().setSelectItem(2);
        }else if (aiMutualVoiceFinishModel.content.contains("三") || aiMutualVoiceFinishModel.content.contains("3")){
            getAIMutualUtils().setSelectItem(3);
        }else if (aiMutualVoiceFinishModel.content.contains("四") || aiMutualVoiceFinishModel.content.contains("4")){
            getAIMutualUtils().setSelectItem(4);
        }else if (aiMutualVoiceFinishModel.content.contains("五") || aiMutualVoiceFinishModel.content.contains("5")){
            getAIMutualUtils().setSelectItem(5);
        }else if (aiMutualVoiceFinishModel.content.contains("六") || aiMutualVoiceFinishModel.content.contains("6")){
            getAIMutualUtils().setSelectItem(6);
        }else if (aiMutualVoiceFinishModel.content.contains("七") || aiMutualVoiceFinishModel.content.contains("7")){
            getAIMutualUtils().setSelectItem(7);
        }else if (aiMutualVoiceFinishModel.content.contains("八") || aiMutualVoiceFinishModel.content.contains("8")){
            getAIMutualUtils().setSelectItem(8);
        }else if (aiMutualVoiceFinishModel.content.contains("取消") || aiMutualVoiceFinishModel.content.contains("返回")){
            getAIQuery().cancleQuestion();
            getAIMutualUtils().hidePager();
            showRecordView();
            stopRecord();
        }else if ((index = getAIMutualUtils().coatain(aiMutualVoiceFinishModel.content)) != -1){
            getAIMutualUtils().setSelectItem(index);
        } else {
            handNoHandle();
        }
    }

    private int mError = 0;
    private void handNoHandle(){
        if (mError == 2){
            mVoiceManager.startRecord();
            getAIMutualUtils().hidePager();
            getWindowUtils().closePopWindow();
            mError = 0;
            return;
        }
        mVoiceManager.startRecordWithHelloText("没有听清，请再说一遍");
        mError++;

    }

    @Subscribe
    public void handleAIMutual(AIMutualModel aiMutualModel){
        getAIMutualUtils().setDatas(aiMutualModel.datas);
        getAIMutualUtils().showPager();
        getWindowUtils().closePopWindow();
        startVoiceWithText(aiMutualModel.title, 2);
        hideRecordView();
    }

    @Subscribe
    public void handleLastPager(LastPager lastPager){
        if (lastPager.currentPager != 0){
            startVoiceWithText("没有下一页了哦", 2);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleRecordResult(RecordResultModel recordResultModel){
        getWindowUtils().showTips(mActivity);
        getWindowUtils().setContent(recordResultModel.content);
    }

    private AIQuery mAIQuery;

    public AIQuery getAIQuery() {
        if (mAIQuery == null){
            mAIQuery = new AIQuery();
        }
        return mAIQuery;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleMyVoice(MyVoiceModel myVoiceModel){
        getWindowUtils().showTips(mActivity);
        getWindowUtils().setContent(myVoiceModel.content);
        getAIQuery().queryWithQuestion(myVoiceModel.content);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleRecordBegin(RecordBeginModel recordBeginModel){
//        getWindowUtils().showTips(mActivity);
//        getWindowUtils().setContent("正在处理。。。");
        mRecordingView.stopAutoPlay();
    }

    private int errorCount = 0;
    private static final String NO_VOICE_TEXT = "没听清您说的话.请再说一遍.";
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleRecordError(RecordErrorModel recordErrorModel){
        if (recordErrorModel.errorCode == 10118){
            if (errorCount == 2){
                errorCount = 0;
                getWindowUtils().closePopWindow();
                getAIMutualUtils().hidePager();
                showRecordView();
                startVoiceWithText("抱歉，您没有说话，期待下次为您服务");
                return;
            }
            errorCount++;
            mVoiceManager.startRecordWithHelloText(NO_VOICE_TEXT);
            getWindowUtils().showTips(mActivity);
            getWindowUtils().setContent(NO_VOICE_TEXT);
        }
    }

    private void stepToHelpActivity() {
        HelpActivity.start(mActivity);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleVoiceFinish(VoiceFinishModel voiceFinishModel){
        getWindowUtils().closePopWindow();
        if (mRecordingView != null){
            mRecordingView.stop();
        }
    }

    private WifyChangeViewUtils mWifyChangeViewUtils;

    public WifyChangeViewUtils getWifyChangeViewUtils() {
        if (mWifyChangeViewUtils == null){
            mWifyChangeViewUtils = new WifyChangeViewUtils();
        }
        return mWifyChangeViewUtils;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleWifyNeed(NeedWifyModel needWifyModel){
        getWifyChangeViewUtils().show();
        hideRecordView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleWifyConnected(WifyConnectModel needWifyModel){
        getWifyChangeViewUtils().hide();
        showRecordView();
//        EventBus.getDefault().removeStickyEvent(needWifyModel);
    }

    @Subscribe
    public void handleVoiceNotify(VoiceNotifyModel voiceNotifyModel){
        mRecordingView.setVolume(4 * voiceNotifyModel.volum);
    }

    @Subscribe
    public void handleWakeupSuccess(WakeupSuccessModel wakeupSuccessModel){
        mRecordingView.start();
        mRecordingView.autoPlay();
        errorCount = 0;
    }

    private NetWorkErrorViewUtils mNetWorkErrorViewUtils;

    public NetWorkErrorViewUtils getNetWorkErrorViewUtils() {
        if (mNetWorkErrorViewUtils == null){
            mNetWorkErrorViewUtils = new NetWorkErrorViewUtils();
        }
        return mNetWorkErrorViewUtils;
    }

    @Subscribe
    public void handleNetWorkError(NetWorkErrorModel netWorkErrorModel){
        getNetWorkErrorViewUtils().show();
        hideRecordView();
        stopRecord();
    }

    @Subscribe
    public void handleAIAnswer(ActivityAIAnswerModel aiAnswerModel){
        getAIMutualUtils().hidePager();
        SecondActivity.start(mActivity, aiAnswerModel.mShowPagerModel);
        getWindowUtils().closePopWindow();
    }

    private int noRecognise = 0;
    @Subscribe
    public void handleAICanNotSolve(AICanNotSolveModel aiCanNotSolveModel){
        if (errorCount == 2){
            errorCount = 0;
            if (aiCanNotSolveModel.isRelativeToCar){
                startVoiceWithText("抱歉，您的语言能力太强，暂时还不能理解，请尝试按帮助页语言方式提问", 1);
            }else {
                startVoiceWithText("抱歉没有查询到相关数据");
            }
            getWindowUtils().closePopWindow();
            getAIMutualUtils().hidePager();
            showRecordView();
            return;
        }
        errorCount++;
        if (noRecognise == 1){
            noRecognise = 0;
            getAIQuery().cancleQuestion();
        }
        noRecognise++;
        startVoiceWithText("没有理解您的意思，请换种说法在说一遍", 0);
        getWindowUtils().closePopWindow();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleHelloFinish(HelloFinishModel helloFinishModel){
        if (helloFinishModel.type == 1){
            HelpActivity.start(mActivity);
        }else if (helloFinishModel.type == 2){
//            mVoiceManager.startRecord();
        }
    }

    @Subscribe
    public void handleNetWorkDisappear(NetWorkErrorDisappearModel netWorkErrorDisappearModel){
        showRecordView();
    }

    @Subscribe
    public void handleRobotSpeakBegin(RobotSpeakBegin robotSpeakBegin){
        mRecordingView.autoPlay();
    }

    public void startRecord(){
        if (NetWorkRecongniseUtils.isNetworkConnected()){
            mRecordingView.start();
            EventBus.getDefault().post(new RecordResultModel("请说"));
//            mVoiceManager.startRecordWithHello();
            EventBus.getDefault().post(new HelloFinishModel(0));
            errorCount = 0;
        }else {
            handleWifyNeed(null);
        }
    }

    public void stopRecord(){
        mRecordingView.stop();
        mVoiceManager.stoprecord();
        getWindowUtils().closePopWindow();
    }

    public boolean isRecord(){
        return mVoiceManager.isListening();
    }

    public void stopVoice(){
        mRecordingView.stop();
        mVoiceManager.stopVoice();
        getWindowUtils().closePopWindow();
        mRecordingView.stopAutoPlay();
    }

    public boolean isVoice(){
        return mVoiceManager.isSpeaking();
    }

    public void startVoiceWithText(String text){
        mVoiceManager.startVoice(text);
    }

    public void startVoiceWithText(String text, int type){
        mVoiceManager.startVoice(text, type);
    }

    public void onResume(){
        mVoiceManager.onResume();
        mRecordingView.reset();
        showRecordView();
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
        getWifyChangeViewUtils().hide();
    }

    public void hideRecordView(){
        mRecordingView.setVisibility(View.INVISIBLE);
    }

    public void showRecordView(){
        mRecordingView.setVisibility(View.VISIBLE);
    }

    public void onPause() {
        mVoiceManager.onPause();
        getWindowUtils().closePopWindow();
        if ((EventBus.getDefault().isRegistered(this))){
            EventBus.getDefault().unregister(this);
        }
        mRecordingView.stop();
        getAIMutualUtils().hidePager();
    }

    public void onDestroy() {
        mVoiceManager.desaroy();
        getWindowUtils().onAnimationEnd();
        mRecordingView.release();
        mRecordingView = null;
    }
}

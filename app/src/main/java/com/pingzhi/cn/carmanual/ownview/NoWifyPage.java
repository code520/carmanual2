package com.pingzhi.cn.carmanual.ownview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.busmodel.NetWorkErrorDisappearModel;
import com.pingzhi.cn.carmanual.utils.OperatorUtils;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by ${fanjie} on 2017/6/28.
 */

public class NoWifyPage extends ConstraintLayout {

    public NoWifyPage(Context context) {
        this(context, null);
    }

    public NoWifyPage(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        initClick();
    }


    private ImageView mImgBack;
    private ImageView mImgHome;
    private ImageView mImgWangLuo;
    private TextView mText;
    private void initView(){
        LayoutInflater.from(getContext()).inflate(R.layout.duanwang, this, true);
        setClickable(true);
        mImgBack = (ImageView) findViewById(R.id.id_duangwang_back);
        mImgHome = (ImageView) findViewById(R.id.id_duangwang_home);
        mImgWangLuo = (ImageView) findViewById(R.id.id_duangwang_wangluo);
        mText = (TextView) findViewById(R.id.id_duangwang_text);
        setBackgroundResource(R.mipmap.background);
    }

    private void initClick(){
        mImgBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                back((Activity) v.getContext());
            }
        });

        mImgHome.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                stepToHome(v.getContext());
            }
        });

        mImgWangLuo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toSettingWify(v.getContext());
            }
        });

        mText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toSettingWify(v.getContext());
            }
        });
    }


    private void back(Activity activity){
        ViewGroup viewGroup = (ViewGroup) this.getParent();
        viewGroup.removeView(this);
        EventBus.getDefault().post(new NetWorkErrorDisappearModel());
    }

    private void stepToHome(Context context){
        OperatorUtils.stepToHome(context);
    }

    private void toSettingWify(Context context){
//        Intent intent = new Intent();
//        intent.setAction("android.net.wifi.PICK_WIFI_NETWORK");
//        context.startActivity(intent);
        context.startActivity(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS));
    }
}

package com.pingzhi.cn.carmanual.utils.ai.table;

import com.pingzhi.cn.carmanual.db.model.Qa;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;
import com.pingzhi.cn.carmanual.utils.ai.TableOperatorAble;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public class QaOperator implements TableOperatorAble {
    @Override
    public ShowPagerModel getPagerModelByTableAndIntent(AIAnswerModel aiAnswerModel) {
//        List<Qa> qaList = DbHelper.getInstance().getDaoSession().getQaDao().queryBuilder().where(QaDao.Properties.Id.eq(aiAnswerModel.getAnswer().getResult().getId())).list();
//        if (qaList != null && qaList.size() > 0){
//            Qa qa = qaList.get(0);
//            return qa.getShowPagerModel(aiAnswerModel);
//        }else {
//            ShowPagerModel showPagerModel = new ShowPagerModel(aiAnswerModel);
//            return showPagerModel;
//        }
        Qa qa = new Qa();
        return qa.getShowPagerModel(aiAnswerModel);
    }
}

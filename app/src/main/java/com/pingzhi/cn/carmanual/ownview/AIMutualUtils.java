package com.pingzhi.cn.carmanual.ownview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.busmodel.AIMutualVoiceFinishModel;
import com.pingzhi.cn.carmanual.model.NItemModel;
import com.pingzhi.cn.carmanual.utils.gettopactivity.CJTools;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/12.
 */

public class AIMutualUtils {

    private ViewGroup mRoot;
    private TextView nextPager;
    private NItems mNItems;
    private TextView nexttext;
    private TextView back;
//    private List<NItemModel> datas;
    private ConstraintSet mConstraintSet = new ConstraintSet();
    public AIMutualUtils() {
        mRoot = (ViewGroup) LayoutInflater.from(CJTools.getTopActivity()).inflate(R.layout.more_question, null);
        mConstraintSet.clone((ConstraintLayout) mRoot);
        nextPager = (TextView) mRoot.findViewById(R.id.textView6);
        mNItems = (NItems) mRoot.findViewById(R.id.n_item);
        nexttext = (TextView)mRoot.findViewById(R.id.textView11);
        back = (TextView) mRoot.findViewById(R.id.textView19);
        nextPager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextPager();
            }
        });

        mNItems.setOnNItemClick(new NItems.OnNItemClick() {
            @Override
            public void onItemClick(View view, String content, int position) {
                operatorNItemClick(view, content, position);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AIMutualVoiceFinishModel aiMutualVoiceFinishModel = new AIMutualVoiceFinishModel();
                aiMutualVoiceFinishModel.content = "取消";
                EventBus.getDefault().post(aiMutualVoiceFinishModel);
            }
        });
    }

    private void operatorNItemClick(View view, String content, int position){

    }

    public boolean nextPager(){
        boolean hasNext = mNItems.nextPager();
        if (!hasNext){
            mConstraintSet.setVisibility(R.id.textView11, ConstraintSet.GONE);
            nextPager.setClickable(false);
//            nextPager.setVisibility(View.INVISIBLE);
            mConstraintSet.setVisibility(R.id.textView6, ConstraintSet.INVISIBLE);
            mConstraintSet.applyTo((ConstraintLayout) mRoot);
        }else {
            mConstraintSet.setVisibility(R.id.textView11, ConstraintSet.VISIBLE);
            nextPager.setClickable(true);
//            nextPager.setVisibility(View.VISIBLE);
            mConstraintSet.setVisibility(R.id.textView6, ConstraintSet.VISIBLE);
            mConstraintSet.applyTo((ConstraintLayout) mRoot);
        }
        return hasNext;
    }

    public boolean canNext(){
        boolean canNext = mNItems.canNextPager();
        if (!canNext){
            mConstraintSet.setVisibility(R.id.textView11, ConstraintSet.GONE);
            nextPager.setClickable(false);
//            nextPager.setVisibility(View.INVISIBLE);
            mConstraintSet.setVisibility(R.id.textView6, ConstraintSet.INVISIBLE);
            mConstraintSet.applyTo((ConstraintLayout) mRoot);
        }else {
            mConstraintSet.setVisibility(R.id.textView11, ConstraintSet.VISIBLE);
            nextPager.setClickable(true);
//            nextPager.setVisibility(View.VISIBLE);
            mConstraintSet.setVisibility(R.id.textView6, ConstraintSet.VISIBLE);
            mConstraintSet.applyTo((ConstraintLayout) mRoot);
        }
        return canNext;
    }

    public boolean prePager(){
        boolean hasPre = mNItems.prePager();
        nexttext.setVisibility(View.VISIBLE);
        return hasPre;
    }

    public void setDatas(List<NItemModel> datas) {
//        this.datas = datas;
        mNItems.setDatas(datas);
    }

    public void clearData(){
        mNItems.clearData();
    }
    
    public int coatain(@NonNull String world){
        return mNItems.contain(world);
    }

    public void setSelectItem(int item){
        mNItems.setClickItemPosition(item);
    }

    public void setOnItemClickListener(NItems.OnNItemClick onItemClickListener){
        mNItems.setOnNItemClick(onItemClickListener);
    }

    public void showPager(){
        if (mRoot.getParent() == null){
            Window window = CJTools.getTopActivity().getWindow();
            window.addContentView(mRoot, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mRoot, "translationY", -1000, 0);
            objectAnimator.setDuration(400);
            objectAnimator.addListener(new AnimatorListenerAdapter() {
                /**
                 * {@inheritDoc}
                 *
                 * @param animation
                 */
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    canNext();
                }
            });
            objectAnimator.start();
        }
    }

    public void hidePager(){
        if (mNItems.isAdd()){
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mRoot, "translationY", 0, -1000);
            objectAnimator.setDuration(400);
            objectAnimator.addListener(new AnimatorListenerAdapter() {
                /**
                 * {@inheritDoc}
                 *
                 * @param animation
                 */
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    ViewGroup group = (ViewGroup) mRoot.getParent();
                    if (group != null){
                        group.removeView(mRoot);
                    }
                    Constant.isMutibleVoice = false;
                    mNItems.clearData();
                }
            });
            objectAnimator.start();
        }
    }
}

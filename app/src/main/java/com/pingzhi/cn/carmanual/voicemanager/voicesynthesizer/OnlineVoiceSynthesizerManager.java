package com.pingzhi.cn.carmanual.voicemanager.voicesynthesizer;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SynthesizerListener;
import com.pingzhi.cn.carmanual.busmodel.HelloFinishModel;
import com.pingzhi.cn.carmanual.busmodel.RobotSpeakBegin;
import com.pingzhi.cn.carmanual.busmodel.VoiceFinishModel;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by ${fanjie} on 2017/6/23.
 */

public class OnlineVoiceSynthesizerManager implements VoiceSynthesizerAble{

    private SpeechSynthesizer mSpeechSynthesizer;
    private Context mContext;

    public OnlineVoiceSynthesizerManager(@NonNull Context context) {
        this.mContext = context;
        mSpeechSynthesizer = SpeechSynthesizer.createSynthesizer(context, new MyInitListener());
    }

    private MySynthesizerListener mMySynthesizerListener;

    public MySynthesizerListener getMySynthesizerListener() {
        if(mMySynthesizerListener == null){
            mMySynthesizerListener = new MySynthesizerListener();
        }
        return mMySynthesizerListener;
    }

    public void startVoice(@NonNull String text){
        int resultCode = mSpeechSynthesizer.startSpeaking(text, getMySynthesizerListener());

        if (resultCode != ErrorCode.SUCCESS){
            showTip("语音合成失败,错误码: " + resultCode);
        }else {

        }
    }

    public void startHelloVoice(@NonNull String text){
        startHelloVoice(text, 0);
    }

    private HelloSynthesizerListener mHelloSynthesizerListener;

    public HelloSynthesizerListener getHelloSynthesizerListener(int type) {
        if (mHelloSynthesizerListener == null){
            mHelloSynthesizerListener = new HelloSynthesizerListener();
        }
        mHelloSynthesizerListener.setType(type);
        return mHelloSynthesizerListener;
    }

    /**
     * type: 0: hello
     * */
    public void startHelloVoice(@NonNull String text, int type){
        if (mSpeechSynthesizer != null){
            int resultCode = mSpeechSynthesizer.startSpeaking(text, getHelloSynthesizerListener(type));
            if (resultCode != ErrorCode.SUCCESS){
                showTip("语音合成失败,错误码: " + resultCode);
            }else {

            }
        }
    }

    @Override
    public void stopVoice() {
        if (mSpeechSynthesizer != null){
            mSpeechSynthesizer.stopSpeaking();
        }
    }

    @Override
    public void destory() {
        if (mSpeechSynthesizer == null){
            return;
        }
        mSpeechSynthesizer.destroy();
    }

    @Override
    public boolean isSpeaking() {
        if (mSpeechSynthesizer == null){
            return false;
        }
        return mSpeechSynthesizer.isSpeaking();
    }

    private static final String VOICER = "xiaoyan";
    private static final int SPEED = 50;
    private static final int PITCH = 50;
    private static final int VOLUME = 50;
    private static final int STREAM_TYPE = 3;
    private static final String AUDIO_FORMAT = "wav";
    private void setParams(){
        mSpeechSynthesizer.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
        mSpeechSynthesizer.setParameter(SpeechConstant.VOICE_NAME, VOICER);
        mSpeechSynthesizer.setParameter(SpeechConstant.SPEED, SPEED + "");
        mSpeechSynthesizer.setParameter(SpeechConstant.PITCH, PITCH + "");
        mSpeechSynthesizer.setParameter(SpeechConstant.VOLUME, VOLUME + "");
        //设置播放器音频流类型
        mSpeechSynthesizer.setParameter(SpeechConstant.STREAM_TYPE, STREAM_TYPE + "");
        // 设置播放合成音频打断音乐播放，默认为true
        mSpeechSynthesizer.setParameter(SpeechConstant.KEY_REQUEST_FOCUS, "true");

        // 设置音频保存路径，保存音频格式支持pcm、wav，设置路径为sd卡请注意WRITE_EXTERNAL_STORAGE权限
        // 注：AUDIO_FORMAT参数语记需要更新版本才能生效
        mSpeechSynthesizer.setParameter(SpeechConstant.AUDIO_FORMAT, AUDIO_FORMAT);
        mSpeechSynthesizer.setParameter(SpeechConstant.TTS_AUDIO_PATH, Environment.getExternalStorageDirectory()+"/msc/tts.wav");
    }

    private class MyInitListener implements InitListener{

        @Override
        public void onInit(int code) {
            if (code != ErrorCode.SUCCESS){
                showTip("初始化失败，错误码：" + code);
            }else {
                setParams();
            }
        }
    }

    private static final String TAG = "SynthesizerListener";
    private class HelloSynthesizerListener implements SynthesizerListener{

        private int type;

        public HelloSynthesizerListener(int type) {
            this.type = type;
        }

        public HelloSynthesizerListener() {
        }

        public void setType(int type) {
            this.type = type;
        }

        @Override
        public void onSpeakBegin() {
            EventBus.getDefault().post(new RobotSpeakBegin());
        }

        @Override
        public void onBufferProgress(int i, int i1, int i2, String s) {

        }

        @Override
        public void onSpeakPaused() {

        }

        @Override
        public void onSpeakResumed() {

        }

        @Override
        public void onSpeakProgress(int i, int i1, int i2) {

        }

        @Override
        public void onCompleted(SpeechError speechError) {
            EventBus.getDefault().post(new HelloFinishModel(type));
        }

        @Override
        public void onEvent(int i, int i1, int i2, Bundle bundle) {

        }
    }

    private class MySynthesizerListener implements SynthesizerListener{

        @Override
        public void onSpeakBegin() {
            EventBus.getDefault().post(new RobotSpeakBegin());
        }

        @Override
        public void onBufferProgress(int i, int i1, int i2, String s) {

        }

        @Override
        public void onSpeakPaused() {

        }

        @Override
        public void onSpeakResumed() {

        }

        @Override
        public void onSpeakProgress(int i, int i1, int i2) {

        }

        @Override
        public void onCompleted(SpeechError speechError) {
            EventBus.getDefault().post(new VoiceFinishModel());
        }

        @Override
        public void onEvent(int i, int i1, int i2, Bundle bundle) {

        }
    }

    private void showTip(String s) {
        Toast.makeText(mContext, s, Toast.LENGTH_SHORT);
    }
}

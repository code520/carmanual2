package com.pingzhi.cn.carmanual.voicemanager.voicesynthesizer;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by ${fanjie} on 2017/6/23.
 */

public class OfflineVoiceSynthesizerManager implements VoiceSynthesizerAble {

    private Context mContext;

    public OfflineVoiceSynthesizerManager(Context context) {
        mContext = context.getApplicationContext();
    }

    @Override
    public void startVoice(@NonNull String text) {

    }

    @Override
    public void startHelloVoice(@NonNull String text) {

    }

    @Override
    public void startHelloVoice(@NonNull String text, int type) {

    }

    @Override
    public void stopVoice() {

    }

    @Override
    public void destory() {

    }

    @Override
    public boolean isSpeaking() {
        return false;
    }
}

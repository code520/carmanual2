package com.pingzhi.cn.carmanual.voicemanager.recognizer;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechRecognizer;
import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.busmodel.AIMutualVoiceFinishModel;
import com.pingzhi.cn.carmanual.busmodel.MyVoiceModel;
import com.pingzhi.cn.carmanual.busmodel.RecordBeginModel;
import com.pingzhi.cn.carmanual.busmodel.RecordErrorModel;
import com.pingzhi.cn.carmanual.busmodel.RecordFinishModel;
import com.pingzhi.cn.carmanual.busmodel.VoiceNotifyModel;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import static android.content.ContentValues.TAG;

/**
 * Created by ${fanjie} on 2017/6/23.
 */

public class OnlineRecognizerManager implements RecognizerAble{

    private SpeechRecognizer mSpeechRecognizer;
    private Context mContext;
    private SharedPreferences mSharedPreferences;
    private static final String PREFER_NAME = "com.iflytek.setting";

    public OnlineRecognizerManager(@NonNull Context context){
        mContext = context.getApplicationContext();
        mSpeechRecognizer = SpeechRecognizer.createRecognizer(mContext, new MyInitListener());
        mSharedPreferences = mContext.getSharedPreferences(PREFER_NAME,
                Activity.MODE_PRIVATE);
    }

    public void startRecord(){
//        FlowerCollector.onEvent(mContext, "iat_recognize");
        int resultCode = mSpeechRecognizer.startListening(mRecognizerListener);
        if (resultCode != ErrorCode.SUCCESS){

        }else {

        }
    }

    @Override
    public void stoprecord() {
        if (mSpeechRecognizer != null){
            mSpeechRecognizer.stopListening();
            mSpeechRecognizer.cancel();
        }
        EventBus.getDefault().post(new RecordFinishModel());
    }

    @Override
    public void destory() {
        if (mSpeechRecognizer == null){
            return;
        }
        mSpeechRecognizer.destroy();
    }

    @Override
    public boolean isListening() {
        if (mSpeechRecognizer == null){
            return false;
        }
        return mSpeechRecognizer.isListening();
    }

    private static final String RESULT_TYPE = "json";
    private static final String LANGUAGE = "zh_cn";
    private static final String ACCENT = "mandarin";
    private static final int SECOND = 1000;
    private static final int LONG_NO_VOICE = 5 * SECOND;
    private static final int LONG_STOP = 1 * SECOND;
    private void setParam(){
        mSpeechRecognizer.setParameter(SpeechConstant.PARAMS, null);
        mSpeechRecognizer.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
        mSpeechRecognizer.setParameter(SpeechConstant.RESULT_TYPE, RESULT_TYPE);
        mSpeechRecognizer.setParameter(SpeechConstant.LANGUAGE, LANGUAGE);
        mSpeechRecognizer.setParameter(SpeechConstant.ACCENT, ACCENT);
        // 设置语音前端点:静音超时时间，即用户多长时间不说话则当做超时处理
        mSpeechRecognizer.setParameter(SpeechConstant.VAD_BOS, LONG_NO_VOICE + "");
        // 设置语音后端点:后端点静音检测时间，即用户停止说话多长时间内即认为不再输入， 自动停止录音
        mSpeechRecognizer.setParameter(SpeechConstant.VAD_EOS, LONG_STOP + "");
        // 设置标点符号,设置为"0"返回结果无标点,设置为"1"返回结果有标点
        mSpeechRecognizer.setParameter(SpeechConstant.ASR_PTT, "1");//SpeechConstant.ASR_PTT, mSharedPreferences.getString("iat_punc_preference", "0")

        // 设置音频保存路径，保存音频格式支持pcm、wav，设置路径为sd卡请注意WRITE_EXTERNAL_STORAGE权限
        // 注：AUDIO_FORMAT参数语记需要更新版本才能生效
        mSpeechRecognizer.setParameter(SpeechConstant.AUDIO_FORMAT,"wav");
        mSpeechRecognizer.setParameter(SpeechConstant.ASR_AUDIO_PATH, Environment.getExternalStorageDirectory()+"/msc/iat.wav");
    }

    private class MyInitListener implements InitListener{
        private static final String TAG = "MyInitListener";
        @Override
        public void onInit(int code) {
            Log.e(TAG, "onInit: " + code, null);
            if (code != ErrorCode.SUCCESS){
                showTip("初始化失败，错误码：" + code);
            }else {
                setParam();
            }
        }
    }

    private RecognizerListener mRecognizerListener = new RecognizerListener() {
        @Override
        public void onVolumeChanged(int i, byte[] bytes) {
            Log.e(TAG, "onVolumeChanged: " + i, null);
            VoiceNotifyModel voice = new VoiceNotifyModel();
            voice.volum = i;
            EventBus.getDefault().post(voice);
        }

        @Override
        public void onBeginOfSpeech() {
            EventBus.getDefault().post(new RecordBeginModel());
        }

        @Override
        public void onEndOfSpeech() {
            Log.e(TAG, "onEndOfSpeech: ", null);
        }

        @Override
        public void onResult(RecognizerResult recognizerResult, boolean b) {
            printResult(recognizerResult, b);
        }

        @Override
        public void onError(SpeechError speechError) {
            //被认为没有输入

            Log.e(TAG, "onError: " + speechError.getErrorCode(), null);

            if (speechError.getErrorCode() == ErrorCode.MSP_ERROR_NO_DATA){
                EventBus.getDefault().post(new RecordErrorModel(speechError.getErrorCode()));
            }
        }

//        boolean isNoVoice = false;
        @Override
        public void onEvent(int i, int i1, int i2, Bundle bundle) {
            Log.e(TAG, "onEvent: i=" + i, null);
            Log.e(TAG, "onEvent: i1=" + i1, null);
            Log.e(TAG, "onEvent: i2=" + i2, null);
        }
    };

    private String tempText = "";
    private void printResult(RecognizerResult result, boolean isLast){
        Log.e(TAG, "printResult:" + tempText + "===", null);
        String text = parseIatResult(result.getResultString());
        if (text != null && !text.equals("") && !text.equals("null")){
            tempText += text;
        }
        if (isLast){
            if (tempText != null && !tempText.equals("")){
                if (!Constant.isMutibleVoice){
                    EventBus.getDefault().post(new MyVoiceModel(tempText));
                }else {
                    AIMutualVoiceFinishModel aiMutualVoiceFinishModel = new AIMutualVoiceFinishModel();
                    aiMutualVoiceFinishModel.content = tempText;
                    EventBus.getDefault().post(aiMutualVoiceFinishModel);
                }
                tempText = "";
            }else {
                EventBus.getDefault().post(new RecordErrorModel(10118));
            }
        }
    }

    public  String parseIatResult(String json) {
        StringBuffer ret = new StringBuffer();
        try {
            JSONTokener tokener = new JSONTokener(json);
            JSONObject joResult = new JSONObject(tokener);

            JSONArray words = joResult.getJSONArray("ws");
            for (int i = 0; i < words.length(); i++) {
                // 转写结果词，默认使用第一个结果
                JSONArray items = words.getJSONObject(i).getJSONArray("cw");
                JSONObject obj = items.getJSONObject(0);
                String str = obj.getString("w");
                if (str != null && !str.equals("")){
                    ret.append(str);
                }
//				如果需要多候选结果，解析数组其他字段
//				for(int j = 0; j < items.length(); j++)
//				{
//					JSONObject obj = items.getJSONObject(j);
//					ret.append(obj.getString("w"));
//				}
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ret.toString();
    }

    private void showTip(String s) {
        Toast.makeText(mContext, s, Toast.LENGTH_SHORT);
    }
}

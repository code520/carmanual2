package com.pingzhi.cn.carmanual.utils.ai.table;

import com.pingzhi.cn.carmanual.db.model.Insurance;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;
import com.pingzhi.cn.carmanual.utils.ai.TableOperatorAble;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public class InsuranceOperator implements TableOperatorAble {
    @Override
    public ShowPagerModel getPagerModelByTableAndIntent(AIAnswerModel aiAnswerModel) {
//        List<Insurance> insuranceList= DbHelper.getInstance().getDaoSession().getInsuranceDao().queryBuilder().where(InsuranceDao.Properties.Id.eq(aiAnswerModel.getAnswer().getResult().getId())).list();
//        if (insuranceList != null && insuranceList.size() > 0){
//            Insurance brokedown = insuranceList.get(0);
//            return brokedown.getShowPagerModel(aiAnswerModel);
//        }else {
//            ShowPagerModel showPagerModel = new ShowPagerModel(aiAnswerModel);
//            return showPagerModel;
//        }
        Insurance insurance = new Insurance();
        return insurance.getShowPagerModel(aiAnswerModel);
    }
}

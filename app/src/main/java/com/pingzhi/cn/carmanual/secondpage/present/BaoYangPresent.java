package com.pingzhi.cn.carmanual.secondpage.present;

import android.support.v4.app.Fragment;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.busmodel.AIAnswerListModel;
import com.pingzhi.cn.carmanual.db.DbHelper;
import com.pingzhi.cn.carmanual.db.model.Maintenance;
import com.pingzhi.cn.carmanual.db.model.MaintenanceDao;
import com.pingzhi.cn.carmanual.present.ShowtipPresent;
import com.pingzhi.cn.carmanual.secondpage.fragment.baoyang.BaoYanFragment;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${fanjie} on 2017/6/21.
 */

public class BaoYangPresent implements BasePresent {
    @Override
    public String[] getTypeTitles() {
        return new String[]{
                "仪表台", "轮胎", "", "", "", "", "", "", "", "", "发动机", "方向盘"
        };
    }

    @Override
    public int[] getTypeIcons() {
        return new int[]{
                R.mipmap.yibiaotai_, R.mipmap.luntai_, R.mipmap.trans, R.mipmap.trans,
                R.mipmap.trans, R.mipmap.trans, R.mipmap.trans, R.mipmap.trans,
                R.mipmap.trans, R.mipmap.trans, R.mipmap.fadongji_, R.mipmap.fangxiangp_
        };
    }

    @Override
    public int[] getSelectIcons() {
        return new int[]{
                R.mipmap.yibiaotai, R.mipmap.luntai, R.mipmap.trans, R.mipmap.trans,
                R.mipmap.trans, R.mipmap.trans, R.mipmap.trans, R.mipmap.trans,
                R.mipmap.trans, R.mipmap.trans, R.mipmap.fadongji, R.mipmap.fangxiangpan
        };
    }

    @Override
    public String getType() {
        return "保 养";
    }

    @Override
    public void query(String world) {
        List<ShowPagerModel> showPagerModels = new ArrayList<>();
        List<Maintenance> datas = DbHelper.getInstance().getDaoSession().getMaintenanceDao().queryBuilder().where(MaintenanceDao.Properties.Group.like("%" + world + "%")).list();
        for (Maintenance maintenance : datas){
            showPagerModels.addAll(maintenance.getShowPagerModelList());
        }

        EventBus.getDefault().postSticky(new AIAnswerListModel(showPagerModels));

    }

    private BaoYanFragment mBaoYanFragment;

    public BaoYanFragment getBaoYanFragment() {
        if (mBaoYanFragment == null){
            mBaoYanFragment = new BaoYanFragment();
        }
        return mBaoYanFragment;
    }

    @Override
    public <T> Fragment getFragment(T datas) {
        getBaoYanFragment().mShowtipPresent = (ShowtipPresent) datas;
        return getBaoYanFragment();
    }

    @Override
    public boolean canScroll() {
        return false;
    }

    @Override
    public boolean canScroll(int position) {
        if (position == 0 || position == 1 || position == 10 || position == 11){
            return true;
        }
        return false;
    }
}

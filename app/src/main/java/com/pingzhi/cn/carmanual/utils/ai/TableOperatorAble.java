package com.pingzhi.cn.carmanual.utils.ai;

import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public interface TableOperatorAble {

    ShowPagerModel getPagerModelByTableAndIntent(AIAnswerModel aiAnswerModel);
}

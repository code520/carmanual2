package com.pingzhi.cn.carmanual.secondpage.fragment.chexian;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.TiaoKuanModel;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.ownview.MoreTextView;


/**
 * Created by ${fanjie} on 2017/7/9.
 */

public class TiaoKuanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private TiaoKuanModel mTiaoKuanModel;

    public TiaoKuanAdapter(TiaoKuanModel mTiaoKuanModel) {
        this.mTiaoKuanModel = mTiaoKuanModel;
    }

    private LayoutInflater mLayoutInflater;

    public LayoutInflater getLayoutInflater(Context context) {
        if (mLayoutInflater == null){
            mLayoutInflater = LayoutInflater.from(context);
        }
        return mLayoutInflater;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1){
            View view = getLayoutInflater(parent.getContext()).inflate(R.layout.item_tiao_kuan, parent, false);
            return new ViewHolder(view);
        }else {
            View view = getLayoutInflater(parent.getContext()).inflate(R.layout.item_tiao_kuan_title, parent, false);
            return new ViewHolder0(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position > 0){
            TiaoKuanAdapter.ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.bindData(mTiaoKuanModel.getDatas().get(position - 1));
        } else {
            ViewHolder0 viewHolder0 = (ViewHolder0) holder;
            viewHolder0.bindData(mTiaoKuanModel.title);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0){
            return 0;
        }
        return 1;
    }

    @Override
    public int getItemCount() {
        return mTiaoKuanModel.getDatas().size() + 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView title;
        private MoreTextView content;
        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.textView2);
            content = (MoreTextView) itemView.findViewById(R.id.textView3);
//            content.getTextView().setMovementMethod(new ScrollingMovementMethod());
        }

        public void bindData(TiaoKuanModel.TiaoKuanItem tiaoKuanModel){
            title.setText(tiaoKuanModel.type);
            content.setText(tiaoKuanModel.content);
        }
    }

    public class ViewHolder0 extends RecyclerView.ViewHolder{

        private TextView title;
        public ViewHolder0(View itemView) {
            super(itemView);
            title = (TextView) itemView;
        }

        public void bindData(String title){
            this.title.setText(title);
        }
    }
}

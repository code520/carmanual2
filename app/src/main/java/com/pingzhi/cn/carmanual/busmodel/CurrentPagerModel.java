package com.pingzhi.cn.carmanual.busmodel;

/**
 * Created by ${fanjie} on 2017/7/18.
 */

public class CurrentPagerModel {

    public int currentPager;

    public int totalpager;

    public CurrentPagerModel(int currentPager, int totalpager) {
        this.currentPager = currentPager;
        this.totalpager = totalpager;
    }
}

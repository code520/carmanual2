package com.pingzhi.cn.carmanual.jpushreceiver;

import com.pingzhi.cn.carmanual.jpushreceiver.action.ClickAction;
import com.pingzhi.cn.carmanual.jpushreceiver.action.ConnectionChangeAction;
import com.pingzhi.cn.carmanual.jpushreceiver.action.MessageReceiveAction;
import com.pingzhi.cn.carmanual.jpushreceiver.action.NotificationAction;
import com.pingzhi.cn.carmanual.jpushreceiver.action.OpenAction;
import com.pingzhi.cn.carmanual.jpushreceiver.action.RegisterAction;
import com.pingzhi.cn.carmanual.router.ActionOperator;
import com.pingzhi.cn.carmanual.router.RegisterOperator;
import com.pingzhi.cn.carmanual.router.RouterAble;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by ${fanjie} on 2017/6/22.
 */

public class JpushMsgRouter implements RouterAble<JpushMsgReceiveable> {
    @Override
    public void initRouter(RegisterOperator<JpushMsgReceiveable> registerOperator) {
        registerOperator.registerOperator(JPushInterface.ACTION_CONNECTION_CHANGE, new ActionOperator<JpushMsgReceiveable>(){
            @Override
            public JpushMsgReceiveable createService() {
                return new ConnectionChangeAction();
            }
        });

        registerOperator.registerOperator(JPushInterface.ACTION_NOTIFICATION_CLICK_ACTION, new ActionOperator<JpushMsgReceiveable>(){
            @Override
            public JpushMsgReceiveable createService() {
                return new ClickAction();
            }
        });

        registerOperator.registerOperator(JPushInterface.ACTION_NOTIFICATION_OPENED, new ActionOperator<JpushMsgReceiveable>(){
            @Override
            public JpushMsgReceiveable createService() {
                return new OpenAction();
            }
        });

        registerOperator.registerOperator(JPushInterface.ACTION_NOTIFICATION_RECEIVED, new ActionOperator<JpushMsgReceiveable>(){
            @Override
            public JpushMsgReceiveable createService() {
                return new NotificationAction();
            }
        });

        registerOperator.registerOperator(JPushInterface.ACTION_MESSAGE_RECEIVED, new ActionOperator<JpushMsgReceiveable>(){
            @Override
            public JpushMsgReceiveable createService() {
                return new MessageReceiveAction();
            }
        });

        registerOperator.registerOperator(JPushInterface.ACTION_REGISTRATION_ID, new ActionOperator<JpushMsgReceiveable>(){
            @Override
            public JpushMsgReceiveable createService() {
                return new RegisterAction();
            }
        });
    }
}

package com.pingzhi.cn.carmanual.utils.recycleclickhelper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by ${fanjie} on 17/4/13.
 */

public class OnRecyclerViewTouchEvent implements RecyclerView.OnItemTouchListener {

    private static final int TAPUP = 0;

    private static final int LONGPRESS = 1;

    private int currentEvent = -1;

    public static final String TAG = "=OnRecycler=";

    private GestureDetector mGestureDetector;

    private View selectCell;

    private RecyclerView mRecyclerView;

    private RecyclerViewItemClickListener mRecyclerViewItemClickListener;

    private RecyclerViewItemLongPressListener mRecyclerViewItemLongPressListener;

    private RecyclerViewItemLongPressUpListener mRecyclerViewItemLongPressUpListener;

    public OnRecyclerViewTouchEvent(Context context) {
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                Log.e(TAG, "onSingleTapUp: ", null);
                currentEvent = TAPUP;
                if (getTargetView(selectCell, e) == null){
                    if (currentEvent == TAPUP && mRecyclerViewItemClickListener != null){
                        mRecyclerViewItemClickListener.onClick(mRecyclerView, selectCell, mRecyclerView.getChildAdapterPosition(selectCell));
                        return true;
                    }
                }

                return false;

            }

            @Override
            public void onLongPress(MotionEvent e) {
                super.onLongPress(e);
                Log.e(TAG, "onLongPress: ", null);
                currentEvent = LONGPRESS;
                if (currentEvent == LONGPRESS && mRecyclerViewItemLongPressListener != null){
                    mRecyclerViewItemLongPressListener.onLongPress(mRecyclerView, selectCell, mRecyclerView.getChildAdapterPosition(selectCell));
                }
            }
        });
    }

    public void setmRecyclerViewItemClickListener(RecyclerViewItemClickListener mRecyclerViewItemClickListener) {
        this.mRecyclerViewItemClickListener = mRecyclerViewItemClickListener;
    }

    public void setmRecyclerViewItemLongPressListener(RecyclerViewItemLongPressListener mRecyclerViewItemLongPressListener) {
        this.mRecyclerViewItemLongPressListener = mRecyclerViewItemLongPressListener;
    }

    public void setmRecyclerViewItemLongPressUpListener(RecyclerViewItemLongPressUpListener mRecyclerViewItemLongPressUpListener) {
        this.mRecyclerViewItemLongPressUpListener = mRecyclerViewItemLongPressUpListener;
    }

    /**
     * Silently observe and/or take over touch events sent to the RecyclerView
     * before they are handled by either the RecyclerView itself or its child views.
     * <p>
     * <p>The onInterceptTouchEvent methods of each attached OnItemTouchListener will be run
     * in the order in which each listener was added, before any other touch processing
     * by the RecyclerView itself or child views occurs.</p>
     *
     * @param rv
     * @param e  MotionEvent describing the touch event. All coordinates are in
     *           the RecyclerView's coordinate system.
     * @return true if this OnItemTouchListener wishes to begin intercepting touch events, false
     * to continue with the current behavior and continue observing future events in
     * the gesture.
     */
    private boolean isInterrupt;
    private boolean shouldInterrupt;
    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        this.selectCell = rv.findChildViewUnder(e.getX(), e.getY());
        this.mRecyclerView = rv;
        shouldInterrupt = mGestureDetector.onTouchEvent(e);

        if (currentEvent == LONGPRESS && e.getAction() == MotionEvent.ACTION_UP){
            isInterrupt = true;
        }

        if (e.getAction() == MotionEvent.ACTION_DOWN){
            currentEvent = -1;
            isInterrupt = false;
        }

        if (e.getAction() == MotionEvent.ACTION_UP){
            if (currentEvent == LONGPRESS && mRecyclerViewItemLongPressUpListener != null){
                mRecyclerViewItemLongPressUpListener.onLognPressUp(mRecyclerView, selectCell, mRecyclerView.getChildAdapterPosition(selectCell));
            }
        }

        return isInterrupt || shouldInterrupt;
    }

    private boolean inRangeOfView(View view, MotionEvent ev){
        return inRangeOfView(view, (int)ev.getRawX(), (int)ev.getRawY());
    }

    /**
     *
     * @param view 需要判断的view
     * @param rawX 相对于屏幕
     * @param rawY 相对于屏幕
     * @return
     */
    private boolean inRangeOfView(View view, int rawX, int rawY){
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        int x = location[0];
        int y = location[1];
        if(rawX < x || rawX > (x + view.getWidth()) || rawY < y || rawY > (y + view.getHeight())){
            return false;
        }
        return true;
    }

    private View getTargetView(View parent, MotionEvent ev){
        return getTargetView(parent, (int)ev.getRawX(), (int)ev.getRawY());
    }

    private View getTargetView(View parent, int rawX, int rawY){
        if (parent == null){
            return null;
        }
        ArrayList<View> viewLists = parent.getTouchables();
        View target = null;
        for (View view : viewLists){
            if (inRangeOfView(view, rawX, rawY)){
                target = view;
                break;
            }
        }
        return target;
    }

    /**
     * Process a touch event as part of a gesture that was claimed by returning true from
     * a previous call to {@link #onInterceptTouchEvent}.
     *
     * @param rv
     * @param e  MotionEvent describing the touch event. All coordinates are in
     */
    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    /**
     * Called when a child of RecyclerView does not want RecyclerView and its ancestors to
     * intercept touch events with
     * {@link ViewGroup#onInterceptTouchEvent(MotionEvent)}.
     *
     * @param disallowIntercept True if the child does not want the parent to
     *                          intercept touch events.
     * @see ViewParent#requestDisallowInterceptTouchEvent(boolean)
     */
    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}

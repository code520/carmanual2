package com.pingzhi.cn.carmanual.utils.ai.table;

import com.pingzhi.cn.carmanual.db.model.Maintenance;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;
import com.pingzhi.cn.carmanual.utils.ai.TableOperatorAble;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public class MaintenanceOperator implements TableOperatorAble {
    @Override
    public ShowPagerModel getPagerModelByTableAndIntent(AIAnswerModel aiAnswerModel) {
//        List<Maintenance> maintenancesList = DbHelper.getInstance().getDaoSession().getMaintenanceDao().queryBuilder().where(MaintenanceDao.Properties.Id.eq(aiAnswerModel.getAnswer().getResult().getId())).list();
//        if (maintenancesList != null && maintenancesList.size() > 0){
//            Maintenance maintenance = maintenancesList.get(0);
//            return maintenance.getShowPagerModel(aiAnswerModel);
//        }else {
//            ShowPagerModel showPagerModel = new ShowPagerModel(aiAnswerModel);
//            return showPagerModel;
//        }
        Maintenance maintenance = new Maintenance();
        return maintenance.getShowPagerModel(aiAnswerModel);
    }
}

package com.pingzhi.cn.carmanual.db.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by ${fanjie} on 2017/7/24.
 */
@Entity(nameInDb = "gs8_manual_v2_conf", createInDb = false)
public class ManualV2Conf {

    @Property(nameInDb = "id")
    @Id(autoincrement = true)
    public long id;

    public String parent_grp;

    public String field;

    public String interactive;

    public String list;

    public String upList;

    @Generated(hash = 80028202)
    public ManualV2Conf(long id, String parent_grp, String field,
            String interactive, String list, String upList) {
        this.id = id;
        this.parent_grp = parent_grp;
        this.field = field;
        this.interactive = interactive;
        this.list = list;
        this.upList = upList;
    }

    @Generated(hash = 565133781)
    public ManualV2Conf() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getParent_grp() {
        return this.parent_grp;
    }

    public void setParent_grp(String parent_grp) {
        this.parent_grp = parent_grp;
    }

    public String getField() {
        return this.field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getInteractive() {
        return this.interactive;
    }

    public void setInteractive(String interactive) {
        this.interactive = interactive;
    }

    public String getList() {
        return this.list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getUpList() {
        return this.upList;
    }

    public void setUpList(String upList) {
        this.upList = upList;
    }
}

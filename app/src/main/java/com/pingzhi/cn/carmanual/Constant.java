package com.pingzhi.cn.carmanual;

/**
 * Created by ${fanjie} on 2017/6/21.
 */

public class Constant {

    public static boolean isMutibleVoice = false;

    public static final int CHE_XIAN = 0;

    public static final int CHANG_SHI = 1;

    public static final int WEI_XIU = 2;

    public static final int BAO_YANG = 3;

    public static final int QA = 4;

    public static String imageDir;
    //http://47.92.130.0:5000/gac/demo
    public static final String BASE_URL = "http://47.93.162.158:8585";

    public static final String AI_BASE_URL = "http://47.92.130.0:5000";

    public static final String IMAGE_BASE = "ftp://47.93.162.158";

    public static final String UPLOAD_USERINFO = BASE_URL + "/chechi/app/first/use";

    public static final String RANDOM_TALK = BASE_URL + "/chechi/app/getRandomTalk";

    public static final String VERSION_UPDATE = "/chechi/app/andriod/versionControl";

    public static final String AI_QUESTION_QUERY = AI_BASE_URL + "/gac/demo";

    public static final String DOWNLOAD_IMAGE = IMAGE_BASE + "/img/";

    public static final String UPDATEDATA_SQL_IMG = BASE_URL + "/chechi/app/update/data";


}

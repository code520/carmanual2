package com.pingzhi.cn.carmanual.utils;

import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.model.GetNewVersionModel;
import com.pingzhi.cn.carmanual.utils.json.JsonUtils;

import java.util.HashMap;
import java.util.Map;

import cn.fanjie.com.cjvolley.NetworkFactory;
import cn.fanjie.com.cjvolley.OnNetworkReturn;

/**
 * Created by ${fanjie} on 2017/7/2.
 */

public class APKUpdate {

    public void updateAPK(String currentVersion){
        Map<String, Object> params = new HashMap<>();
        params.put("currentVersion", currentVersion);
        NetworkFactory.getInstance().request(Constant.VERSION_UPDATE, params, new OnNetworkReturn<String>() {
            @Override
            protected void onSuccess(String data) {
                GetNewVersionModel getNewVersionModel = JsonUtils.parseObject(data, GetNewVersionModel.class);
                if (getNewVersionModel.getCode() == 200 && "success".equals(getNewVersionModel.getMsg())){

                }

            }

            @Override
            protected void onError(Exception e) {

            }
        });
    }

    private void downLoadAPK(String url){

    }
}

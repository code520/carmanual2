package com.pingzhi.cn.carmanual.router;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by tusm on 17/5/12.
 */
 class CallBackOperation2<T> implements RegisterOperator, ObtainActionOperatorAble{

    final ArrayList<T> mOperatorCache = new ArrayList<>();
    int sNextPerOperatorCacheIndex = 0;

    public CallBackOperation2(RouterAble routerAble){
        routerAble.initRouter(this);
    }

    private  final HashMap<String, ActionOperator> OPERATOR_HASH_MAP = new HashMap<>();

    @Override
    public void registerOperator(String action, ActionOperator actionOperator){
        actionOperator.mCallIndex = sNextPerOperatorCacheIndex++;
        OPERATOR_HASH_MAP.put(action, actionOperator);
    }

    @Override
    public T getActionOperator(String action){
        ActionOperator<T> actionOperator = OPERATOR_HASH_MAP.get(action);
        if (actionOperator == null){
            throw new RuntimeException("you do not had register the action");
        }
        return actionOperator == null? null : actionOperator.getActionOperator(this);
    }
}

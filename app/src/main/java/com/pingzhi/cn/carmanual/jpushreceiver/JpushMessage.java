package com.pingzhi.cn.carmanual.jpushreceiver;

/**
 * Created by ${fanjie} on 2017/6/27.
 */

public class JpushMessage {


    /**
     * thumb :
     * title : 标题1850
     * imageUrl : http://localhost:8080/chechi/html/1498560887993.html
     * pushTime : 1498560887
     * content : 简介1850
     * flag : 2
     * guildUrl :
     * newsUrl :
     */

    public String thumb;
    public String title;
    public String imageUrl;
    public String pushTime;
    public String content;
    public String flag;
    public String guildUrl;
    public String newsUrl;
}

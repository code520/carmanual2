package com.pingzhi.cn.carmanual.utils.ai.table;

import com.pingzhi.cn.carmanual.db.model.ManualV3;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;
import com.pingzhi.cn.carmanual.utils.ai.TableOperatorAble;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public class ManualOperator implements TableOperatorAble {
    @Override
    public ShowPagerModel getPagerModelByTableAndIntent(AIAnswerModel aiAnswerModel) {
//        List<ManualV3> manualList = DbHelper.getInstance().getDaoSession().getManualV3Dao().queryBuilder().where(ManualV3Dao.Properties.Id.eq(aiAnswerModel.getAnswer().getResult().getId())).list();
//        if (manualList != null && manualList.size() > 0){
//            ManualV3 manual = manualList.get(0);
//            return manual.getShowPagerModel(aiAnswerModel);
//        }else {
//            ShowPagerModel showPagerModel = new ShowPagerModel(aiAnswerModel);
//            return showPagerModel;
//        }
        ManualV3 manualV3 = new ManualV3();
        return manualV3.getShowPagerModel(aiAnswerModel);
    }
}

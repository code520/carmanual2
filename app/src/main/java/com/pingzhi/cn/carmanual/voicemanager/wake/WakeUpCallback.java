package com.pingzhi.cn.carmanual.voicemanager.wake;

/**
 * Created by ${fanjie} on 2017/6/23.
 */

public interface WakeUpCallback {

    void wakeupSuccess(String world, byte[] data, int offset, int length);

    void wakeupEixt();
}

package com.pingzhi.cn.carmanual.router;

import java.util.ArrayList;

/**
 * Created by tusm on 17/5/12.
 */
public class  ActionOperator<T>{
    int mCallIndex = -1;
     T getActionOperator(CallBackOperation2<T> operationAction){
        ArrayList<T> cache = operationAction.mOperatorCache;
        T callBack;
        synchronized (cache){
            if (cache.size() == 0){
                for (int i = 0; i < operationAction.sNextPerOperatorCacheIndex; i++){
                    cache.add(null);
                }
            }else {
                callBack = cache.get(mCallIndex);
                if (callBack != null){
                    return callBack;
                }
            }
            callBack = createService();
            cache.set(mCallIndex, callBack);
            return callBack;
        }
    }

    public T createService(){
        throw new RuntimeException("not implement");
    }
}

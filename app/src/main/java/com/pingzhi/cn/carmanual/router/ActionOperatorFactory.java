package com.pingzhi.cn.carmanual.router;

/**
 * Created by tusm on 17/5/12.
 */
public class ActionOperatorFactory {

    public static <T> ObtainActionOperatorAble getActionOperator(RouterAble<T> routerAble){
        return new CallBackOperation2<T>(routerAble);
    }
}

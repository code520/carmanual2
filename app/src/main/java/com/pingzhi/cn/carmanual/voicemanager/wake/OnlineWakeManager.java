package com.pingzhi.cn.carmanual.voicemanager.wake;

import android.content.Context;
import android.util.AndroidRuntimeException;
import android.util.Log;

import com.baidu.speech.EventListener;
import com.baidu.speech.EventManager;
import com.baidu.speech.EventManagerFactory;
import com.pingzhi.cn.carmanual.busmodel.NeedWifyModel;
import com.pingzhi.cn.carmanual.busmodel.WakeupSuccessModel;
import com.pingzhi.cn.carmanual.utils.network.NetWorkRecongniseUtils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by ${fanjie} on 2017/6/23.
 */

public class OnlineWakeManager implements WakeupAble{

    private EventManager mEventManager;
    private HashMap params;
    private WakeUpCallback mWakeUpCallback;
    private EventListener mEventListener;
    private Context mContext;

    public OnlineWakeManager(Context context) {
        mContext = context.getApplicationContext();
        // 3) 通知唤醒管理器, 启动唤醒功能
        params = new HashMap();
        params.put("kws-file", "assets:///WakeUp.bin"); // 设置唤醒资源, 唤醒资源请到 http://yuyin.baidu.com/wake#m4 来评估和导出
    }

    private boolean isWakeUp;
    @Override
    public void registerListener(WakeUpCallback wakeUpCallback){
        this.mWakeUpCallback = wakeUpCallback;
        mEventListener = new EventListener() {
            @Override
            public void onEvent(String name, String params, byte[] bytes, int i, int i1) {
                try {
                    JSONObject json = new JSONObject(params);
                    if ("wp.data".equals(name)) { // 每次唤醒成功, 将会回调name=wp.data的时间, 被激活的唤醒词在params的word字段
                        isWakeUp = true;
                        String word = json.getString("word"); // 唤醒词
                        if (NetWorkRecongniseUtils.isNetworkConnected()){
                            mWakeUpCallback.wakeupSuccess(word, bytes, i, i1);
                            EventBus.getDefault().post(new WakeupSuccessModel());
                        }else {
                            EventBus.getDefault().post(new NeedWifyModel());
                        }
                    } else if ("wp.exit".equals(name)) {
                        // 唤醒已经停止
                        isWakeUp = false;
                        mWakeUpCallback.wakeupEixt();
                    }
                } catch (JSONException e) {
                    throw new AndroidRuntimeException(e);
                }
            }
        };
        getEventManager().registerListener(mEventListener);
    }

    @Override
    public void unRegisterListener() {
        getEventManager().unregisterListener(mEventListener);
    }


    public EventManager getEventManager() {
        if (mEventManager == null){
            mEventManager = EventManagerFactory.create(mContext, "wp");
        }
        return mEventManager;
    }

    @Override
    public void wakeUpStart(){
        Log.e("OnlineWakeManager", "wakeUpStart: ", null);
        getEventManager().send("wp.start", new JSONObject(params).toString(), null, 0, 0);
    }

    @Override
    public void wakeUpStop(){
        Log.e("OnlineWakeManager", "wakeUpStop: " + isWakeUp, null);
//        if (isWakeUp){
            getEventManager().send("wp.stop", null, null, 0, 0);
//        }else {
//            EventBus.getDefault().post(new NotStopModel());
//        }
    }


}

package com.pingzhi.cn.carmanual.secondpage.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.pingzhi.cn.carmanual.present.ShowtipPresent;

/**
 * Created by ${fanjie} on 2017/7/7.
 */

public class BaseFragment extends Fragment {

    public ShowtipPresent mShowtipPresent;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}

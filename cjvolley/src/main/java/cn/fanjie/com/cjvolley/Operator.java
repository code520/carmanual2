package cn.fanjie.com.cjvolley;

import com.android.volley.Request;

import java.util.Map;

/**
 * Created by ${fanjie} on 16/10/3.
 */

public interface Operator<T> {

     Request<T> operator(int cjMethod, String url, Map<String, String> params, OnNetworkReturn<T> callback);

}

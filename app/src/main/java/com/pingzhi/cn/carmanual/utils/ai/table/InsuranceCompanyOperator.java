package com.pingzhi.cn.carmanual.utils.ai.table;

import com.pingzhi.cn.carmanual.db.model.InsuranceCompany;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;
import com.pingzhi.cn.carmanual.utils.ai.TableOperatorAble;

/**
 * Created by ${fanjie} on 2017/7/4.
 */

public class InsuranceCompanyOperator implements TableOperatorAble {
    @Override
    public ShowPagerModel getPagerModelByTableAndIntent(AIAnswerModel aiAnswerModel) {
//        List<InsuranceCompany> insuranceCompanyList = DbHelper.getInstance().getDaoSession().getInsuranceCompanyDao().queryBuilder().where(InsuranceCompanyDao.Properties.Id.eq(aiAnswerModel.getAnswer().getResult().getId())).list();
//        if (insuranceCompanyList != null && insuranceCompanyList.size() > 0){
//            InsuranceCompany insuranceCompany = insuranceCompanyList.get(0);
//            return insuranceCompany.getShowPagerModel(aiAnswerModel);
//        }else {
//            ShowPagerModel showPagerModel = new ShowPagerModel(aiAnswerModel);
//            return showPagerModel;
//        }
        InsuranceCompany insuranceCompany = new InsuranceCompany();
        return insuranceCompany.getShowPagerModel(aiAnswerModel);
    }
}

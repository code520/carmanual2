package cn.fanjie.com.cjvolley;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;

/**
 * Created by ${fanjie} on 16/10/3.
 */

public class StringRequestOperator extends BaseOperator<String> {


    @Override
    Request<String> requestWithNoReturnParams(int Method, String url, OnNetworkReturn callback) {
        return new StringRequest(Method, url, listener.get(), errorListener.get());
    }

    @Override
    Request<String> postWithParams(String url, final Map<String, String> params, OnNetworkReturn callback) {
        return new StringRequest(Request.Method.POST, url, listener.get(), errorListener.get()){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
    }
}
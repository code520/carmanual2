package com.pingzhi.cn.carmanual.voicemanager.voicesynthesizer;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by ${fanjie} on 2017/6/26.
 */

public class VoiceSynthesizerContext implements VoiceSynthesizerAble {

    private VoiceSynthesizerAble onlineVoiceSynthesizerManager;

    private VoiceSynthesizerAble offlineVoiceSynthesizerManager;

    private VoiceSynthesizerAble currentVoiceSynthesizerManager;

    private Context mContext;

    public VoiceSynthesizerAble getOnlineVoiceSynthesizerManager(Context context) {
        if (onlineVoiceSynthesizerManager == null){
            onlineVoiceSynthesizerManager = new OnlineVoiceSynthesizerManager(context);
        }
        return onlineVoiceSynthesizerManager;
    }

    public VoiceSynthesizerAble getOfflineVoiceSynthesizerManager(Context context) {
        if (offlineVoiceSynthesizerManager == null){
            offlineVoiceSynthesizerManager = new OfflineVoiceSynthesizerManager(context);
        }
        return offlineVoiceSynthesizerManager;
    }

    public VoiceSynthesizerContext(Context context) {
        mContext = context.getApplicationContext();
        currentVoiceSynthesizerManager = getOnlineVoiceSynthesizerManager(mContext);
    }

    @Override
    public void startVoice(@NonNull String text) {
        currentVoiceSynthesizerManager.startVoice(text);
    }

    @Override
    public void startHelloVoice(@NonNull String text) {
        currentVoiceSynthesizerManager.startHelloVoice(text);
    }

    @Override
    public void startHelloVoice(@NonNull String text, int type) {
        currentVoiceSynthesizerManager.startHelloVoice(text, type);
    }

    @Override
    public void stopVoice() {
        currentVoiceSynthesizerManager.stopVoice();
    }

    @Override
    public void destory() {
        currentVoiceSynthesizerManager.destory();
    }

    @Override
    public boolean isSpeaking() {
        return currentVoiceSynthesizerManager.isSpeaking();
    }
}

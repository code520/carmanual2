package com.pingzhi.cn.carmanual.db.zipdownload;

import android.app.Activity;

import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.db.DbHelper;
import com.pingzhi.cn.carmanual.model.UpdateVersionModel;
import com.pingzhi.cn.carmanual.utils.PreferenceUtils;
import com.pingzhi.cn.carmanual.utils.json.JsonUtils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.fanjie.com.cjvolley.NetworkFactory;
import cn.fanjie.com.cjvolley.OnNetworkReturn;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ${fanjie} on 2017/6/27.
 */

public class ZipDownLoaderUtils {

    public static void checkUpdate(final Activity activity){
        Map<String, Object> params = new HashMap<>();
        params.put("sqlVersion", PreferenceUtils.getInstance().getSqlVersion());
        params.put("zipVersion", PreferenceUtils.getInstance().getImgVersion());
        NetworkFactory.getInstance().request(Constant.UPDATEDATA_SQL_IMG, params, new OnNetworkReturn<String>() {
            @Override
            protected void onSuccess(String data) {
                UpdateVersionModel updateVersionModel = JsonUtils.parseObject(data, UpdateVersionModel.class);
                if (updateVersionModel.getCode() == 200 && "success".equals(updateVersionModel.getMsg())){
                    List<String> sqls = updateVersionModel.getContent().get(0).getMap().getSqls();
                    if (sqls != null && sqls.size() > 0){
                        try {
                            boolean success = DbHelper.getInstance().execSqls(sqls);
                            if (success){
                                PreferenceUtils.getInstance().updateSqlVersion();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    UpdateVersionModel.ContentBean contentBean = updateVersionModel.getContent().get(1);
                    List<String> imgs = contentBean.getMap().getSqls();
                    if (imgs != null && imgs.size() > 0){
                        for (String imgName : imgs){
                            String fileName = activity.getDir("carmanual", MODE_PRIVATE) + "/" + imgName;
                            File file = new File(fileName);
                            if (file.exists()){
                                file.delete();
                            }
                        }
                        PreferenceUtils.getInstance().updateImgVersion();
                    }
                    String zipName = contentBean.getZipName();
                    if (zipName != null && !"".equals(zipName)){
                        download(Constant.DOWNLOAD_IMAGE + contentBean.getVersion() + ".zip", activity);
                    }
                }
            }

            @Override
            protected void onError(Exception e) {

            }
        });
    }

    public static void download(String url, Activity activity){
//        if (PreferenceUtils.getInstance().needDownloadImg()){
            DownLoaderTask task = new DownLoaderTask(url, activity.getDir("carmanual", MODE_PRIVATE) + "/", activity);
            task.execute();
//        }
    }
}

package com.pingzhi.cn.carmanual.busmodel;

import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/5.
 */

public class AIAnswerListModel {

    public List<ShowPagerModel> mShowPagerModelList;

    public AIAnswerListModel(List<ShowPagerModel> showPagerModelList) {
        mShowPagerModelList = showPagerModelList;
    }
}

package com.pingzhi.cn.carmanual.secondpage.fragment.chexian;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chechi.aiandroid.R;
import com.pingzhi.cn.carmanual.MainApplication;
import com.pingzhi.cn.carmanual.busmodel.AIGonShiModel;
import com.pingzhi.cn.carmanual.secondpage.fragment.BaseFragment;
import com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model.GonShiModel;
import com.pingzhi.cn.carmanual.utils.ScreenUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/7.
 */

public class GonShiFragment extends BaseFragment {

    private View mView;
    private RecyclerView mRecyclerView;
    private RecycleAdapter mRecycleAdapter;
    private List<GonShiModel> datas = new ArrayList<>();

//    public void setDatas(List<GonShiModel> datas) {
//        this.datas = datas;
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_gon_shi, container, false);
        datas = new ArrayList<>();
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.recycle);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            /**
             * Draw any appropriate decorations into the Canvas supplied to the RecyclerView.
             * Any content drawn by this method will be drawn before the item views are drawn,
             * and will thus appear underneath the views.
             *
             * @param c      Canvas to draw into
             * @param parent RecyclerView this ItemDecoration is drawing into
             * @param state  The current state of RecyclerView
             */
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                super.onDraw(c, parent, state);
                drawHorizontalLine(c, parent, state);
            }

            /**
             * Retrieve any offsets for the given item. Each field of <code>outRect</code> specifies
             * the number of pixels that the item view should be inset by, similar to padding or margin.
             * The default implementation sets the bounds of outRect to 0 and returns.
             * <p>
             * <p>
             * If this ItemDecoration does not affect the positioning of item views, it should set
             * all four fields of <code>outRect</code> (left, top, right, bottom) to zero
             * before returning.
             * <p>
             * <p>
             * If you need to access Adapter for additional data, you can call
             * {@link RecyclerView#getChildAdapterPosition(View)} to get the adapter position of the
             * View.
             *
             * @param outRect Rect to receive the output.
             * @param view    The child view to decorate
             * @param parent  RecyclerView this ItemDecoration is decorating
             * @param state   The current state of RecyclerView.
             */
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.set(0, 0, 0, ScreenUtils.dip2px(view.getContext(), 1));
            }

            //画横线, 这里的parent其实是显示在屏幕显示的这部分
            public void drawHorizontalLine(Canvas c, RecyclerView parent, RecyclerView.State state){
                int left = parent.getPaddingLeft();
                int right = parent.getWidth() - parent.getPaddingRight() - ScreenUtils.dip2px(MainApplication.getAppContext(), 5);
                final int childCount = parent.getChildCount();
                Paint paint = new Paint();
                paint.setColor(Color.BLACK);
                paint.setAntiAlias(true);
                for (int i = 0; i < childCount; i++){
                    final View child = parent.getChildAt(i);

                    //获得child的布局信息
                    final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)child.getLayoutParams();
                    final int top = child.getBottom();
                    final int bottom = top + ScreenUtils.dip2px(MainApplication.getAppContext(), 1);
                    c.drawLine(left, top, right, bottom, paint);
                }
            }
        });
        mRecycleAdapter = new RecycleAdapter(datas);
        mRecyclerView.setAdapter(mRecycleAdapter);
        EventBus.getDefault().register(this);
        return mView;
    }

    private void telephone(String phone){
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+phone));
        startActivity(intent);
    }

    private void stepToUrl(String url){
        Uri uri = Uri.parse(url);
        Intent it = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(it);
    }

    @Subscribe(sticky = true)
    public void handleGonShis(AIGonShiModel aiGonShiModel){
        datas.clear();
        datas.addAll(aiGonShiModel.datas);
        mRecycleAdapter.notifyDataSetChanged();
        EventBus.getDefault().removeStickyEvent(aiGonShiModel);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
}

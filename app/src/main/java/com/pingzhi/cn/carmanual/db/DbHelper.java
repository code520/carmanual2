package com.pingzhi.cn.carmanual.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.RawRes;

import com.pingzhi.cn.carmanual.db.model.DaoMaster;
import com.pingzhi.cn.carmanual.db.model.DaoSession;

import org.greenrobot.greendao.database.Database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by ${fanjie} on 2017/6/22.
 */

public class DbHelper {

    private DbHelper(){

    }

    private String databaseFilename;
    private String path;
    private boolean checkDataBase(Context context, String dbName){
        SQLiteDatabase checkDB = null;
        File file = context.getDatabasePath(dbName);
        return file.exists();
    }

    /**
     * 复制数据库到手机指定文件夹下
     * @throws IOException
     */
    private void copyDataBase(Context context, @RawRes int dbId) throws IOException {
//        String databaseFilenames =DATABASE_PATH+dbName;
        File dir = new File(path);
        if(!dir.exists())//判断文件夹是否存在，不存在就新建一个
            dir.mkdir();
        FileOutputStream os = null;
        try{
            os = new FileOutputStream(databaseFilename);//得到数据库文件的写入流
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        InputStream is = context.getResources().openRawResource(dbId);//得到数据库文件的数据流
        byte[] buffer = new byte[8192];
        int count = 0;
        try{

            while((count=is.read(buffer))>0){
                os.write(buffer, 0, count);
                os.flush();
            }
        }catch(IOException e){

        }
        try{
            is.close();
            os.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public void initDbHelper(Context context, @RawRes int dbId) throws IOException {
        String dbName = context.getResources().getResourceEntryName(dbId) + ".db";
        path = "/data/data/" + context.getPackageName() + "/databases/";
        databaseFilename = path + dbName;
        if (checkDataBase(context, dbName)){

        }else {
            copyDataBase(context, dbId);
        }
        setupDatabase(context, dbName);
    }

    private DaoSession daoSession;

    public DaoSession getDaoSession() {
        return daoSession;
    }

    private DaoMaster mDaoMaster;

    public DaoMaster getDaoMaster() {
        return mDaoMaster;
    }

    public Boolean execSqls(final List<String> sqls) throws Exception {
        return daoSession.callInTx(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Database database = daoSession.getDatabase();
                for (String sql : sqls){
                    database.execSQL(sql);
                }
                return true;
            }
        });
    }

    public void execSql(final String sql){
        daoSession.runInTx(new Runnable() {
            @Override
            public void run() {
                daoSession.getDatabase().execSQL(sql);
            }
        });
    }

    /**
     * 配置数据库
     */
    private void setupDatabase(Context context, String dbName) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, dbName, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        mDaoMaster = new DaoMaster(db);
        daoSession = mDaoMaster.newSession();
    }

    public static DbHelper getInstance(){
        return InstanceHolder.mInstance;
    }

    private static class InstanceHolder {
        private static final DbHelper mInstance = new DbHelper();
    }

}

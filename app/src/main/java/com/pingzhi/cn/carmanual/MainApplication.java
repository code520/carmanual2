package com.pingzhi.cn.carmanual;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;

import com.bugtags.library.Bugtags;
import com.chechi.aiandroid.BuildConfig;

import com.chechi.aiandroid.R;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.pingzhi.cn.carmanual.db.DbHelper;
import com.pingzhi.cn.carmanual.present.UploadUserInfo;
import com.pingzhi.cn.carmanual.utils.gettopactivity.CJTools;
import com.pingzhi.cn.carmanual.utils.network.NetWorkRecongniseUtils;
import com.pingzhi.cn.carmanual.utils.network.NetworkConnectChangedReceiver;


import java.io.IOException;
import java.util.List;

import cn.fanjie.com.cjvolley.NetworkFactory;
import cn.jpush.android.api.JPushInterface;

/**
 * Created by ${fanjie} on 2017/6/21.
 */

public class MainApplication extends Application {

    private static final String TAG = "=MainApplication=";
    private static final boolean LEIBU = false;
    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "onCreate: " + getProcessName(this, Process.myPid()), null);
        if (getPackageName().equals(getProcessName(this, Process.myPid()))){
            mContext = this;
            NetworkFactory.init(this);
            Fresco.initialize(this);
            Bugtags.start("7e1235d2ef516b2ddae6a12ca387784d", this, Bugtags.BTGInvocationEventBubble);
//            FlowerCollector.setDebugMode(BuildConfig.DEBUG);
            registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
                @Override
                public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
//                    RefWatcher refWatcher = getRefWatcher(activity);
                    if (LEIBU){
//                        mRefWatcher.watch(activity);
                    }
                }

                @Override
                public void onActivityStarted(Activity activity) {

                }

                @Override
                public void onActivityResumed(Activity activity) {
//                    FlowerCollector.onResume(activity);
                    Bugtags.onResume(activity);
                }

                @Override
                public void onActivityPaused(Activity activity) {
//                    FlowerCollector.onPause(activity);
                    Bugtags.onPause(activity);
                }

                @Override
                public void onActivityStopped(Activity activity) {

                }

                @Override
                public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

                }

                @Override
                public void onActivityDestroyed(Activity activity) {

                }
            });

            SpeechUtility.createUtility(this, SpeechConstant.APPID +"=583fe408");////59423aa7
            CJTools.init(this);
            NetWorkRecongniseUtils.init(this);
            new NetworkConnectChangedReceiver().registerSelf(this);
            Constant.imageDir = "file://"+ getDir("carmanual", MODE_PRIVATE) + "/";
            UploadUserInfo.uploadInfo();
            try {
                DbHelper.getInstance().initDbHelper(this, R.raw.test);
            } catch (IOException e) {
                e.printStackTrace();
            }
            JPushInterface.setDebugMode(BuildConfig.DEBUG);
            JPushInterface.init(this);
            if (BuildConfig.DEBUG && LEIBU){
//                mRefWatcher = LeakCanary.install(this);
            }
        }else {

        }

    }

//    private RefWatcher mRefWatcher;
    public static String getProcessName(Context cxt, int pid) {
        ActivityManager am = (ActivityManager) cxt.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
        if (runningApps == null) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo procInfo : runningApps) {
            if (procInfo.pid == pid) {
                return procInfo.processName;
            }
        }
        return null;
    }

    private static Context mContext;
    public static Context getAppContext(){
        return mContext;
    }


}

package com.pingzhi.cn.carmanual.utils.gettopactivity;

import android.app.Activity;
import android.app.Application;
import android.os.Build;

/**
 * Created by ${fanjie} on 16/10/10.
 */

public class CJTools {

    private static GetTopActivity instace;
    public static void init(Application application){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH){
            GetTopActivityImpl1.initInstance(application);
            instace = GetTopActivityImpl1.getInstance();
        }else {
            instace = GetTopActivityImpl2.getInstance();
        }
    }

    public static Activity getTopActivity(){
        if (instace == null){
            throw new RuntimeException("请先在Application里面初始化");
        }
        return instace.getTopActivity();
    }
}

package com.pingzhi.cn.carmanual.ownview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;

/**
 * Created by ${fanjie} on 2017/6/19.
 */

public class ScrollTextView extends android.support.v7.widget.AppCompatTextView {

    public ScrollTextView(Context context) {
        this(context, null);
    }

    public ScrollTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScrollTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setEllipsize(TextUtils.TruncateAt.MARQUEE);
        setSingleLine(true);
        setMarqueeRepeatLimit(-1);
        setHorizontallyScrolling(true);
    }

    @Override
    public boolean isFocused() {
        return true;

    }
}

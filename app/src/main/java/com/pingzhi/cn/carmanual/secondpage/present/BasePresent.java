package com.pingzhi.cn.carmanual.secondpage.present;

import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;

/**
 * Created by ${fanjie} on 2017/6/21.
 */

public interface BasePresent {

    String[] getTypeTitles();

    @DrawableRes int[] getTypeIcons();

    @DrawableRes int[] getSelectIcons();

    String getType();

    void query(String world);

    <T>Fragment getFragment(T datas);

    boolean canScroll();

    boolean canScroll(int position);

}

package com.pingzhi.cn.carmanual.helppage.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.chechi.aiandroid.R;
import com.haozhang.lib.AnimatedRecordingView;
import com.pingzhi.cn.carmanual.helppage.model.TalkModel;
import com.pingzhi.cn.carmanual.helppage.present.HelpPresent;
import com.pingzhi.cn.carmanual.helppage.view.ownview.MyLayout;
import com.pingzhi.cn.carmanual.present.ShowtipPresent;
import com.pingzhi.cn.carmanual.utils.OperatorUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class HelpActivity extends AppCompatActivity {

    public static void start(Context context){
        Intent intent = new Intent(context, HelpActivity.class);

        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        initView();
        initData();
        initAction();
    }

    private void initAction() {
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });

        homeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stepHome();
            }
        });

        mRecordingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVoice();
            }
        });
    }

    private void startVoice(){
//        if (mVoiceManager.isSpeaking()){
//            mVoiceManager.stopVoice();
//        }else if (!mVoiceManager.isListening()){
//            mVoiceManager.startRecord();
//        }
        if (mShowtipPresent.isVoice()){
            mShowtipPresent.stopVoice();
            return;
        }

        if (mShowtipPresent.isRecord()){
            mShowtipPresent.stopRecord();
        }else {
            mShowtipPresent.startRecord();
        }
    }

    private void stepHome(){
        OperatorUtils.stepToHome(HelpActivity.this);
    }

    private void back(){
        finish();
    }

//    private VoiceManager mVoiceManager;
    private ShowtipPresent mShowtipPresent;
    private HelpPresent mHelpPresent;
    private void initData() {
        mShowtipPresent = new ShowtipPresent(mRecordingView);
        mHelpPresent = new HelpPresent();
        mHelpPresent.start();
    }

    private ImageView backImg;
    private ImageView homeImg;
    private AnimatedRecordingView mRecordingView;
    private MyLayout mBaoYang;
    private MyLayout mGuZhang;
    private MyLayout mChangShi;
    private MyLayout mBaoXian;
    private void initView() {
        backImg = (ImageView) findViewById(R.id.id_help_back);
        homeImg = (ImageView) findViewById(R.id.id_help_home);
        mRecordingView = (AnimatedRecordingView) findViewById(R.id.id_help_huatong);
        mBaoYang = (MyLayout) findViewById(R.id.id_help_layout1);
        mChangShi = (MyLayout) findViewById(R.id.id_help_layout2);
        mGuZhang = (MyLayout) findViewById(R.id.id_help_layout3);
        mBaoXian = (MyLayout) findViewById(R.id.id_help_layout4);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleTalksModel(com.pingzhi.cn.carmanual.busmodel.TalkModel talkModel){
        mBaoXian.setContent(talkModel.talkMap.get(TalkModel.CHE_XIAN));
        mGuZhang.setContent(talkModel.talkMap.get(TalkModel.WEI_XIU));
        mChangShi.setContent(talkModel.talkMap.get(TalkModel.CHANG_SHI));
        mBaoYang.setContent(talkModel.talkMap.get(TalkModel.BAO_YANG));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mShowtipPresent.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mShowtipPresent.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mShowtipPresent.onDestroy();
    }
}

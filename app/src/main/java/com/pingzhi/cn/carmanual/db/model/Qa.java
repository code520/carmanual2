package com.pingzhi.cn.carmanual.db.model;

import com.pingzhi.cn.carmanual.Constant;
import com.pingzhi.cn.carmanual.model.AIAnswerModel;
import com.pingzhi.cn.carmanual.secondpage.model.ShowPagerModel;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

/**
 * Created by ${fanjie} on 2017/7/4.
 * qa问答
 */
@Entity(createInDb = false, nameInDb = "gs8_qa")
public class Qa implements ToShowPagerModel{
    @Property(nameInDb = "id")
    @Id(autoincrement = true)
    public long id;

    public String category;

    public String question;

    public String answer;

    @Generated(hash = 2127604385)
    public Qa(long id, String category, String question, String answer) {
        this.id = id;
        this.category = category;
        this.question = question;
        this.answer = answer;
    }

    @Generated(hash = 555976076)
    public Qa() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getQuestion() {
        return this.question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return this.answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public ShowPagerModel getShowPagerModel(AIAnswerModel aiAnswerModel) {
        ShowPagerModel showPagerModel = new ShowPagerModel();
        if (Constant.isMutibleVoice){
            showPagerModel.question = aiAnswerModel.getResult().getTitle();
        }else {
            showPagerModel.question = aiAnswerModel.getSemantic().getText();
        }
        showPagerModel.content = aiAnswerModel.getResult().getAnswer();
        String category = aiAnswerModel.getResult().getCategory();
        if (!category.equals("保养")){
            showPagerModel.type = Constant.CHE_XIAN;
        }else {
            showPagerModel.type = Constant.BAO_YANG;
        }
        return showPagerModel;
    }

    @Override
    public ShowPagerModel getShowPagerModel() {
        ShowPagerModel showPagerModel = new ShowPagerModel();
        showPagerModel.question = question;
        showPagerModel.content = answer;
        if (!category.equals("保养")){
            showPagerModel.type = Constant.CHE_XIAN;
        }else {
            showPagerModel.type = Constant.BAO_YANG;
        }
        return showPagerModel;
    }
}

package com.pingzhi.cn.carmanual.secondpage.fragment.chexian.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/9.
 */

public class TiaoKuanModel {

    public String title;

//    public String content;

    public String define;

    public String touBaoPrice;

    public String peiFuPrice;

    public String priceAdd;

    public String mianPeiLv;

    public List<TiaoKuanItem> datas;

    public List<TiaoKuanItem> getDatas() {
        if (datas == null){
            datas = new ArrayList<>(5);
            datas.add(new TiaoKuanItem("险种定义", define));
            datas.add(new TiaoKuanItem("投保价格", touBaoPrice));
            datas.add(new TiaoKuanItem("赔付金额", peiFuPrice));
            datas.add(new TiaoKuanItem("金额累加", priceAdd));
            datas.add(new TiaoKuanItem("免  赔  率", mianPeiLv));
        }
        return datas;
    }

    public class TiaoKuanItem{
        public String type;

        public String content;

        public TiaoKuanItem(String type, String content) {
            this.type = type;
            this.content = content;
        }

        @Override
        public String toString() {
            return type + ":" + content;
        }
    }

}

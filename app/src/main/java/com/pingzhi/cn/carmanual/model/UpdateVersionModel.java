package com.pingzhi.cn.carmanual.model;

import java.util.List;

/**
 * Created by ${fanjie} on 2017/7/17.
 */

public class UpdateVersionModel {


    /**
     * code : 200
     * msg : success
     * content : [{"type":"1","map":{"sqls":["INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);"]},"version":2000,"zipName":""},{"type":"2","map":{"deleteimg":["g03.jpg","g04.jpg"]},"version":2000,"zipName":"2000.txt"}]
     */

    private int code;
    private String msg;
    private List<ContentBean> content;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ContentBean> getContent() {
        return content;
    }

    public void setContent(List<ContentBean> content) {
        this.content = content;
    }

    public static class ContentBean {
        /**
         * type : 1
         * map : {"sqls":["INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);","INSERT INTO member(name,age)  values('SIMTH',20);"]}
         * version : 2000
         * zipName :
         */

        private String type;
        private MapBean map;
        private int version;
        private String zipName;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public MapBean getMap() {
            return map;
        }

        public void setMap(MapBean map) {
            this.map = map;
        }

        public int getVersion() {
            return version;
        }

        public void setVersion(int version) {
            this.version = version;
        }

        public String getZipName() {
            return zipName;
        }

        public void setZipName(String zipName) {
            this.zipName = zipName;
        }

        public static class MapBean {
            private List<String> sqls;

            public List<String> getSqls() {
                return sqls;
            }

            public void setSqls(List<String> sqls) {
                this.sqls = sqls;
            }
        }
    }
}

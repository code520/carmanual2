package com.pingzhi.cn.carmanual;

import android.support.annotation.IntDef;

import static com.pingzhi.cn.carmanual.Constant.BAO_YANG;
import static com.pingzhi.cn.carmanual.Constant.CHANG_SHI;
import static com.pingzhi.cn.carmanual.Constant.CHE_XIAN;
import static com.pingzhi.cn.carmanual.Constant.QA;
import static com.pingzhi.cn.carmanual.Constant.WEI_XIU;

/**
 * Created by ${fanjie} on 2017/6/21.
 */
@IntDef({CHE_XIAN, CHANG_SHI, WEI_XIU, BAO_YANG, QA})
public @interface CARQUESTIONTYPE {
}

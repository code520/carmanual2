package com.pingzhi.cn.carmanual.firstpage.view;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chechi.aiandroid.R;
import com.haozhang.lib.AnimatedRecordingView;
import com.pingzhi.cn.carmanual.CARQUESTIONTYPE;
import com.pingzhi.cn.carmanual.busmodel.ReceiveJpushModel;
import com.pingzhi.cn.carmanual.db.zipdownload.ZipDownLoaderUtils;
import com.pingzhi.cn.carmanual.firstpage.view.ownview.CircleMenuLayout2;
import com.pingzhi.cn.carmanual.firstpage.view.ownview.TipGrounp;
import com.pingzhi.cn.carmanual.helppage.view.HelpActivity;
import com.pingzhi.cn.carmanual.ownview.Util;
import com.pingzhi.cn.carmanual.present.ShowtipPresent;
import com.pingzhi.cn.carmanual.secondpage.view.SecondActivity;
import com.pingzhi.cn.carmanual.utils.OperatorUtils;
import com.pingzhi.cn.carmanual.utils.PreferenceUtils;
import com.pingzhi.cn.carmanual.utils.ViewGestuerHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.pingzhi.cn.carmanual.Constant.BAO_YANG;
import static com.pingzhi.cn.carmanual.Constant.CHANG_SHI;
import static com.pingzhi.cn.carmanual.Constant.CHE_XIAN;
import static com.pingzhi.cn.carmanual.Constant.WEI_XIU;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "=MainActivity=";
    private ShowtipPresent mShowtipPresent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        ZipDownLoaderUtils.download(Constant.DOWNLOAD_IMAGE, this);
        ZipDownLoaderUtils.checkUpdate(this);
        initView();
        initData();
        initAction();
    }

    private Util windowUtils;

    private Util getWindowUtils(){
        if (windowUtils == null){
            windowUtils = new Util();
        }
        return windowUtils;
    }

//    private String[] typeStrs = {"车险", "常识", "", "", "", "", "", "维修", "保养"};
    private String[] typeStrs = {"手册", "保养", "", "", "", "", "", "", "", "", "车险", "故障"};
//    private int[] imgs = {R.drawable.automobile_selector, R.drawable.maintain_selector, R.mipmap.trans, R.mipmap.trans, R.mipmap.trans, R.mipmap.trans, R.mipmap.trans, R.drawable.fault_selector, R.drawable.manual_selector};
    private int[] imgs = {R.drawable.maintain_selector, R.drawable.manual_selector, R.mipmap.trans, R.mipmap.trans, R.mipmap.trans, R.mipmap.trans, R.mipmap.trans, R.mipmap.trans, R.mipmap.trans, R.mipmap.trans, R.drawable.automobile_selector, R.drawable.fault_selector};
    private ViewGestuerHelper mGestuerHelper;
    private void initData() {
        mCircleMenuLayout2.setMenuItemIconsAndTexts(imgs, typeStrs);
        mShowtipPresent = new ShowtipPresent(mRecordingView);
        mTipGrounp.setContent(PreferenceUtils.getInstance().getJpushMessage("夏季路面高温，注意保持规定的轮胎气压，避免发生爆胎。"));
        mGestuerHelper = new ViewGestuerHelper(mRoot, new ViewGestuerHelper.OpenMenuAble() {
            @Override
            public void openMenu() {
                if (!mCircleMenuLayout2.isOpen()){
                    mCircleMenuLayout2.open();
                }
            }
        });
    }

    private void initAction() {
        openCloseSwith.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operatorSwith();
            }
        });
        mCircleMenuLayout2.setOnMenuItemClickListener(new CircleMenuLayout2.OnMenuItemClickListener() {
            @Override
            public void itemClick(View view, int pos) {
                operatorTypeClick(pos);
//                AIMutualModel aiMutualModel = new AIMutualModel();
//                List<String> datas = new ArrayList<String>();
//                for (int i = 0; i < 20; i++){
//                    datas.add("item" + i);
//                }
//                aiMutualModel.datas = datas;
//                EventBus.getDefault().post(aiMutualModel);
            }

            @Override
            public void itemCenterClick(View view) {

            }
        });
        mRecordingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mShowtipPresent.isVoice()){
                    mShowtipPresent.stopVoice();
                    return;
                }

                if (mShowtipPresent.isRecord()){
                    mShowtipPresent.stopRecord();
                }else {
                    mShowtipPresent.startRecord();
                }
            }
        });

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.finish();
            }
        });

        homeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorUtils.stepToHome(MainActivity.this);
            }
        });

        helpImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stepToHelp();
            }
        });

        helpText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stepToHelp();
            }
        });

        mTipGrounp.setOnTipClickListener(new TipGrounp.OnTipClickListener() {
            @Override
            public void onTipClick(View view) {
                mShowtipPresent.startVoiceWithText(mTipGrounp.getContent());
            }
        });
    }

    private void stepToHelp(){
        HelpActivity.start(this);
    }

    private void operatorTypeClick(int pos){

        switch (pos){
            case 0:{
                startActivity(CHANG_SHI);
            }
            break;
            case 1:{
                startActivity(BAO_YANG);
            }
            break;
            case 11:{
                startActivity(WEI_XIU);
            }
            break;
            case 10:{
                startActivity(CHE_XIAN);
            }
            break;
            default:
                break;
        }
    }

    private void startActivity(@CARQUESTIONTYPE int type){
        SecondActivity.start(this, type);
    }

    private void operatorSwith(){
        if (mCircleMenuLayout2.isOpen()){
            mCircleMenuLayout2.close();
        }else {
            mCircleMenuLayout2.open();
        }
    }

    private CircleMenuLayout2 mCircleMenuLayout2;
    private ImageView backImg;
    private ImageView homeImg;
    private AnimatedRecordingView mRecordingView;
    private ImageView openCloseSwith;
    private ImageView helpImg;
    private TextView helpText;
//    private TextView scrollText;
    private ConstraintLayout mRoot;
    private TipGrounp mTipGrounp;
    private void initView() {
        mCircleMenuLayout2 = (CircleMenuLayout2) findViewById(R.id.id_main_menu);
        backImg = (ImageView) findViewById(R.id.id_main_back);
        homeImg = (ImageView) findViewById(R.id.id_main_home);
        mRecordingView = (AnimatedRecordingView) findViewById(R.id.id_main_huatong_back);
        openCloseSwith = (ImageView) findViewById(R.id.id_main_open_close);
        helpImg = (ImageView) findViewById(R.id.id_main_help);
        helpText = (TextView) findViewById(R.id.textView);
        mTipGrounp = (TipGrounp) findViewById(R.id.id_main_scroll_text);
        mRoot = (ConstraintLayout) findViewById(R.id.root);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
        mShowtipPresent.onResume();
//        mMyLayout.startRecord();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        mShowtipPresent.onPause();
//        mMyLayout.stopRecord();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mShowtipPresent.onDestroy();
        mGestuerHelper.destroy();
        mGestuerHelper = null;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleReceiveJpush(ReceiveJpushModel receiveJpushModel){
//        scrollText.setText(receiveJpushModel.content);
        mTipGrounp.setContent(receiveJpushModel.content);
    }

//    @Subscribe
//    public void handleWakeupSuccess(WakeupSuccessModel wakeupSuccessModel){
//
//        showVoiceVolum();
//    }

//    @Subscribe
//    public void handleVoiceFinish(VoiceFinishModel voiceFinishModel){
//        showHuaTong();
//    }

//    @Subscribe
//    public void handleVoiceNotify(VoiceNotifyModel voiceNotifyModel){
//        mRecordingView.setVolume(voiceNotifyModel.volum);
//    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void handleRecordResult(RecordResultModel recordResultModel){
//        mRecordingView.loading();
//    }


//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void handleRecordResult(RecordResultModel recordResultModel){
//        getWindowUtils().showTips(MainActivity.this);
//        getWindowUtils().setContent(recordResultModel.content);
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void handleRecordBegin(RecordBeginModel recordBeginModel){
//        getWindowUtils().showTips(MainActivity.this);
//        getWindowUtils().setContent("正在处理。。。");
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void handleRecordError(RecordErrorModel recordErrorModel){
//        if (recordErrorModel.errorCode == 10118){
//             mVoiceManager.startVoice("您好像没有说话哦.");
//            getWindowUtils().showTips(MainActivity.this);
//            getWindowUtils().setContent("您好像没有说话哦.");
//        }
//    }
}

package cn.fanjie.com.cjvolley;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;

import java.util.Map;

/**
 * Created by ${fanjie} on 16/10/3.
 */

public class JsonArrayObjectOperator extends BaseOperator {

    @Override
    Request postWithParams(String url, final Map params, OnNetworkReturn callback) {
        return new JsonArrayRequest(Request.Method.POST, url, (Response.Listener)listener.get(), (Response.ErrorListener)errorListener.get()){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

        };
    }

    @Override
    Request requestWithNoReturnParams(int Method, String url, OnNetworkReturn callback) {
        return new JsonArrayRequest(Method, url, (Response.Listener)listener.get(), (Response.ErrorListener)errorListener.get());
    }
}

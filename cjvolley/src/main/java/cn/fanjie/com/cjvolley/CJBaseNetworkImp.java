package cn.fanjie.com.cjvolley;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by ${fanjie} on 16/10/3.
 */

public class CJBaseNetworkImp implements CJBaseNetworkInterface {


    private RequestQueue mQueue;

    public CJBaseNetworkImp(Context context) {
        //HurlStack两个参数默认都是null,如果传入SSLSocketFactory，那么会以Https的方式来请求网络
//        HurlStack stack = new HurlStack(null, SSLFactory.getSocketFactory(context).sSLSocketFactory);
//        mQueue = Volley.newRequestQueue(context, stack);
        mQueue = Volley.newRequestQueue(context);
        ManagerRequestClass.registerRequestClass(String.class.getName(), new StringRequestOperator());
        ManagerRequestClass.registerRequestClass(JSONArray.class.getName(), new JsonArrayObjectOperator());
        ManagerRequestClass.registerRequestClass(JSONObject.class.getName(), new JsonObjectRequestOperator());
    }

//    protected  InputStream pkcs12ToBks(InputStream pkcs12Stream, String pkcs12Password) {
//        final char[] password = pkcs12Password.toCharArray();
//        try {
//            KeyStore pkcs12 = KeyStore.getInstance("PKCS12");
//            pkcs12.load(pkcs12Stream, password);
//            Enumeration<String> aliases = pkcs12.aliases();
//            String alias;
//            if (aliases.hasMoreElements()) {
//                alias = aliases.nextElement();
//            } else {
//                throw new Exception("pkcs12 file not contain a alias");
//            }
//            Certificate certificate = pkcs12.getCertificate(alias);
//            final Key key = pkcs12.getKey(alias, password);
//            KeyStore bks = KeyStore.getInstance("BKS");
//            bks.load(null, password);
//            bks.setKeyEntry(alias, key, password, new Certificate[]{certificate});
//            ByteArrayOutputStream out = new ByteArrayOutputStream();
//            bks.store(out, password);
//            return new ByteArrayInputStream(out.toByteArray());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }

//    @Override
//    public <T> void get(@NonNull String url, final OnNetworkReturn<T> callback) {
//        Class<T> clazz = (Class<T>) GetClass.getSuperClassGenricType(callback.getClass(), 0).getClass();
//        String name = clazz.getName();
//        if (String.class.getName().equals(name)){
//            StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
//                @Override
//                public void onResponse(String response) {
//                    callback.onSuccess((T) response);
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    callback.onEror(error);
//                }
//            });
//        }else if (JSONObject.class.getName().equals(name)){
//            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    callback.onSuccess((T) response);
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    callback.onEror(error);
//                }
//            });
//        }else if (JSONArray.class.getName().equals(name)){
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
//                @Override
//                public void onResponse(JSONArray response) {
//                    callback.onSuccess((T) response);
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//
//                }
//            });
//        }
//    }
//
//    @Override
//    public <T> void get(@NonNull String url, Map<String, String> params, OnNetworkReturn<T> callback) {
//
//    }
//
//    @Override
//    public <T> void post(@NonNull String url, OnNetworkReturn<T> callback) {
//
//    }
//
//    @Override
//    public <T> void post(@NonNull String url, Map<String, String> params, OnNetworkReturn<T> callback) {
//
//    }

    @Override
    public <T> void request(int method, @NonNull String url, Map<String, Object> params, OnNetworkReturn<T> callback) {
       Request<T> request = ManagerRequestClass.<T>startNetwork(method, url, params, callback);
        mQueue.add(request);
    }

    @Override
    public <T> void request(@NonNull String url, Map<String, Object> params, OnNetworkReturn<T> callback) {
        request(Request.Method.GET, url, params, callback);
    }
}
